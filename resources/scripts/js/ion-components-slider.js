/* 
 * See license information at the package root in LICENSE.md
 */

try {
    ion;
} catch (e) {
    if (e instanceof ReferenceError) {
        throw new Error("The 'ion' module has not been initialized.");
    }
}


ion.registerModule('slider', function () {

    this.sliders = {};
//    this.initializationHook = function() { return; };

    this.__construct = function () {

        ion.onContentLoaded(function (event, obj) {

            obj.update();

        }, this);
    },
    this.getSliders = function () {

        return this.sliders;
    },
    this.getSlider = function (sliderName) {

        if (Object.prototype.hasOwnProperty.call(this.sliders, sliderName)) {

            return this.sliders[sliderName];
        }

        throw new Error('Slider "' + sliderName + '" does not exist.');
    },
    this.hasSliders = function () {

        return (this.sliders.keys().length > 0);
    },
    this.hasSlider = function (sliderName) {

        try {

            this.getSlider(sliderName);
            return true;
        } 
        catch(error) {

            return false;
        }
    },
    
//    this.setInitializationHook = function(hook) {
//        
//        this.initializationHook = hook;
//        
//        ion.onContentLoaded(function (event, obj) {
//
//            obj.update();
//
//        }, this);
//        
//        return this;
//    },
//    
//    this.getInitializationHook = function() {
//        
//        return this.initializationHook;
//    },
//    
//    this.hasInitializationHook = function() {
//      
//        return !ion.isEmpty(this.initializationHook);
//    },

    this.update = function () {


        ion.whenContentLoaded(function (event, obj) {

            var sliders = document.querySelectorAll("[data-component='ion/slider']");

            ion.forEach(sliders, function (sliderElement) {

                if (!ion.isEmpty(sliderElement.dataset.name)) {

                    var slider = {

                        element: null,

                        initialize: function(sliderElement) {

//                            if(!obj.hasInitializationHook()) {
//                            
//                                throw new Error('No initialization hook has been specified!');
//                            }
//                            
                            this.element = sliderElement;
//                            
//                            obj.getInitializationHook()(this);
                            
                            return this;
                        },
                        getName: function () {

                            if (ion.isEmpty(this.element.dataset.name)) {

                                return null;
                            }

                            return this.element.dataset.name;
                        },
                        getElement: function() {
                            
                            return this.element;
                        }

                    };

                    slider = slider.initialize(sliderElement);
                    obj.sliders[slider.getName()] = slider;     
                
                } else {

                    ion.console.groupCollapsed('Skipped slider due to the fact that no "data-name" attribute was specified.');
                    ion.console.dir(sliderElement);
                    ion.console.groupEnd();
                }
            });
            
        }, this);
        

        return this;
    };

    this.__construct();
});
