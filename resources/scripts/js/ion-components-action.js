/* 
 * See license information at the package root in LICENSE.md
 */

try {
    ion;
} catch (e) {
    if (e instanceof ReferenceError) {
        throw new Error("The 'ion' module has not been initialized.");
    }
}

ion.registerModule('actions', function () {

    this.actions = [];
    this.onClickHook = null;

    this.__construct = function () {

        ion.onContentLoaded(function (event, obj) {

            obj.update();

        }, this);
    },
    this.getActions = function () {

        return this.actions;
    },
    this.getAction = function (componentId) {


        if (Object.prototype.hasOwnProperty.call(this.actions, componentId)) {

            return this.actions[componentId];
        }

        throw new Error('Action "' + componentId + '" does not exist.');
    },
    this.hasActions = function () {

        return (this.actions.keys().length > 0);
    },
    this.setActivationHook = function(closure) {
        
        this.onClickHook = closure;
        return this;
    },
    this.getActivationHook = function() {
        
        return this.onClickHook;        
    },
    this.hasActivationHook = function() {
        
        return (this.onClickHook !== null);
    },
    this.update = function () {

        ion.whenContentLoaded(function (event, obj) {

            var actions = document.querySelectorAll('[data-component="ion/action"]');

            ion.forEach(actions, function (actionElement) {

//                if (!ion.isEmpty(actionElement.attributes.id.nodeValue)) {
                    
                
                var action = {

                    element: null,
                    captionElement: null,
                    targetElement: null,

                    initialize: function (actionElement) {

                        this.element = actionElement;
                        
                        var captions = this.element.querySelectorAll("[data-component='ion/action-caption']");

                        if(captions && captions.length > 0) {

                            this.captionElement = captions[0];
                        }
                        
                        var targets = this.element.querySelectorAll("[data-component='ion/action-target']");

                        if(targets && targets.length > 0) {

                            this.targetElement = targets[0];
                        }
                            
                        return this;
                    },

                    getId: function () {

                        if (ion.isEmpty(this.element.attributes.id.nodeValue)) {

                            return null;
                        }

                        return this.element.attributes.id.nodeValue;                                  
                    },

                    getElement: function () {

                        return this.element;
                    },

                    getCaptionElement: function () {
                      
                        return this.captionElement;
                    },                    
                    
                    hasCaptionElement: function () {
                      
                        return (this.getCaptionElement() != null);
                    },                    

                    getTargetElement: function () {
                      
                        return this.targetElement;
                    },               
                    
                    hasTargetElement: function () {
                      
                        return (this.getTargetElement() != null);
                    },
                    
                    getAutoClickCountdown: function () {
                        
                        return this.element.dataset.autoClickCountdown;
                    },
                    
                    hasAutoClickCountdown: function () {
                      
                        return (this.getAutoClickCountdown() > 0);
                    },
                    
                    getAutoClickDelay: function () {
                        
                        return this.element.dataset.autoClickDelay;
                    },
                    
                    hasAutoClickDelay: function () {
                      
                        return (this.getAutoClickDelay() > 0);
                    },
                    
                    activate: function() {
                        
                        if(obj.hasActivationHook()) {
                            
                            obj.getActivationHook()();
                            return;
                        }
                        
                        if(this.hasTargetElement()) {

                            if(this.getTargetElement().nodeName === 'A' || this.getTargetElement().nodeName === 'BUTTON') {

                                if(this.getTargetElement().nodeName === 'A') {
                                    
                                    var queryChar = '?';
                                    
                                    if(this.getTargetElement().href.indexOf('?') > -1) {
                                        
                                        queryChar = '&';
                                    }
                                    
                                    this.getTargetElement().href = this.getTargetElement().href + queryChar + "auto=true";
                                }

                                this.getTargetElement().click();
                                return;
                            }                            
                        }                        
                        
                        console.log("No action target has been defined - please provide an activation hook or an action link.");
                        return;
                    }
                };

                action = action.initialize(actionElement);
                obj.actions.push(action);

                if(action.hasAutoClickDelay()) {

                    var delay = action.getAutoClickDelay();
                    
                    if(action.hasAutoClickCountdown()) {

                        delay = delay - action.getAutoClickCountdown();
                    }                    
                    
                    window.setTimeout(function() {
                    
                        var targetElement = action.getTargetElement();
                    
                        if(action.hasAutoClickCountdown()) {

                            var captionElement = action.getCaptionElement();
                            var caption = null;
                            
                            if(captionElement && captionElement.textContent) {
                                
                                caption = captionElement.textContent;
                            }

                            window.setTimeout(function() {
                                
                                var tick = action.getAutoClickCountdown();
                                
                                var timer = window.setInterval(function() {
                                    
                                    captionElement.textContent = caption + ' (' + tick + ')';
                                    
                                    if(tick === 0) {
                                        
                                        window.clearInterval(timer);
                                        captionElement.textContent = caption;
                                        
                                        if(obj.hasActivationHook()) {

                                            obj.getActivationHook()();                        

                                        } else {

                                            action.activate();
                                        }                                        
                                    }
                                    
                                    tick--;
                                    
                                }, 1000);

                            }, action.getAutoClickCountdown());

                        } else {

                            if(obj.hasActivationHook()) {

                                obj.getActivationHook()();                        

                            } else {

                                action.activate();
                            }                        
                        }
                        
                    }, delay * 1000);
                    
                }

                action.getElement().addEventListener('click', function(event) {
                    
                    if(obj.hasActivationHook()) {
                        
                        event.preventDefault();
                        obj.getActivationHook()();                        
                        return false;
                    }

                    return true;
                });

 
            }, obj);

        }, this);
    };

    this.__construct();
});
