/* 
 * See license inmodalation at the package root in LICENSE.md
 */

try {
    ion;
} catch (e) {
    if (e instanceof ReferenceError) {
        throw new Error("The 'ion' module has not been initialized.");
    }
}


ion.registerModule('modal', function () {

    this.modals = {};

    this.__construct = function () {

        ion.onContentLoaded(function (event, obj) {

            obj.update();

        }, this);
    },
    this.getModals = function () {

        return this.modals;
    },
    this.getModal = function (modalName) {

        if (Object.prototype.hasOwnProperty.call(this.modals, modalName)) {

            return this.modals[modalName];
        }

        throw new Error('Modal "' + modalName + '" does not exist.');
    },
    this.hasModals = function () {

        return (this.modals.keys().length > 0);
    },
    this.hasModal = function (modalName) {

        try {

            this.getModal(modalName);
            return true;
        } 
        catch(error) {

            return false;
        }
    },
    this.isModalActive = function(modalName) {

        var modals = [];

        if(ion.isEmpty(modalName)) {

            modals = this.getModals();      

        } else {

            modals = [ modalName ];
        }

        for(var m in modals) {

            var modal = this.getModal(modals[m].getName());

            if(modal.isActive()) {

                return true;
            }
        }                

        return false;
    },
    this.update = function () {

        ion.whenContentLoaded(function (event, obj) {

            var modals = document.querySelectorAll("[data-component='ion/modal']");
                       
            ion.forEach(modals, function (modalElement) {

                if (!ion.isEmpty(modalElement.dataset.name)) {

                    var modal = {

                        element: null,
                        active: false,
                        initialActivation: false,
                        buttons: [],
                        cookie: null,

                        initialize: function(modalElement) {

                            var self = this;

                            this.element = modalElement;

                            if(modalElement.dataset.cookie) {
                                
                                this.setCookie(modalElement.dataset.cookie);
                            }
                            
                            if(modalElement.dataset.activationFrequency) {
                                
                                this.setActivationFrequency(modalElement.dataset.activationFrequency);
                            }                            
                            
                            var closeBtns = this.element.querySelectorAll("[data-component='ion/modal-close-button']");

                            if(closeBtns && closeBtns.length > 0) {

                                var closeBtn = closeBtns[0];

                                closeBtn.onclick = function(event) {

                                    self.deactivate();
                                };
                            }

                            var buttons = this.element.querySelectorAll("[data-component='ion/modal-button']");

                            if(buttons && buttons.length > 0) {
                                
                                ion.forEach(buttons, function (buttonElement) {

                                    if (!ion.isEmpty(buttonElement.dataset.click)) {

                                        var button = {

                                            element: buttonElement,
                                            handler: new Function(buttonElement.dataset.click)
                                        };

                                        self.buttons.push(button);
                                        
                                        buttonElement.onclick = function(event) {
                                          
                                            var result = button.handler(event);
                                            
                                            if(result !== false ) {
                                            
                                                self.deactivate();
                                            }
                                        };
                                    }
                                });                                
                            }
                            
                            // NOTE: ion.hasCookie() checks browser cookies,
                            // self.*Cookie() loads the modal cookie name
                            
                            if(this.hasCookie() && ion.hasCookie(this.getCookie())) {
                                
                                return this;
                            }                            

                            if(this.hasActivationFrequency() && ion.hasCookieValue('session-modals', this.getName())) {

                                var activationCount = ion.getCookieValue('session-modals', this.getName());

//                                console.groupCollapsed(this.getName());
//                                console.log("FREQ: ", this.getActivationFrequency());                         
//                                console.log("CNT: ", activationCount);
//                                console.groupEnd();

                                if(this.getActivationFrequency() == 0 && activationCount > 0) {

//                                    console.log("NOT SUPPOSED TO SHOW");
                                    return this;
                                }

                                if(this.getActivationFrequency() % activationCount != 0) {

                                    return this;
                                }
                            }

                            if(this.getActivation() === 'page-load') {


                                if(!self.getAllowMultipleActivations() && self.initialActivation) {

                                    return this;
                                }

                                self.activate(true);
                                return this;
                            }

                            if(this.getActivation() === 'mouse-exit') {

                                var leaveElement = document;

                                if(!!window.MSInputMethodContext && !!document.documentMode) {

                                    leaveElement = document.body;
                                }

                                leaveElement.onmouseout = function (event) {

                                    if(!self.getAllowMultipleActivations() && self.initialActivation) {

                                        return;
                                    }

                                    if (event.clientY < 0) {

                                        self.activate(true);
                                    }
                                };

                                return this;
                            }                                    

                            return this;
                        },


                        getName: function () {

                            if (ion.isEmpty(this.element.dataset.name)) {

                                return null;
                            }

                            return this.element.dataset.name;
                        },

                        deactivate: function () {

                            if (this.isActive()) {

                                ion.removeClass(this.element, 'active');
                                ion.addClass(this.element, 'inactive');

                                this.active = false;
                            }
                        },

                        activate: function (obeyDelay) {

                            var self = this;

                            if(ion.modal.isModalActive()) {

                                return;
                            }
                            
                            var activationCount = 0;                         
                            
                            if(ion.hasCookieValue('session-modals', this.getName())) {
                                                           
                               activationCount = ion.getCookieValue('session-modals', this.getName());                                 
                            }
                            
                            activationCount++;
                            
                            ion.setCookieValue('session-modals', this.getName(), activationCount);

                            var callBack = function() {

                                if (!self.isActive()) {

                                    ion.removeClass(self.element, 'inactive');
                                    ion.addClass(self.element, 'active');                                            

                                    self.active = true;

                                    if(!self.initialActivation) {

                                        self.initialActivation = true;
                                    }                                            
                                }
                            };

                            if(obeyDelay) {

                                if(self.getDelay() === 0) {

                                    callBack();
                                    return;
                                }

                                setTimeout(function () {

                                    callBack();

                                }, self.getDelay() * 1000);

                                return;
                            }

                            callBack();
                            return this;
                        },

                        isActive: function () {

                            return this.active === true;
                        },

                        setDelay: function (delay) {

                            this.element.dataset.delay = delay;
                            return this;
                        },

                        getDelay: function () {

                            if (ion.isEmpty(this.element.dataset.delay)) {

                                return 0;
                            }

                            return this.element.dataset.delay;
                        },

                        setActivation: function (activation) {

                            this.element.dataset.activation = activation;
                            return this;
                        },

                        getActivation: function () {

                            if (ion.isEmpty(this.element.dataset.activation)) {

                                return 'none';
                            }

                            return this.element.dataset.activation;
                        },

                        setAllowMultipleActivations: function (multipleActivations) {

                            this.element.dataset.multipleActivations = multipleActivations;
                            return this;
                        },                                

                        getAllowMultipleActivations: function() {

                            if (ion.isEmpty(this.element.dataset.multipleActivations)) {

                                return false;
                            }

                            return true;                                    
                        },
                        
                        getButtons: function() {
                            
                            return this.buttons;
                        },
                        
                        getButton: function(buttonName) {
                            
                            if (Object.prototype.hasOwnProperty.call(this.buttons, buttonName)) {

                                return this.buttons[buttonName];
                            }

                            throw new Error('Modal button "' + buttonName + '" does not exist.');                            
                            
                        },
                        
                        setCookie: function(cookie) {
                            
                            this.cookie = cookie;
                            return this;
                        },
                        
                        getCookie: function() {
                            
                            return this.cookie;
                        },
                        
                        hasCookie: function() {
                         
                            return this.cookie != null;
                        },
                        
                        setActivationFrequency: function(activationFrequency) {
                            
                            this.activationFrequency = activationFrequency;
                            return this;
                        },
                        
                        getActivationFrequency: function() {
                            
                            return this.activationFrequency;
                        },
                        
                        hasActivationFrequency: function() {
                         
                            return this.activationFrequency != null;
                        }                        
                    };

                    modal = modal.initialize(modalElement);
                    obj.modals[modal.getName()] = modal;

                } else {

                    ion.console.groupCollapsed('Skipped modal due to the fact that no "data-name" attribute was specified.');
                    ion.console.dir(modalElement);
                    ion.console.groupEnd();
                }
            }, obj);

            var targets = document.querySelectorAll("a[data-modal-target]");

            ion.forEach(targets, function (targetElement) {

                targetElement.addEventListener('click', function(event) {

                    var targetModalName = null;

                    if(targetElement.dataset.modalTarget) {

                        targetModalName = targetElement.dataset.modalTarget;
                    }

                    if(targetModalName == null) {
                        
                        return true;
                    }

                    var modal = ion.modal.getModal(targetModalName);

                    if(modal) {

                        event.preventDefault();
                        modal.activate(false);                            

                        return false;
                    }

                    return true;
                });                        

            }, obj);

        }, this);
    };

    this.__construct();
    
    //console.log(this.modals);
});
