/* 
 * See license information at the package root in LICENSE.md
 */

var ion = {};

(function () {

//    function IonModule() {
//        
//        this.assetPath = null;
//      
//        this.__construct = function () {
//          
//            return this;
//        };
//        
//        this.update = function () {
//            
//            return this;
//        };
//        
//        this.setAssetPath = function(assetPath) {
//            
//            this.assetPath = assetPath;
//            return this;
//        };
//        
//        this.getAssetPath = function() {
//          
//            return this.assetPath;
//        };
//        
//        this.__construct();
//    };

    this.modules = {};
    this.values = {};
    this.mediaQueries = {

        'small': null,
        'medium': null,
        'large': null
    };
    this.windowSizeCallBacks = {

        'small': [],
        'medium': [],
        'large': []
    };
    this.settings = {
        'debugMode': false
    };
    this.initialized = false;
    this.loaded = false;   

    this.__construct = function () {

        // Create the virtual console module

        ion.registerModule('console', function () {

            this.__construct = function () {

            },
            this.log = function (items) {

                console.log(items);
            },
            this.dir = function (obj) {

                console.dir(obj);
            },
            this.groupCollapsed = function (title) {

                console.groupCollapsed(title);
            },
            this.groupEnd = function () {

                console.groupEnd();
            };

            this.__construct();
        });

        // Start doing DOM stuff

        this.onContentLoaded(function (event, obj) {

            if (obj.isLoaded()) {

                return;
            }

            if (!obj.isInitialized()) {

                obj.initialize();
            }

            var body = document.getElementsByTagName('body');

            if (body && body.length > 0) {

                body = body[0];

                if (obj.hasClass(body, 'noscript')) {

                    obj.removeClass(body, 'noscript');
                }
            }

            var sizeClosure = function (event) {

                var callBackClosure = function (mediaQueryKey) {

                    for (var i = 0; i < obj.windowSizeCallBacks[mediaQueryKey].length; i++) {

                        var windowSizeCallBack = obj.windowSizeCallBacks[mediaQueryKey][i];

                        if (windowSizeCallBack && windowSizeCallBack.closure && windowSizeCallBack.closure instanceof Function) {

                            windowSizeCallBack.closure(event, windowSizeCallBack.object);
                        }
                    }
                };

                var mediaQueryKeys = Object.keys(obj.mediaQueries);

                for (var i = 0; i < mediaQueryKeys.length; i++) {

                    var mediaQueryKey = mediaQueryKeys[i];

                    var query = obj.mediaQueries[mediaQueryKey];

                    if (!query) {

                        continue;
                    }

                    var width = window.innerWidth;

                    if (query.min === null && query.max !== null) {

                        if (width <= query.max) {

                            callBackClosure(mediaQueryKey);
                            break;
                        }
                    }

                    if (query.max === null && query.min !== null) {

                        if (width >= query.min) {

                            callBackClosure(mediaQueryKey);
                            break;
                        }
                    }

                    if (width >= query.min && width <= query.max) {

                        callBackClosure(mediaQueryKey);
                        break;
                    }
                }

                return;
            };

            ion.onSize('small', function () {

                if (ion.hasClass(body, 'ion-medium')) {

                    ion.removeClass(body, 'ion-medium');
                }

                if (ion.hasClass(body, 'ion-large')) {

                    ion.removeClass(body, 'ion-large');
                }

                if (!ion.hasClass(body, 'ion-small')) {

                    ion.addClass(body, 'ion-small');
                }
            });

            ion.onSize('medium', function () {

                if (ion.hasClass(body, 'ion-small')) {

                    ion.removeClass(body, 'ion-small');
                }

                if (ion.hasClass(body, 'ion-large')) {

                    ion.removeClass(body, 'ion-large');
                }

                if (!ion.hasClass(body, 'ion-medium')) {

                    ion.addClass(body, 'ion-medium');
                }
            });

            ion.onSize('large', function () {

                if (ion.hasClass(body, 'ion-small')) {

                    ion.removeClass(body, 'ion-small');
                }

                if (ion.hasClass(body, 'ion-medium')) {

                    ion.removeClass(body, 'ion-medium');
                }

                if (!ion.hasClass(body, 'ion-large')) {

                    ion.addClass(body, 'ion-large');
                }
            });


            sizeClosure(null);

            obj.onResize(function (event) {

                sizeClosure(event);
            });

            obj.loaded = true;

        }, this);
    },
    this.initialize = function (callBack, settings) {

        if (this.initialized === true) {

            throw new Error("The 'ion' object has already been initialized.");
        }

        if (callBack && !(callBack instanceof Function)) {

            throw new Error("The call-back needs to be a function/closure.");
        }

        if (settings) {

            var settingsValidationResult = this.__validateSettings(settings);
            if (settingsValidationResult !== null) {

                throw new Error("Settings validation failed: " + settingsValidationResult + ".");
            }
        }

        if (callBack) {

            callBack(this, settings);
        }

        this.initialized = true;

        return this;
    },
    this.__validateSettings = function (settingsObject) {

        //TODO
        return null;
    },
    this.getSettings = function () {

        return this.settings;
    },
    this.isInitialized = function () {

        return this.initialized;
    },
    this.isLoaded = function () {

        return this.loaded;
    },
    this.isDebugMode = function () {

        return this.getSettings().debugMode;
    },
    this.onContentLoaded = function (callBack, obj) {

        if (!callBack) {

            throw new Error("The call-back needs to be specified.");
        }

        if (!(callBack instanceof Function)) {

            throw new Error("The call-back needs to be a function/closure.");
        }

        document.addEventListener("DOMContentLoaded", function (event) {

            callBack(event, obj);
        });

        return this;
    },
    this.whenContentLoaded = function (callBack, obj) {

        if (this.isLoaded()) {

            callBack(event, obj);
            return;
        }

        this.onContentLoaded(callBack, obj);

        return this;
    },
    this.onReady = function (callBack, obj) {

        if (!callBack) {

            throw new Error("The call-back needs to be specified.");
        }

        if (!(callBack instanceof Function)) {

            throw new Error("The call-back needs to be a function/closure.");
        }

        this.onContentLoaded(function (event, obj) {

            // TODO: transitions
            callBack(event, obj);
        }, obj);

        return this;
    },
    this.onResize = function (callBack, obj) {

        if (!callBack) {

            throw new Error("The call-back needs to be specified.");
        }

        if (!(callBack instanceof Function)) {

            throw new Error("The call-back needs to be a function/closure.");
        }

        window.addEventListener("resize", function (event) {

            callBack(event, obj);
        });

        return this;
    },
    this.onSize = function (queryKey, callBack, obj) {

        if (!queryKey) {

            throw new Error("The media query key needs to be specified.");
        }

        if (!callBack) {

            throw new Error("The call-back needs to be specified.");
        }

        if (!(callBack instanceof Function)) {

            throw new Error("The call-back needs to be a function/closure.");
        }

        if (typeof obj === 'undefined') {

            obj = null;
        }

        this.windowSizeCallBacks[queryKey].push({
            'closure': callBack,
            'object': obj
        });

        return this;
    },
    this.getMediaQueries = function () {

        return this.mediaQueries;
    },
    this.setMediaQueries = function (queryObjects) {

        var objectKeys = Object.keys(queryObjects);

        for (var i = 0; i < objectKeys.length; i++) {

            var objectKey = objectKeys[i];

            var queryObj = queryObjects[objectKey];

            if (objectKey.toLowerCase() === 'small' || objectKey.toLowerCase() === 'medium' || objectKey.toLowerCase() === 'large') {

                if (typeof queryObj['min'] === 'undefined') {

                    throw new Error("Media query '" + objectKey + "' is missing the 'min' property.");
                }

                if (typeof queryObj['max'] === 'undefined') {

                    throw new Error("Media query '" + objectKey + "' is missing the 'max' property.");
                }

                if (queryObj['min'] === null && queryObj['max'] === null) {

                    throw new Error("Media query '" + objectKey + "' must have at least one value specified (not null).");
                }

                var tmp = {
                    'min': queryObj['min'],
                    'max': queryObj['max']
                };

                this.mediaQueries[objectKey.toLowerCase()] = tmp;
                continue;
            }

            throw new Error("Invalid media query key ('" + objectKey + "').");
        }

        return this;
    },
    this.hasClass = function (elem, className) {

        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
    },
    this.addClass = function (elem, className) {

        if (!this.hasClass(elem, className)) {
            elem.className += ' ' + className;
        }

        return this;
    },
    this.removeClass = function (elem, className) {

        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';

        if (this.hasClass(elem, className)) {

            while (newClass.indexOf(' ' + className + ' ') >= 0) {

                newClass = newClass.replace(' ' + className + ' ', ' ');
            }

            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }

        return this;
    },
    this.setValue = function (key, value) {

        this.values[key] = value;
    },
    this.getValue = function (key) {

        return this.values[key];
    },
    this.hasValue = function (key) {

        if (!this.values[key]) {

            return false;
        }

        return true;
    },
    this.registerModule = function (name, moduleObject) {

        if (this.hasModule(name)) {

            return;
        }

        this.modules[name] = {};
        moduleObject.apply(this.modules[name]);
        this[name] = this.modules[name];

        return this;
    },
    this.hasModule = function (name) {

        if (!this.modules[name]) {

            return false;
        }

        return true;
    },
    this.forEach = function (array, callBack, scope) {

        for (var i = 0; i < array.length; i++) {

            callBack.call(scope, array[i], i);
        }

        return this;
    },
    this.isAttributeFlagSet = function(elem, name) {

        if(elem.hasAttribute(name)) {

            var tmp = elem.getAttributeNode(name);

            if(!tmp) {

                return false;
            }

            tmp = tmp.value.toLowerCase();

            if(tmp === 'false' || tmp === 'no') {

                return false;
            }

            return true;
        }

        return false;
    },
    this.isEmpty = function (variable) {

        // Credit to: https://www.sitepoint.com/testing-for-empty-values/

        if (typeof (variable) == 'number' || typeof (variable) == 'boolean') {

            return false;
        }

        if (typeof (variable) == 'undefined' || variable === null) {

            return true;
        }

        if (typeof (variable.length) != 'undefined') {

            return variable.length == 0;
        }

        var count = 0;

        for (var i in variable) {

            if (variable.hasOwnProperty(i)) {

                count++;
            }
        }

        return (count == 0);
    },
    
    this.setCookie = function(name, content, expiry, path, domain) {
        
        var expires = null;
        var cookie = name + '=';                
        
        if(expiry) {
            
            var d = new Date();
            d.setTime(d.getTime() + expiry);
            expires = d.toUTCString();
        }
        
        if(typeof(content) === 'object') {
            
            cookie += encodeURIComponent(JSON.stringify(content));
            
        } else {
            
            cookie += content;
        }
        
        if((!path || path === '') && this.hasValue('site-path')) {

            path = ion.getValue('site-path');
        }       
        
        if((!domain || domain === '') && this.hasValue('site-domain')) {

            domain = ion.getValue('site-domain');
        }            
        
        cookie += ";";

        if(expires && expires !== '') {
        
            cookie += 'expires=' + expires + ';';
        }
        
        if(path && path !== '') {
            
            cookie += 'path=' + path + ';';
        }

        if(domain && domain !== '') {
            
            cookie += 'domain=.' + domain + ';';
        }

        document.cookie = cookie;
        
        return this;
    },
    
    
    this.getCookie = function(name) {
        
        name = name + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        
        for(var i = 0; i < ca.length; i++) {
            
            var c = ca[i];
            
            while (c.charAt(0) == ' ') {
                
                c = c.substring(1);
            }
            
            if (c.indexOf(name) == 0) {
                
                return decodeURIComponent(c.substring(name.length, c.length));
            }
        }
        
        return null;
    },
    
    this.hasCookie = function(name) {
      
        return (this.getCookie(name) !== null);
    },    
    
    this.setCookieValue = function(name, key, value, expiry, path, domain) {
      
        var cookie = this.getCookie(name);
        var obj = {};
        
        if(cookie) {
            
            obj = JSON.parse(cookie);     
            
            if(!expiry) {

                expiry = null;
            }

            if(!path | path === '') {

                path = null;
                
                if(this.hasValue('site-path')) {
                    
                    path = ion.getValue('site-path');
                }                
            }

            if(!domain || domain === '') {

                domain = null;
                
                if(this.hasValue('site-domain')) {
                    
                    domain = ion.getValue('site-domain');
                }                      
            }            
        }

        obj[this.toCamelCase(key)] = value;

//        console.groupCollapsed('setCookieValue');
//        console.dir(cookie);
//        console.dir(obj);
//        console.groupEnd();

                    
        this.setCookie(name, obj, expiry, path, domain);        
        
        return this;
    },
    
    this.getCookieValue = function(name, key, value) {
        
        var cookie = this.getCookie(name);
        
//        console.groupCollapsed('getCookieValue (DOESNT EXIST)');
//        console.dir(cookie);
//        console.groupEnd();        
        
        if(!cookie) {
            
            return null;
        }        
        
        var obj = JSON.parse(cookie);

//        console.groupCollapsed('getCookieValue (EXISTS)');
//        console.dir(cookie);
//        console.dir(obj);
//        console.log(obj[this.toCamelCase(key)]);
//        console.groupEnd();        

        
        if(obj) {
            
            return obj[this.toCamelCase(key)];
        }
        
        return null;
    },
    
    this.hasCookieValue = function(name, key) {
      
        return !this.isEmpty(this.getCookieValue(name, key));
    },
    
//    this.toDashedCase = function(string, dash) {
//        
//    },

    this.toCamelCase = function(string) {
        
        return string.toLowerCase().replace(/-(.)/g, function(match, group1) {
            
            return group1.toUpperCase();
        });
    },
    
    this.ajax = function () {

//var r = new XMLHttpRequest();
//r.open("POST", "path/to/api", true);
//r.onreadystatechange = function () {
//  if (r.readyState != 4 || r.status != 200) return;
//  alert("Success: " + r.responseText);
//};
//r.send("banana=yellow");        
    },
    this.loadScript = function(src, onLoad) {

        if (!src) {

            throw new Error("The script source URI (src) needs to be specified.");
        }                

        var script = document.createElement('script');                

        script.setAttribute("src", src);

        if(onLoad)
            script.onload = onLoad;

        document.head.appendChild(script);
    },
    this.loadStyle = function(href, onLoad) {

        if (!href) {

            throw new Error("The style source URI (href) needs to be specified.");
        }                                

        var style = document.createElement('link');

        style.setAttribute("rel", "stylesheet");
        style.setAttribute("type", "text/css");
        style.setAttribute("href", href);

        if(onLoad) 
            style.onload = onLoad;

        document.head.appendChild(style);
    }

    this.__construct();

}).apply(ion);