/* 
 * See license information at the package root in LICENSE.md
 */

try { ion; } catch(e) {
    if(e instanceof ReferenceError) {        
        throw new Error("The 'ion' module has not been initialized.");
    }
}
  
    
ion.registerModule('navigation', function() { 

    this.items = [];
    this.navigation = false;
    this.scrollY = 0;
    this.ticking = false;
    this.docked = false;
    this.initialTop = false;
    this.searchActive = false;
    this.searchInterval = null;
    
    this.elements = {
        
        'outer-container': null,
        'inner-container': null,
        'root-menu': null        
    };

    this.__construct = function() {
      
//        console.log('Navigation::__construct()');

        var self = this;

        ion.whenContentLoaded(function(event, obj) { 

            var nav = document.getElementById('ion-navigation');
            
            if(nav) {

                obj.navigation = true;
                
                obj.elements['outer-container'] = { 'element': nav };
               
                var body = document.body;
               
                if(nav.childNodes.length > 0) {
                
                    var div = nav.querySelector('div');

                    if(div.nodeName.toLowerCase() === 'div') {
                        
                        obj.elements['inner-container'] = { 'element': div };
                    }                        
                }
                
                if(body) {   

                    var panels = document.querySelectorAll('#ion-navigation-large'); // #ion-navigation div.ion-navigation

                    //console.log(panels);

//console.log(ion.getScrollTopElement());
//console.log(ion.getScrollTopElement().scrollTop);

                    var fnPanel = function(panel) {
                        
                        var panelBackground = panel.querySelectorAll('.ion-navigation-background');
                        
                        if(panelBackground.length > 0) {
                            
                            panelBackground = panelBackground[0];
                        }
                        
                        var panelContent = panel.querySelectorAll('.ion-navigation-content');
                        
                        if(panelContent.length > 0) {
                            
                            panelContent = panelContent[0];
                        }
                        
//                        console.log(panelBackground);
//                        console.log(panelContent);
                        
                        // sets a class for the initial nav position

                        var data = JSON.parse(panel.dataset.object);
                        
                        if(!data) {
                            
                            return;
                        }

                        if(self.scrollY === 0) {

                            if(!ion.hasClass(panel, 'ion-navigation-top')) {

                                ion.addClass(panel, 'ion-navigation-top');
                            }
                            
                        } else {

                            if(ion.hasClass(panel, 'ion-navigation-top')) {

                                ion.removeClass(panel, 'ion-navigation-top');
                            }                            
                        }                                                
                        
                        if(data.cssClasses) {
                            if(data.cssClasses.default !== 'undefined' && data.cssClasses.initial !== 'undefined') {

                                if(Array.isArray(data.cssClasses.default.primary) && Array.isArray(data.cssClasses.default.contrast) && Array.isArray(data.cssClasses.default.common) &&
                                   Array.isArray(data.cssClasses.initial.primary) && Array.isArray(data.cssClasses.initial.contrast) && Array.isArray(data.cssClasses.initial.common)
                                )
                           
                                {

                                    if(data.cssClasses.initial.primary.length > 0) {
                                        
                                        if(self.scrollY === 0) {

                                            ion.forEach(data.cssClasses.default.primary, function(cssClass) {

                                                if(ion.hasClass(panelBackground, cssClass)) {

                                                    ion.removeClass(panelBackground, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.default.contrast, function(cssClass) {

                                                if(ion.hasClass(panelContent, cssClass)) {

                                                    ion.removeClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.default.common, function(cssClass) {

                                                if(ion.hasClass(panelContent, cssClass)) {

                                                    ion.removeClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });


                                            ion.forEach(data.cssClasses.initial.primary, function(cssClass) {

                                                if(!ion.hasClass(panelBackground, cssClass)) {

                                                    ion.addClass(panelBackground, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.initial.contrast, function(cssClass) {

                                                if(!ion.hasClass(panelContent, cssClass)) {

                                                    ion.addClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });      

                                            ion.forEach(data.cssClasses.initial.common, function(cssClass) {

                                                if(!ion.hasClass(panelContent, cssClass)) {

                                                    ion.addClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });                                               
                                        }
                                        else {

                                            ion.forEach(data.cssClasses.initial.primary, function(cssClass) {

                                                if(ion.hasClass(panelBackground, cssClass)) {

                                                    ion.removeClass(panelBackground, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.initial.contrast, function(cssClass) {

                                                if(ion.hasClass(panelContent, cssClass)) {

                                                    ion.removeClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.initial.common, function(cssClass) {

                                                if(ion.hasClass(panelContent, cssClass)) {

                                                    ion.removeClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });                                        


                                            ion.forEach(data.cssClasses.default.primary, function(cssClass) {

                                                if(!ion.hasClass(panelBackground, cssClass)) {

                                                    ion.addClass(panelBackground, cssClass);
                                                }  
                                                return;
                                            });

                                            ion.forEach(data.cssClasses.default.contrast, function(cssClass) {

                                                if(!ion.hasClass(panelContent, cssClass)) {

                                                    ion.addClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });                  

                                            ion.forEach(data.cssClasses.default.common, function(cssClass) {

                                                if(!ion.hasClass(panelContent, cssClass)) {

                                                    ion.addClass(panelContent, cssClass);
                                                }  
                                                return;
                                            });    
                                        }
                                    } else {
                                        
                                        ion.forEach(data.cssClasses.default.common, function(cssClass) {

                                            if(!ion.hasClass(panelContent, cssClass)) {

                                                ion.addClass(panelContent, cssClass);
                                            }  
                                            return;
                                        });    
                                    }
                                 
                                    
                                }
                            }    
                        }                      
                        
                                    //console.log(self.scrollY, panel.offsetTop);
                                      
                                    // NOTE: the following code is not in use (yet) and requires fixes / testing
                                    // It should either dock / float a nav depending on whether there is some 
                                    // kind of hero above it.
                                    
//                                    if(self.scrollY >= panel.offsetTop) {
//
//                                        // We're at / past the top of the nav
//
//                                        if(!ion.hasClass(panel, 'ion-navigation-floating')) {
//
//                                            ion.addClass(panel, 'ion-navigation-floating');
//                                        } 
//                                        
//                                        if(ion.hasClass(panel, 'ion-navigation-docked')) {
//
//                                            ion.removeClass(panel, 'ion-navigation-docked');
//                                        }                                         
//
//                                    } else {
//
//                                        // We're before the top of the nav
//
//                                        if(!ion.hasClass(panel, 'ion-navigation-docked')) {
//
//                                            ion.addClass(panel, 'ion-navigation-docked');
//                                        } 
//                                        
//                                        if(ion.hasClass(panel, 'ion-navigation-floating')) {
//
//                                            ion.removeClass(panel, 'ion-navigation-floating');
//                                        }                                                
//                                    }                        
                        
    //                    var searchForm = document.querySelector('.ion-navigation-search > form');                    
                        var searchBtn = panel.querySelector('.ion-navigation-search-button > button');
                        var searchTxt = panel.querySelector('.ion-navigation-search-field > input');
                        
                        if(searchBtn && searchTxt) {

                            var closeFn = function() {
                              
                                if(!ion.isEmpty(searchTxt.value) || document.activeElement === searchTxt) {

                                    return;
                                }
                                
                                var element = document.getElementById('ion-navigation');                                

                                if(element) {

                                    if(!ion.hasClass(element, 'ion-navigation-search-active')) {

                                        return;
                                    }

                                    window.clearInterval(self.searchInterval);
                                    self.searchInterval = null;

                                    ion.removeClass(element, 'ion-navigation-search-active');                                        
                                }
                                return;
                            };
                            
//                            searchTxt.addEventListener('change', function(event) {
//                               
//                                if(!self.searchActive) {
//                                    
//                                    return;
//                                }
//                                                                                                
//                                return;
//                            });

                            searchBtn.addEventListener('click', function(event) {

                                if(self.searchInterval === null) {
                                    
                                    event.preventDefault();
                                    event.stopPropagation();
                                    
                                    var element = document.getElementById('ion-navigation');
                                    
                                    if(element) {
                                        
                                        if(ion.hasClass(element, 'ion-navigation-search-active')) {
                                           
                                            return;
                                        }

                                        self.searchInterval = window.setInterval(closeFn, 5000);
                                        
                                        ion.addClass(element, 'ion-navigation-search-active');  
                                        
                                        var input = document.querySelector('#ion-navigation-large .ion-navigation-search-field > input');
                                        
                                        if(input) {
                                        
                                            input.focus();
                                        }

                                    }
                                    
                                    return;
                                }
                                
                                
                                
                                
                                return;
                            });

//                            console.dir(searchBtn);

    //                        searchBtn.onclick = function(event) { event.preventDefault(); alert('onclick'); };
    //                        
    //                        searchForm.addEventListener('submit', function(event) {
    //
    //                            event.preventDefault();
    //                            event.stopPropagation();
    //                            
    //                            alert('submit click');
    //                            
    //                            return false;
    //                        });    
    //                        
    //                        searchForm.onsubmit = function(event) { event.preventDefault(); alert('onsubmit'); };
                        }           
                        
                        return;
                    };

                    if(!self.initialTop) {
                        
                        ion.forEach(panels, fnPanel);                        
                        self.initialTop = true;
                    }                    

                    window.addEventListener('scroll', function(event) {

                        self.scrollY = window.scrollY;

                        if (!self.ticking) {
                            
                            window.requestAnimationFrame(function() {
                                
                                ion.forEach(panels, fnPanel);                                
                                self.ticking = false;
                            });

                            self.ticking = true;
                        }
                        
                        return false;
                    });                    

                    if(!ion.hasClass(body, 'ion-navigation-closed')) {

                        ion.addClass(body, 'ion-navigation-closed');
                    } 

                    var openBtns = document.querySelectorAll('.ion-navigation-open-button');
                    var closeBtns = document.querySelectorAll('.ion-navigation-close-button');

                    if(openBtns.length > 0 && closeBtns.length > 0) {

                        for(var i = 0; i < openBtns.length; i++) {
                            
                            openBtns[i].onclick = function() {

                                if(ion.hasClass(body, 'ion-navigation-closed')) {

                                    ion.removeClass(body, 'ion-navigation-closed');
                                }
                                
                                if(!ion.hasClass(body, 'ion-navigation-opened')) {

                                    ion.addClass(body, 'ion-navigation-opened');
                                }                                
                            };
                        }
                        
                       for(var i = 0; i < closeBtns.length; i++) {
                            
                            closeBtns[i].onclick = function() {

                                if(ion.hasClass(body, 'ion-navigation-opened')) {

                                    ion.removeClass(body, 'ion-navigation-opened');
                                }
                                
                                if(!ion.hasClass(body, 'ion-navigation-closed')) {

                                    ion.addClass(body, 'ion-navigation-closed');
                                }     
                            };
                        }                        
            
                    }
                }
            }
            


        }, this);
    },
    
    this.getElements = function() {
      
        if(!this.hasNavigation()) {
            
            throw new Error("The page does not contain markup for navigation.");
        }
      
        return this.elements;
    },
    
    this.getItems = function() {
        
      
        if(!this.hasNavigation()) {
            
            throw new Error("The page does not contain markup for navigation.");
        }
              
        
        return this.items;
    },
    
    this.hasNavigation = function() {
      
        return this.navigation;
    },
    
    
    
//    this.getScrollThreshold = function () {
//
//        var obj = $("#navigation-container");
//
//        if (obj) {
//            
//            var position = obj.position();
//
//            if (position) {
//                
//                return position.top;
//            }
//        }
//
//        return null;
//    },
//
//
//    this.isOverScrollThreshold = function () {
//
//        var self = this;
//
//        var threshold = this.getScrollThreshold();
//
//        //console.log("Threshold: ", threshold);
//
//        if (threshold !== null) {
//            
//            var scrollTop = $(this.__scrollTopElement).scrollTop();
//
//            //console.log("Threshold: element(", self.__scrollTopElement ,") scrollTop(", scrollTop, ") >= getThreshold(", threshold, ")");
//
//            if (scrollTop >= threshold)
//                return true;
//        }
//
//        return false;
//    },


    
    
    this.__construct();    
});
