/* 
 * See license information at the package root in LICENSE.md
 */

try {
    ion;
} catch (e) {
    if (e instanceof ReferenceError) {
        throw new Error("The 'ion' module has not been initialized.");
    }
}


ion.registerModule('components', function () {

    this.components = {};

    this.__construct = function () {

        ion.onContentLoaded(function (event, obj) {

            obj.update();

        }, this);
    },
    this.getComponents = function () {

        return this.components;
    },
    this.getComponent = function (componentId) {


        if (Object.prototype.hasOwnProperty.call(this.components, componentId)) {

            return this.components[componentId];
        }

        throw new Error('Component "' + componentId + '" does not exist.');
    },
    this.hasComponents = function () {

        return (this.components.keys().length > 0);
    },
    this.update = function () {

        ion.whenContentLoaded(function (event, obj) {

            var components = document.querySelectorAll('[data-component|="ion/"]');

            ion.forEach(components, function (componentElement) {

                if (!ion.isEmpty(componentElement.attributes.id.nodeValue)) {
                    
                
                    var component = {

                        element: null,

                        initialize: function (formElement) {

                            this.element = formElement;
                            return this;
                        },

                        getId: function () {

                            if (ion.isEmpty(this.element.attributes.id.nodeValue)) {

                                return null;
                            }

                            return this.element.attributes.id.nodeValue;                                  
                        },

                        getElement: function () {

                            return this.element;
                        }
                    };

                    component = component.initialize(componentElement);
                    obj.components[component.getId()] = component;

//                component.getElement().addEventListener('submit', function(event) {
//
//                    event.preventDefault();
//
//                    return false;
//                });

                } else {

                    ion.console.groupCollapsed('Skipped component due to the fact that no unique "id" attribute was specified.');
                    ion.console.dir(component);
                    ion.console.groupEnd();
                }
 
            }, obj);

        }, this);
    };

    this.__construct();
});
