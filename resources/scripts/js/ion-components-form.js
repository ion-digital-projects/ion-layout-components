/* 
 * See license information at the package root in LICENSE.md
 */

try {
    ion;
} catch (e) {
    if (e instanceof ReferenceError) {
        throw new Error("The 'ion' module has not been initialized.");
    }
}


ion.registerModule('forms', function () {

    this.submitUri = null;
    this.forms = {};

    this.__construct = function () {


        ion.onContentLoaded(function (event, obj) {

            obj.update();

        }, this);
    },
    this.setSubmitUri = function (uri) {

        this.submitUri = uri;
        return this;
    },
    this.getSubmitUri = function () {

        return this.submitUri;
    },
    this.hasSubmitUri = function () {

        return (this.submitUri !== null);
    },
    this.getForms = function () {

        return this.forms;
    },
    this.getForm = function (formId) {


        if (Object.prototype.hasOwnProperty.call(this.forms, formId)) {

            return this.forms[formId];
        }

        throw new Error('Form "' + formId + '" does not exist.');
    },
    this.hasForms = function () {

        return (this.forms.keys().length > 0);
    },
    this.update = function () {

        ion.whenContentLoaded(function (event, obj) {

            var forms = document.querySelectorAll("form[data-component='ion/form']");

            ion.forEach(forms, function (formElement) {

                if (!ion.isEmpty(formElement.attributes.id.nodeValue)) {

                    var form = {

                        element: null,
                        submitHook: null,
                        enabled: true,

                        initialize: function (formElement) {

                            this.element = formElement;
                            return this;
                        },

                        getId: function () {

                            if (ion.isEmpty(this.element.attributes.id.nodeValue)) {

                                return null;
                            }

                            return this.element.attributes.id.nodeValue;                                  
                        },

                        getName: function () {

                            if (ion.isEmpty(this.element.dataset.name)) {

                                if (ion.isEmpty(this.element.attributes.name.nodeValue)) {

                                    return null;
                                }

                                return this.element.attributes.name.nodeValue;
                            }

                            return this.element.dataset.name;
                        },

                        getElement: function () {

                            return this.element;
                        },

                        setAction: function (action) {

                            this.element.attributes.action.nodeValue = action;
                            return this;
                        },

                        getAction: function () {

                            if (ion.isEmpty(this.element.attributes.action.nodeValue)) {

                                return null;
                            }

                            return this.element.attributes.action.nodeValue;
                        },

                        hasAction: function () {

                            return (this.getAction() !== null);
                        },

                        setMethod: function (method) {

                            this.element.attributes.action.nodeValue = method;
                            return this;
                        },

                        getMethod: function () {

                            if (ion.isEmpty(this.element.attributes.method.nodeValue)) {

                                return null;
                            }

                            return this.element.attributes.method.nodeValue;
                        },

                        hasMethod: function () {

                            return (this.getMethod() !== null);
                        },

                        isAjaxEnabled: function () {

                            return ion.isAttributeFlagSet(this.element, 'data-ajax');
                        },

                        disable: function (onDone) {

                            if (this.enabled) {

                                this.enabled = false;
                                
                                if(onDone)
                                    onDone();                                
                            }
                        },

                        enable: function (onDone) {

                            if (!this.enabled) {

                                this.enabled = true;
                                
                                if(onDone)
                                    onDone();
                            }
                        },

                        isEnabled: function () {

                            return this.enabled === true;
                        },
                        
                        setSubmitHook: function(hook) {
                            
                            this.submitHook = hook;
                            return this;
                        },
                        
                        getSubmitHook: function() {
                            
                            return this.submitHook;
                        },
                        
                        hasSubmitHook: function() {
                            
                            return (this.submitHook !== null);
                        },
                        

                        submit: function (force, onDone) {

//                            console.log('submit', this.isEnabled());

                            if (this.isEnabled() || force) {

//                                console.log('submitting');
                                
                                var self = this;
                                
                                this.disable(function() {
                                
                                    
                                    var closure = function(obj) {
                                        
//                                        console.log(obj.isAjaxEnabled());
//                                        throw new Error("WHOOP");    
                    
                                        if(!obj.isAjaxEnabled()) {
                                            
                                            obj.getElement().submit();

                                            obj.enable(function() {

                                                if (onDone)
                                                    onDone();           
                                            });
                                            return;
                                        }
                                        //console.log('SUBMITTING (with hook)' + self.getId());

                                        throw new Error("TODO: AJAX submission has not been implemented yet.");
                                        
                                        return;
                                    };
                                
                                    if(self.getSubmitHook() !== null) {

                                        self.getSubmitHook()(self.getElement(), function() {

                                            closure(self);
                                        });

                                        return self;
                                    } 

                                    closure(self);
                                });
                            }
                            
                            return this;
                        }
                    };

                    form = form.initialize(formElement);
                    obj.forms[form.getId()] = form;

                    form.getElement().addEventListener('submit', function(event) {
                       
                        event.preventDefault();

                        var target = event.target || event.srcElement || event.originalTarget;

                        if(!target) {

                            throw new Error('Could not submit form - could not determine the target <form /> element.');
                        }

                        var formId = target.attributes.id.nodeValue;

                        if(ion.isEmpty(formId)) {

                            throw new Error('Could not submit form - could not determine the <form /> element\'s client ID (please specify the ID attribute).');
                        }                       

                        var form = ion.forms.getForm(formId);
                        
                        form.submit(false, function() {
                           
                            //alert('submitted');
                        });
                        
                        return false;
                    });

                } else {

                    ion.console.groupCollapsed('Skipped form due to the fact that no unique "id" attribute was specified.');
                    ion.console.dir(form);
                    ion.console.groupEnd();
                }
            }, obj);

            //console.dir(obj.forms);

        }, this);
    };

    this.__construct();
});
