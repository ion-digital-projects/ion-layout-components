<?php

namespace ion\Rendering\Layout;

use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;

interface ComponentInterface extends TemplateInterface, DescriptorInterface {

    static function create(

        string $clientId = null,
        RenderOptionsInterface $renderOptions = null,
        TemplateInterface $parent = null,
        CallableMapBaseInterface $hooks = null

    ): TemplateInterface;

    static function createFromDescriptor(

        string $clientId,
        DescriptorInterface $descriptor,
        RenderOptionsInterface $renderOptions = null,
        TemplateInterface $parent = null,
        CallableMapBaseInterface $hooks = null

    ): ?ComponentInterface;

    static function createFromTemplate(TemplateInterface $template, string $clientId = null): TemplateInterface;

}
