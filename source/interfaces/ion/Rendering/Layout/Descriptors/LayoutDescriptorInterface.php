<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Types\StringInterface;
use \ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use \ion\Rendering\Layout\Descriptors\ModalDescriptorVectorInterface;

interface LayoutDescriptorInterface extends ColourPropertyInterface, ImagePropertyInterface {

    function getModals(): ModalDescriptorVectorInterface;

    function getNavigation(): NavigationSectionDescriptorInterface;

    function getHeader(): HeaderSectionDescriptorInterface;

    function getMain(): MainSectionDescriptorInterface;

    function getFooter(): FooterSectionDescriptorInterface;

    function setTitle(string $title = null): LayoutDescriptorInterface;

    function getTitle(): ?StringInterface;

    function hasTitle(): bool;

    function setFullHeight(bool $fullHeight = null): LayoutDescriptorInterface;

    function getFullHeight(): ?bool;

    function hasFullHeight(): bool;

}
