<?php

namespace ion\Rendering\Layout\Descriptors\People;

use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\TextPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\NamesPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\DisplayNamePropertyInterface;

interface PersonDescriptorInterface extends 

    UriPropertyInterface,
    TextPropertyInterface,
    NamesPropertyInterface,
    DisplayNamePropertyInterface

 {

    // No public methods!

}
