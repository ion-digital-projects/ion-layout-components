<?php

namespace ion\Rendering\Layout\Descriptors\Lists;

use \ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;

interface ListDescriptorMapInterface {

    /**
     *
     * Set and associate a value in the map with a key.
     *
     * @param ListDescriptorInterface $key The key to be associated with the value.
     * @param ListDescriptorInterface $value The string value to be defined in the map.
     * @return MapInterface The modified map.
     *
     */

    function set(string $key, ListDescriptorInterface $value): ListDescriptorMapInterface;

    /**
     *
     * Checks whether a map contains either just a key, or a key and value combination.
     *
     * @param ListDescriptorInterface $value The string value to look for in the map.
     * @param ?string $key Combine the value with a key and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the key and/or value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(ListDescriptorInterface $value, string $key = null): bool;

    /**
     *
     * Remove all references of a value from the map
     *
     * @param ListDescriptorInterface $value The value to look for in the map.
     * @return  The modified map
     *
     */

    function removeValue(ListDescriptorInterface $value = null): ListDescriptorMapInterface;

}
