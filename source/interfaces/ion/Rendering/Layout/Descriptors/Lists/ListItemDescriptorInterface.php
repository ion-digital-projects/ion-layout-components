<?php

namespace ion\Rendering\Layout\Descriptors\Lists;

use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\TagPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\UriActionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\IconPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\OrderPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;

interface ListItemDescriptorInterface extends 

    CaptionPropertyInterface,
    TagPropertyInterface,
    UriPropertyInterface,
    UriActionPropertyInterface,
    IconPropertyInterface,
    OrderPropertyInterface,
    ListItemContainerInterface

 {

    function getParentContainer(): ListItemContainerInterface;

    function setIndex(int $index = null): ListItemDescriptorInterface;

    function getIndex(): ?int;

    function hasIndex(): bool;

}
