<?php

namespace ion\Rendering\Layout\Descriptors\Lists;

use \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;

interface ListItemDescriptorVectorInterface {

    /**
     *
     * Add a value to the end of the list.
     *
     * @param ListItemDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */

    function add(ListItemDescriptorInterface $value): ListItemDescriptorVectorInterface;

    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param ListItemDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */

    function insert(int $index, ListItemDescriptorInterface $value): ListItemDescriptorVectorInterface;

    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param ListItemDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */

    function set(int $index, ListItemDescriptorInterface $value): ListItemDescriptorVectorInterface;

    /**
     *
     * Checks whether a list contains a value.
     *
     * @param ListItemDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(ListItemDescriptorInterface $value, int $index = null): bool;

    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param ListItemDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */

    function removeValue(ListItemDescriptorInterface $value = null): ListItemDescriptorVectorInterface;

    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */

    function addVector(ListItemDescriptorVectorBaseInterface $values): ListItemDescriptorVectorInterface;

    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?ListItemDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */

    function strip(ListItemDescriptorVectorBaseInterface $values = null): ListItemDescriptorVectorInterface;

}
