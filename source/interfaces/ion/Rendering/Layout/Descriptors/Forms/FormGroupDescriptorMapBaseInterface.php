<?php

namespace ion\Rendering\Layout\Descriptors\Forms;

interface FormGroupDescriptorMapBaseInterface {

    /**
     *
     * Return a value from the map by key.
     *
     * @param FormGroupDescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */

    function get(string $key): ?object;

}
