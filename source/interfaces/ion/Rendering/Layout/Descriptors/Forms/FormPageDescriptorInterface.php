<?php

namespace ion\Rendering\Layout\Descriptors\Forms;

use \ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;

interface FormPageDescriptorInterface extends CaptionPropertyInterface {

    function getForm(): FormDescriptorInterface;

    function getGroups(): FormGroupDescriptorVectorBaseInterface;

}
