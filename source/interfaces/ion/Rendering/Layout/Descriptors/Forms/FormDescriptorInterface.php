<?php

namespace ion\Rendering\Layout\Descriptors\Forms;

use \ion\Types\Arrays\MapBaseInterface;
use \ion\Types\StringInterface;
use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorVectorBaseInterface;

interface FormDescriptorInterface extends 

    CaptionPropertyInterface,
    UriPropertyInterface,
    NamePropertyInterface,
    ColourPropertyInterface

 {

    function isAjaxEnabled(): bool;

    function isCaptchaEnabled(): bool;

    function setSubmitCaption(string $submitCaption = null): FormDescriptorInterface;

    function getSubmitCaption(): ?StringInterface;

    function hasSubmitCaption(): bool;

    function getPages(): FormPageDescriptorVectorBaseInterface;

    function getGroups(): FormGroupDescriptorVectorBaseInterface;

    function getFields(): FormFieldDescriptorVectorBaseInterface;

    function getAdditionalValues(): MapBaseInterface;

}
