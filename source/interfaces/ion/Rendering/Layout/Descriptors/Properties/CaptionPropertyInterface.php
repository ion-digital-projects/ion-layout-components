<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\StringInterface;

interface CaptionPropertyInterface {

    function getCaption(): ?StringInterface;

    function setCaption(string $caption = null): CaptionPropertyInterface;

    function hasCaption(): bool;

    function setCaptionVisible(

        bool $default = null,
        bool $onLargeScreens = null,
        bool $onMediumScreens = null,
        bool $onSmallScreens = null

    ): CaptionPropertyInterface;

    function isCaptionVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null): ?bool;

}
