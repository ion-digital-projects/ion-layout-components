<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\StringInterface;

interface TagPropertyInterface {

    function getTag(): ?StringInterface;

    function setTag(string $tag = null): TagPropertyInterface;

    function hasTag(): bool;

}
