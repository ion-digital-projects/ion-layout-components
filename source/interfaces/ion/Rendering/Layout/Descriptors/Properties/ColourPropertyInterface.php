<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Rendering\Layout\ColourInterface;

interface ColourPropertyInterface {

    function setColour(ColourInterface $colour = null): ColourPropertyInterface;

    function getColour(): ?ColourInterface;

    function hasColour(): bool;

}
