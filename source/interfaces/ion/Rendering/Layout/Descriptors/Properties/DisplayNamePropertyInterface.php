<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\StringInterface;

interface DisplayNamePropertyInterface {

    function getDisplayName(): ?StringInterface;

    function setDisplayName(string $displayName = null): DisplayNamePropertyInterface;

    function hasDisplayName(): bool;

}
