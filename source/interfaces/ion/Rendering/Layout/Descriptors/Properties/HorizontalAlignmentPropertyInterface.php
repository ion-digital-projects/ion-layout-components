<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Rendering\Layout\HorizontalAlignmentTypeInterface;

interface HorizontalAlignmentPropertyInterface {

    function setHorizontalAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null): HorizontalAlignmentPropertyInterface;

    function getHorizontalAlignment(): HorizontalAlignmentTypeInterface;

    function hasHorizontalAlignment(): bool;

}
