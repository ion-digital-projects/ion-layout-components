<?php

namespace ion\Rendering\Layout\Descriptors\Media;

use \ion\Rendering\Layout\ColourInterface;
use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;

interface ImageDescriptorInterface extends MediaDescriptorInterface {

    function setRound(bool $round = null): ImageDescriptorInterface;

    function getRound(): ?bool;

    function isRound(): bool;

    function setColour(ColourInterface $colour = null): ImageDescriptorInterface;

    function getColour(): ?ColourInterface;

    function hasColour(): bool;

}
