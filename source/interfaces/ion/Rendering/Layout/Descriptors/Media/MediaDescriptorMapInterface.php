<?php

namespace ion\Rendering\Layout\Descriptors\Media;

use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;

interface MediaDescriptorMapInterface {

    /**
     *
     * Set and associate a value in the map with a key.
     *
     * @param MediaDescriptorInterface $key The key to be associated with the value.
     * @param MediaDescriptorInterface $value The string value to be defined in the map.
     * @return MapInterface The modified map.
     *
     */

    function set(string $key, MediaDescriptorInterface $value): MediaDescriptorMapInterface;

    /**
     *
     * Checks whether a map contains either just a key, or a key and value combination.
     *
     * @param MediaDescriptorInterface $value The string value to look for in the map.
     * @param ?string $key Combine the value with a key and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the key and/or value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(MediaDescriptorInterface $value, string $key = null): bool;

    /**
     *
     * Remove all references of a value from the map
     *
     * @param MediaDescriptorInterface $value The value to look for in the map.
     * @return  The modified map
     *
     */

    function removeValue(MediaDescriptorInterface $value = null): MediaDescriptorMapInterface;

}
