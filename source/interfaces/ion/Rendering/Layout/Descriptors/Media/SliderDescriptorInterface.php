<?php

namespace ion\Rendering\Layout\Descriptors\Media;

use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorVectorBaseInterface;

interface SliderDescriptorInterface extends MediaDescriptorInterface, NamePropertyInterface {

    function getMedia(): MediaDescriptorVectorBaseInterface;

}
