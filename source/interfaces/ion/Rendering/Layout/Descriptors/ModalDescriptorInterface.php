<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Types\Arrays\Specialized\StringMapBaseInterface;
use \ion\Rendering\Layout\ColourInterface;
use \ion\Types\StringInterface;
use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyInterface;
use \ion\Rendering\Layout\Descriptors\ModalType;
use \ion\Rendering\Layout\Descriptors\ModalActivationType;

interface ModalDescriptorInterface extends 

    CaptionPropertyInterface,
    NamePropertyInterface,
    ColourPropertyInterface,
    HorizontalAlignmentPropertyInterface

 {

    function setActivationFrequency(int $frequency = null): ModalDescriptorInterface;

    function getActivationFrequency(): ?int;

    function hasActivationFrequency(): bool;

    function setCookie(string $cookie = null): ModalDescriptorInterface;

    function getCookie(): ?StringInterface;

    function hasCookie(): bool;

    function setButtonColour(ColourInterface $colour = null): ModalDescriptorInterface;

    function getButtonColour(): ?ColourInterface;

    function hasButtonColour(): bool;

    function getButtons(): StringMapBaseInterface;

    function hasButtons(): bool;

    function getModalType(): ?ModalType;

    function setModalType(ModalType $modalType): ModalDescriptorInterface;

    function getModalActivationType(): ModalActivationType;

    function setModalActivationType(ModalActivationType $modalActivationType): ModalDescriptorInterface;

    function setModalDelay(int $delay): ModalDescriptorInterface;

    function getModalDelay(): int;

    function setModalAllowMultipleActivations(bool $allowMultipleActivations): ModalDescriptorInterface;

    function getModalAllowMultipleActivations(): bool;

    function setModalContentPadding(bool $modalContentPadding = null): ModalDescriptorInterface;

    function getModalContentPadding(): ?bool;

    function hasModalContentPadding(): bool;

}
