<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\MediaPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\VerticalAlignmentPropertyInterface;

interface HeroDescriptorInterface extends 

    CaptionPropertyInterface,
    MediaPropertyInterface,
    UriPropertyInterface,
    VerticalAlignmentPropertyInterface

 {

    function setUriVisible(bool $uriVisible = null): HeroDescriptorInterface;

    function getUriVisible(): ?bool;

    function isUriVisible(): bool;

}
