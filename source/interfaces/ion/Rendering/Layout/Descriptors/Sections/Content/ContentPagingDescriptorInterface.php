<?php

namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use \ion\System\Remote\UriInterface;
use \ion\System\Remote\UriMapBaseInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;

interface ContentPagingDescriptorInterface extends ColourPropertyInterface {

    function setPageSize(int $pageSize = null): ContentPagingDescriptorInterface;

    function getPageSize(): ?int;

    function hasPageSize(): bool;

    function setEndSize(int $endSize = null): ContentPagingDescriptorInterface;

    function getEndSize(): ?int;

    function hasEndSize(): bool;

    function setNextPreviousEnabled(bool $nextPrevious): ContentPagingDescriptorInterface;

    function isNextPreviousEnabled(): bool;

    function getNextLink(): ?UriInterface;

    function getPreviousLink(): ?UriInterface;

    function setItemCount(int $itemCount): ContentPagingDescriptorInterface;

    function getItemCount(): int;

    function getPageCount(): int;

    function setCurrentPage(int $pageIndex): ContentPagingDescriptorInterface;

    function getCurrentPage(): int;

    function getPageLinks(): UriMapBaseInterface;

}
