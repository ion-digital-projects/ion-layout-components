<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Rendering\Layout\Descriptors\ModalDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;

interface ModalDescriptorVectorInterface {

    /**
     *
     * Add a value to the end of the list.
     *
     * @param ModalDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */

    function add(ModalDescriptorInterface $value): ModalDescriptorVectorInterface;

    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param ModalDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */

    function insert(int $index, ModalDescriptorInterface $value): ModalDescriptorVectorInterface;

    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param ModalDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */

    function set(int $index, ModalDescriptorInterface $value): ModalDescriptorVectorInterface;

    /**
     *
     * Checks whether a list contains a value.
     *
     * @param ModalDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(ModalDescriptorInterface $value, int $index = null): bool;

    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param ModalDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */

    function removeValue(ModalDescriptorInterface $value = null): ModalDescriptorVectorInterface;

    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */

    function addVector(ModalDescriptorVectorBaseInterface $values): ModalDescriptorVectorInterface;

    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?ModalDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */

    function strip(ModalDescriptorVectorBaseInterface $values = null): ModalDescriptorVectorInterface;

}
