<?php

namespace ion\Rendering\Layout\Components\Forms\Fields;

use \ion\Rendering\Layout\Components\Forms\FormFieldComponentInterface;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptorInterface;

interface TextFormFieldComponentInterface extends FormFieldComponentInterface, TextFormFieldDescriptorInterface {

    // No public methods!

}
