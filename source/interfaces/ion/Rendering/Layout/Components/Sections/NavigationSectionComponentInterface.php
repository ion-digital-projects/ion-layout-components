<?php

namespace ion\Rendering\Layout\Components\Sections;

use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;

interface NavigationSectionComponentInterface extends ComponentInterface, NavigationSectionDescriptorInterface {

    // No public methods!

}
