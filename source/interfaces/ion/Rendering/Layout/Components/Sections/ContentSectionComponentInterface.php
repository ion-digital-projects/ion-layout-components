<?php

namespace ion\Rendering\Layout\Components\Sections;

use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;

interface ContentSectionComponentInterface extends ComponentInterface, MainSectionDescriptorInterface {

    // No public methods!

}
