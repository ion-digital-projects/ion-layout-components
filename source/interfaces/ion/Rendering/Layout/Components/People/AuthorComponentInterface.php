<?php

namespace ion\Rendering\Layout\Components\People;

use \ion\Rendering\Layout\Descriptors\People\AuthorDescriptorInterface;

interface AuthorComponentInterface extends AuthorDescriptorInterface {

    // No public methods!

}
