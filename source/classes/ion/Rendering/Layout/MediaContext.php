<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout;

/**
 * Description of UriAction
 *
 * @author Justus
 */

use \ion\Types\EnumObject;

final class MediaContext extends EnumObject implements MediaContextInterface {
    
    public const NONE = 0;
    public const FOREGROUND = 1;
    public const BACKGROUND = 2;
    
}
