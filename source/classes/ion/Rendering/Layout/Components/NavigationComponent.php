<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components;

/**
 * Description of Header
 *
 * @author Justus
 */

use \ion\Rendering\RenderableVectorBaseInterface;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Template;
use \ion\Rendering\Layout\Component;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\TemplateManager;
use \ion\Rendering\Layout\Assets\StyleAsset;
use \ion\Rendering\Layout\Assets\ScriptAsset;
use \ion\System\Remote\UriInterface;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\WordPress\WordPressHelper as WP;
use \ion\Rendering\StringRenderable;
use \ion\Rendering\Markup\Html\HtmlHelper as HTML;
use \ion\Rendering\Layout\Components\Lists\ListComponent;
use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use \ion\Rendering\Layout\Components\Lists\ListComponentInterface;
use \ion\Types\Arrays\Specialized\ObjectVectorBase;
use \ion\Types\Arrays\Specialized\ObjectVectorBaseInterface;
use \ion\WordPress\Site\MenuComponent;
use \ion\WordPress\Identifier;
use \ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use \ion\Rendering\Layout\Descriptors\NavigationDescriptorInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\Rendering\Layout\ColourInterface;

class NavigationComponent extends Component implements NavigationComponentInterface {
    
    use \ion\Rendering\Layout\Descriptors\NavigationDescriptorTrait;
    
    public static function createWithMenu(
            
            string $clientId,
            ListItemContainerInterface $items,
            ImageDescriptorInterface $logo = null,
            ActionComponentInterface $action = null,
            ColourInterface $colour = null,
            ColourInterface $initialColour = null,
            bool $actionEnabled = null,
            bool $searchEnabled = null,
            string $searchParameterName = null,
            UriInterface $searchUri = null,
            bool $shadowEnabled = null,
            bool $initialShadowEnabled = null,
            bool $enableSmall = null,
            bool $enableMedium = null,
            bool $enableLarge = null,
            TemplateInterface $parent = null, 
            RenderOptionsInterface $renderOptions = null, 
            CallableMapBaseInterface $hooks = null) {               
        
        return new static(
                
            $clientId,
            $renderOptions,
            $parent,
            $hooks,
            $items,
            $logo,
            $action,
            $colour,
            $initialColour,
            $actionEnabled,
            $searchEnabled,
            $searchUri,
            $searchParameterName,
            $shadowEnabled,
            $initialShadowEnabled,
            $enableSmall,
            $enableMedium,
            $enableLarge
        );
    }
    

    
    public function __construct(
            
            string $clientId,
            RenderOptionsInterface $renderOptions = null, 
            TemplateInterface $parent = null, 
            CallableMapBaseInterface $hooks = null,
            ListItemContainerInterface $items = null,
            ImageDescriptorInterface $logo = null ,
            ActionComponentInterface $action = null,
            ColourInterface $colour = null,
            ColourInterface $initialColour = null,
            bool $actionEnabled = null,
            bool $searchEnabled = null,
            UriInterface $searchUri = null,
            string $searchParameterName = null,
            bool $shadowEnabled = null,
            bool $initialShadowEnabled = null,
            bool $enableSmall = null,
            bool $enableMedium = null,
            bool $enableLarge = null
    ) {               
        
        $this->setMenu($items);
        $this->setLogo($logo);        
        $this->setAction($action);
        $this->setColour($colour);
        $this->setInitialColour($initialColour); 
        
        if($actionEnabled !== null) {
            
            $this->setActionEnabled($actionEnabled);
        }
        
        if($searchEnabled !== null) {
            
            $this->setSearchEnabled($searchEnabled);
        }
        
        $this->setSearchUri($searchUri);
        $this->setSearchParameterName($searchParameterName);
        
        if($shadowEnabled !== null) {
            
            $this->setShadowEnabled($shadowEnabled);
        }
        
        if($initialShadowEnabled !== null) {
            
            $this->setShadowEnabledInitially($initialShadowEnabled);
        }
        
        $this->setSmallEnabled($enableSmall);
        $this->setMediumEnabled($enableMedium);
        $this->setLargeEnabled($enableLarge);
        

        parent::__construct($clientId, $renderOptions, $parent, $hooks); 
        
        if(!$this->getStyleAssets()->isLocked()) {        
        
            if(!$this->getStyleAssets()->hasKey('ion-components-navigation-colour')) {

                $alpha = ($this->hasColour() ? $this->getColour()->getOpacity() / 100 : 1.0);
                $initialAlpha = ($this->hasInitialColour() ? $this->getInitialColour()->getOpacity() / 100 : 1.0);
                
//                $this->getStyleAssets()->set(
//                        'ion-components-navigation-colour', 
//                        StyleAsset::inline(<<<CSS
//#ion-navigation .ion-navigation .ion-navigation-background {
//    opacity: {$alpha};
//}
//#ion-navigation .ion-navigation.ion-navigation-top .ion-navigation-background {
//    opacity: {$initialAlpha};
//}
//CSS
//                ));
            }
        }                 
    }
    
    protected function initialize(): ?TemplateInterface {

       
        
        TemplateManager::getInstance()->registerFeature('navigation');
        
        if($this->hasMenu()) {
            
            $this->getChildren()->add($this->getMenu());
        }
        
        if($this->hasAction()) {
            
            $this->getChildren()->add($this->getAction()->setRenderSeperately(true, $this));
        }   
        
        return $this;
    }

}
