<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms\Fields;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Component;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\Layout\Components\Forms\FormFieldComponent;

class EmailFormFieldComponent extends FormFieldComponent implements EmailFormFieldComponentInterface {
    
//    protected function createRenderables(TemplateVectorInterface $children): RenderableVectorInterface {
//        
//    }
    
    use \ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptorTrait;
}
