<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */

use \ion\System\Remote\UriInterface;
use \ion\Rendering\Layout\Descriptors\Descriptor;

class SliderDescriptor extends MediaDescriptor implements SliderDescriptorInterface {
        
    use SliderDescriptorTrait;
    
    public function __construct(
            
            MediaDescriptorVectorBaseInterface $media = null,
            UriInterface $uri = null,
            string $caption = null,
            string $text = null,
            bool $visible = null
        ) {
        
        parent::__construct($uri, $caption, $text, $visible);
        
        if($media !== null) {
            
            $this->getMedia()->addVector($media);
        }
    }   
}
