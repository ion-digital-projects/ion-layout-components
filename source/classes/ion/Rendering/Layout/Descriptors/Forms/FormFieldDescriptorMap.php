<?php

/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */

namespace ion\Rendering\Layout\Descriptors\Forms;

use \ion\Types\Arrays\MapBaseInterface;
use \ion\Types\Arrays\MapBase;
use \ion\Types\Arrays\VectorBaseInterface;
use \ion\Types\StringObjectInterface;
use \ion\Types\StringObject;


final class FormFieldDescriptorMap extends FormFieldDescriptorMapBase implements FormFieldDescriptorMapInterface {




    public function __construct(array &$values = null, bool $immutable = false) {
    
        parent::__construct($values, $immutable);
    }
                
             

    /**
     *
     * Set and associate a value in the map with a key.
     *
     * @param FormFieldDescriptorInterface $key The key to be associated with the value.
     * @param FormFieldDescriptorInterface $value The string value to be defined in the map.
     * @return MapInterface The modified map.
     *
     */

    public function set(string $key, FormFieldDescriptorInterface $value): FormFieldDescriptorMapInterface {
    
        return $this->_set($key, $value);
    }

    /**
     *
     * Checks whether a map contains either just a key, or a key and value combination.
     *
     * @param FormFieldDescriptorInterface $value The string value to look for in the map.
     * @param ?string $key Combine the value with a key and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the key and/or value exists, __FALSE__ otherwise.
     *
     */

    public function hasValue(FormFieldDescriptorInterface $value, string $key = null): bool {
    
        return $this->_hasValue($value, $key);
    }
    
    
    /**
     *
     * Remove all references of a value from the map
     *
     * @param FormFieldDescriptorInterface $value The value to look for in the map.
     * @return  The modified map
     *
     */
    
    public function removeValue(FormFieldDescriptorInterface $value = null): FormFieldDescriptorMapInterface {
    
        return $this->_removeValue($value);
    }   
     
}