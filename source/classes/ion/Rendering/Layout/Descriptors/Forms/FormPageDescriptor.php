<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormPageDescriptor
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Descriptors\Descriptor;
use \ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;

class FormPageDescriptor extends Descriptor implements FormPageDescriptorInterface {
    
    use FormPageDescriptorTrait;

    public function __construct(FormDescriptorInterface $form) {
        
        parent::__construct();
     
        $this->setForm($form);
    }        

    
}
