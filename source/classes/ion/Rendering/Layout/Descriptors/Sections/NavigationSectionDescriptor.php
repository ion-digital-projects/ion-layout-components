<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Sections;

/**
 * Description of Form
 *
 * @author Justus
 */



class NavigationSectionDescriptor extends SectionDescriptor implements NavigationSectionDescriptorInterface {
        
    use NavigationSectionDescriptorTrait;
    
    public function __construct() {
        
        parent::__construct();
        
    }   
}