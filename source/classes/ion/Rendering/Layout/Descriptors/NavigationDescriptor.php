<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptor
 *
 * @author Justus
 */

class NavigationDescriptor extends Descriptor implements NavigationDescriptorInterface {
    
    use NavigationDescriptorTrait;
}
