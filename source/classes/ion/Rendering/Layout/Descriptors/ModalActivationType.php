<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalType
 *
 * @author Justus
 */

use \ion\Types\EnumObject;

final class ModalActivationType extends EnumObject {
    
    public const NONE = 0;
    public const PAGE_LOAD = 1;
    public const MOUSE_EXIT = 2;
}
