<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of Form
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;
use \ion\Types\Arrays\Specialized\StringVectorInterface;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
use \ion\Rendering\Layout\ComponentException;
use \ion\Rendering\Layout\Descriptors\Properties;
use \ion\Rendering\Layout\ColourInterface;

trait LayoutDescriptorTrait {
        
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
        
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasBackgroundColour;
    }      
    
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait {
        
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::setImage as setBackgroundImage;
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::getImage as getBackgroundImage;
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::hasImage as hasBackgroundImage;
    }        
    
    private $navigation;
    private $header;
    private $main;
    private $footer;
    private $title;
    private $fullHeight;
    private $features;
    private $modals;
    
//    protected function setModal(ModalDescriptorInterface $descriptor = null): LayoutDescriptorInterface {
//        
//        $this->modals = $descriptor;
//        return $this;
//    }
    
    
    public function getModals(): ModalDescriptorVectorInterface { 
        
        if($this->modals === null) {
            
            $this->modals = ModalDescriptorVector::create();
        }
        
        return $this->modals;
    }    
    
    protected function setNavigation(NavigationSectionDescriptorInterface $descriptor): LayoutDescriptorInterface {
        
        $this->navigation = $descriptor;
        return $this;
    }
    
    public function getNavigation(): NavigationSectionDescriptorInterface {
        
        if($this->header === null) {
            
            throw new ComponentException("Navigation component not initialized.");
        }        
        
        return $this->navigation;
    }
    
    protected function setHeader(HeaderSectionDescriptorInterface $descriptor): LayoutDescriptorInterface {
        
        $this->header = $descriptor;
        return $this;
    }
    
    public function getHeader(): HeaderSectionDescriptorInterface {
        
        if($this->header === null) {
            
            throw new ComponentException("Header component not initialized.");
        }
        
        return $this->header;
    }
    
    protected function setMain(MainSectionDescriptorInterface $descriptor): LayoutDescriptorInterface {
        
        $this->main = $descriptor;
        return $this;
    }    
    
    public function getMain(): MainSectionDescriptorInterface {
        
        if($this->main === null) {
            
            throw new ComponentException("Content component not initialized.");
        }
        
        return $this->main;
    }
    
    protected function setFooter(FooterSectionDescriptorInterface $descriptor): LayoutDescriptorInterface {
        
        $this->footer = $descriptor;
        return $this;
    }    
    
    public function getFooter(): FooterSectionDescriptorInterface {
        
        if($this->footer === null) {
            
            throw new ComponentException("Footer component not initialized.");
        }
        
        return $this->footer;
    }
    
    public function setTitle(string $title = null): LayoutDescriptorInterface {
        
        if($title === null) {
            
            $this->title = null;
            return $this;
        }
        
        $this->title = StringObject::create($title);
        return $this;
    }
    
    public function getTitle(): ?StringInterface {
        
        return $this->title;
    }
    
    public function hasTitle(): bool {
        
        return ($this->getTile() !== null);
    }
    
    public function setFullHeight(bool $fullHeight = null): LayoutDescriptorInterface {
        
        $this->fullHeight = $fullHeight;
        return $this;
    }
    
    public function getFullHeight(): ?bool {
        
        return $this->fullHeight;
    }
    
    public function hasFullHeight(): bool {
        
        return ($this->getFullHeight() === true);
    }

    
    
    
    
//    public function getFeatures(): StringVectorInterface {
//        
//        if($this->features === null) {
//            
//            $this->features = StringVector::create();
//        }
//        
//        return $this->features;
//    }
}
