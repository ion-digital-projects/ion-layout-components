<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\System\Remote\UriInterface;
use \ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
use \ion\Rendering\Layout\UriActionInterface;
use \ion\Rendering\Layout\UriAction;

trait UriActionPropertyTrait {

    private $uriAction = null;
    private $uriModalName = null;
        
    public function setUriAction(UriActionInterface $action = null): UriActionPropertyInterface {
        
        $this->uriAction = $action;
        return $this;
    }
    
    public function getUriAction(): ?UriActionInterface {
        
        return $this->uriAction;
    }
    
    public function hasUriAction(): bool {
        
        return ($this->getUriAction() !== null && !$this->getUriAction()->equals(UriAction::NONE()));
    }
    
    public function setUriModalName(string $uriModalName = null): UriActionPropertyInterface {
        
        $this->uriModalName = $uriModalName;
        return $this;
    }
    
    public function getUriModalName(): ?string {
        
        return $this->uriModalName;
    }
    
    public function hasUriModalName(): bool {
        
        return ($this->getUriModalName() !== null);
    }
    
}