<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait DisplayNamePropertyTrait {

    private $displayName = null;
    
    public function getDisplayName(): ?StringInterface {
        
        return $this->displayName;
    }
    
    public function setDisplayName(string $displayName = null): DisplayNamePropertyInterface {
        
        if($displayName === null) {
            
            $this->displayName = null;
            return $this;
        }
        
        $this->displayName = StringObject::create($displayName);
        return $this;
    }
    
    public function hasDisplayName(): bool {
        
        return ($this->displayName !== null);
    }    
}