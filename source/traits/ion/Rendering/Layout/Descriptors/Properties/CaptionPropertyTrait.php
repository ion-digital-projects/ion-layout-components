<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait CaptionPropertyTrait {

    private $caption = null;
    private $captionVisible = null;
    
    public function getCaption(): ?StringInterface {
        
        return $this->caption;
    }
    
    public function setCaption(string $caption = null): CaptionPropertyInterface {
        
        if($caption === null) {
            
            $this->caption = null;
            return $this;
        }
        
        $this->caption = StringObject::create($caption);
        return $this;
    }
    
    public function hasCaption(): bool {
        
        return ($this->caption !== null);
    }    
    
    public function setCaptionVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null): CaptionPropertyInterface {
        
        $this->captionVisible = [
            'default' => $default,
            'large' => $onLargeScreens,
            'medium' => $onMediumScreens,
            'small' => $onSmallScreens
        ];
        
        return $this;
    }
    
    public function isCaptionVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null): ?bool {
        
        
        if($this->captionVisible === null) {
            
            return null;
        }        
        
        $result = null;
        
        if($onLargeScreens === true) {
            
            if($this->captionVisible['large'] === false) {
                
                return false;
            }
            
            $result = true;
        }
        
        if($onMediumScreens === true) {
            
            if($this->captionVisible['medium'] === false) {
                
                return false;
            }        
            
            $result = true;
        }

        if($onSmallScreens === true) {
            
            if($this->captionVisible['small'] === false) {
                
                return false;
            }     
            
            $result = true;
        }        
        
        if($result !== null) {
            
            return $result;
        }
        
        return $this->captionVisible['default'];
    }
}