<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of ContentDescriptorTrait
 *
 * @author Justus
 */

use \ion\Types\Arrays\Specialized\StringVectorInterface;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Types\Arrays\MapInterface;
use \ion\Types\Arrays\Map;
use \ion\ObservableInterface;
use \ion\Rendering\Layout\DescriptorException;
use \ion\System\Remote\UriInterface;
use \ion\System\Remote\Uri;
use \ion\System\Remote\UriVectorBaseInterface;
use \ion\System\Remote\UriVector;
use \ion\System\Remote\UriMapBaseInterface;
use \ion\System\Remote\UriMap;
//use \ion\Types\Arrays\VectorBaseInterface;
//use \ion\Types\Arrays\Vector;

trait ContentPagingDescriptorTrait {

    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    
    private $currentPage = 1;
    private $pageSize = null;
    private $endSize = null;
    private $itemCount = null;
    private $nextPreviousEnabled = true;
        
    public function setPageSize(int $pageSize = null): ContentPagingDescriptorInterface {
        
        if($pageSize !== null && $pageSize < 1) {
            
            throw new DescriptorException("The page size cannot be zero or negative (page size set to {$pageSize}).");
        }        
        
        $this->pageSize = $pageSize;
        return $this;
    }
    
    public function getPageSize(): ?int {
        
        return $this->pageSize;
    }
    
    public function hasPageSize(): bool {
        
        return ($this->pageSize !== null);
    }
    
    public function setEndSize(int $endSize = null): ContentPagingDescriptorInterface {
        
        $this->endSize = $endSize;
        return $this;
    }
    
    public function getEndSize(): ?int {
        
        return $this->endSize;
    }
    
    public function hasEndSize(): bool {
        
        return ($this->endSize !== null);
    }    
    
    public function setNextPreviousEnabled(bool $nextPrevious): ContentPagingDescriptorInterface {
        
        $this->nextPreviousEnabled = $nextPreviousEnabled;
        return $this;
    }
    
    public function isNextPreviousEnabled(): bool {
        
        return $this->nextPreviousEnabled;
    }
    
    public function getNextLink(): ?UriInterface {
        
        if($this->getCurrentPage() < $this->getPageCount()) {
            
            return $this->createPageUri($this->getCurrentPage() + 1);
        }
        
        return null;
    }
    
    public function getPreviousLink(): ?UriInterface {
        
        if($this->getCurrentPage() > 1) {
            
            return $this->createPageUri($this->getCurrentPage() - 1);
        }
        
        return null;
    }
    
    public function setItemCount(int $itemCount): ContentPagingDescriptorInterface {
        
        $this->itemCount = $itemCount;
        return $this;
    }
    
    public function getItemCount(): int {

        if($this->itemCount === null ) {
            
            $this->itemCount = 0;
        }
        
        return $this->itemCount;
    }    
    
    public function getPageCount(): int {
        
        if($this->getPageSize() === null) {
            
            return 1;
        }
        
        $tmp = (int) ceil($this->getItemCount() / $this->getPageSize());
        
        if($tmp <= 0) {
            
            return 1;
        }
        
        return $tmp;
    }
    
    public function setCurrentPage(int $pageIndex): ContentPagingDescriptorInterface {
        
        if($pageIndex < 1) {
            
            throw new DescriptorException("The current page index cannot be zero or negative (page index set to {$pageIndex}).");
        }
        
        if($pageIndex > $this->getPageCount()) {
            
            throw new DescriptorException("The current page index cannot be higher than the number of pages available (page index set to {$pageIndex}).");
        }        
        
        $this->currentPage = $pageIndex;
        return $this;
    }
    
    public function getCurrentPage(): int {
        
        return $this->currentPage;
    }
    
    protected function createPageUri(int $pageIndex): UriInterface {
        
        throw new DescriptorException("ContentPagingDescriptorTrait::createPageUri() needs to be overridden.");
    }
    
    private function getPageRangeStart(): int {
        
        $pageStart = 1;
        
        if($this->getEndSize() === null) {
            
            return $pageStart;
        }

        if($this->getCurrentPage() - $this->getEndSize() > 1) {
            
            $pageStart = $this->getCurrentPage() - $this->getEndSize();
            
            if($this->getCurrentPage() + $this->getEndSize() > $this->getPageCount()) {
                
                $pageStart = $this->getPageCount() - ($this->getEndSize() * 2);
            }  
        }

        return $pageStart;
    }
    
    private function getPageRangeEnd(): int {
        
        $pageEnd = $this->getPageCount();
        
        if($this->getEndSize() === null) {
            
            return $pageEnd;
        }        
        
        if($this->getCurrentPage() + $this->getEndSize() < $pageEnd) {
            
            $pageEnd = $this->getCurrentPage() + $this->getEndSize();
            
            if($this->getCurrentPage() - $this->getEndSize() < 1) {
                
                $pageEnd = 1 + ($this->getEndSize() * 2);
            }
        } 

        return $pageEnd;
    }
    
    public function getPageLinks(): UriMapBaseInterface {
        
        $links = UriMap::create();
        
        for($p = $this->getPageRangeStart(); $p <= $this->getPageRangeEnd(); ++$p) {
            
            $links = $links->set((string) $p, $this->createPageUri($p));          
        }              
        
        return $links;
    }
}
