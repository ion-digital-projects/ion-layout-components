<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */

use \ion\Types\Arrays\Specialized\StringVectorInterface;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use \ion\Rendering\Layout\DescriptorException;
use \ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use \ion\System\Remote\UriInterface;
use \ion\Types\StringInterface;
use \ion\Types\StringObject;
use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;

trait HeroDescriptorTrait {
    
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\MediaPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
//    use \ion\Rendering\Layout\Descriptors\Properties\TextPropertyTrait;        
    //use \ion\Rendering\Layout\Descriptors\Properties\ColorPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\VerticalAlignmentPropertyTrait {
        
        getVerticalAlignment as getCaptionVerticalAlignment;
        setVerticalAlignment as setCaptionVerticalAlignment;
        hasVerticalAlignment as hasCaptionVerticalAlignment;        
    }
    
    private $uriVisible = null;
    
    public function setUriVisible(bool $uriVisible = null): HeroDescriptorInterface {
        
        $this->uriVisible = $uriVisible;
        return $this;
    }
    
    public function getUriVisible(): ?bool {
        
        return $this->uriVisible;
    }
    
    public function isUriVisible(): bool {
        
        return ($this->getUriVisible() !== false);
    }
    
}
