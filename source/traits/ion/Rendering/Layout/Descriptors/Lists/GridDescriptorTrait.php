<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
trait GridDescriptorTrait {

    use ListDescriptorTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ColumnsPropertyTrait;
}
