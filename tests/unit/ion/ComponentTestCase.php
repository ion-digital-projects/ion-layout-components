<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion;

/**
 * Description of ComponentTestCase
 *
 * @author Justus

 *  */
use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\TemplateManager;
use \ion\Rendering\Layout\Adapters\TestTemplateRenderAdapter;

class ComponentTestCase extends TestCase {

    public function setUp() {
        
        if(class_exists(TestTemplateRenderAdapter::class)) {
            
            $tm = TemplateManager::getInstance();
            
            //registerRenderAdapter
            
            if(!$tm->hasRegisteredRenderAdapter(TestTemplateRenderAdapter::getClassname())) {
                
                $tm->registerRenderAdapter(new TestTemplateRenderAdapter());
            }
            
            if(!$tm->hasActiveRenderAdapter()) {
                
                $tm->setActiveRenderAdapter(TestTemplateRenderAdapter::getClassname());
            }
        }        
    }
    
}
