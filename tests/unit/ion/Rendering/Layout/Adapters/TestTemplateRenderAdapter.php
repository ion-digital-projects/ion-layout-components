<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Adapters;

/**
 * Description of W3CssRenderAdapter
 *
 * @author Justus
 */

use \ion\Rendering\Layout\TemplateRenderAdapter;
use \ion\PhpHelper as PHP;
use \ion\Rendering\Markup\Html\HtmlHelper as HTML;
use \ion\Rendering\Markup\Html\HtmlRenderOptions;
use \ion\Rendering\Markup\Html\HtmlDocumentNodeVector;
use \ion\Rendering\Markup\Html\HtmlElementDocumentNode;
use \ion\Rendering\Markup\Html\Elements\Body\HtmlUlDocumentNode;
use \ion\Rendering\Markup\Html\Elements\Body\HtmlLiDocumentNode;
use \ion\Rendering\Markup\Html\Elements\Body\HtmlDocumentNodeInterface;
use \ion\Rendering\Markup\Html\Elements\Body\HtmlADocumentNode;
use \ion\Rendering\Markup\Html\HtmlTextDocumentNode;
use \ion\Rendering\Markup\Html\HtmlDocumentPartial;
use \ion\Rendering\Layout\TemplateManager;
use \ion\Rendering\Layout\Templates\ContainerTemplateInterface;
use \ion\Rendering\Layout\Templates\ContainerTemplateBase;
use \ion\Rendering\Layout\Template;
use \ion\Rendering\Markup\Html\Elements\Body\HtmlDivDocumentNode;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\Layout\Templates\DocumentTemplateBase;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\RenderableVectorBaseInterface;
use \ion\Rendering\Layout\Components\LayoutHooks;
use \ion\Rendering\Markup\Html\HtmlAsset;
use \ion\System\Remote\UriPath;
use \ion\System\Remote\UriInterface;
use \ion\System\Remote\Uri;
use \ion\Rendering\Layout\Assets\ScriptAssetMapInterface;
use \ion\Rendering\Layout\Assets\StyleAssetMapInterface;
use \ion\Rendering\Layout\Assets\ScriptAssetMap;
use \ion\Rendering\Layout\Assets\StyleAssetMap;
use \ion\Types\StringObject;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Assets\ScriptAsset;
use \ion\Rendering\Layout\Assets\StyleAsset;
use \ion\Rendering\StringRenderable;
use \ion\System\Path;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\Types\Arrays\Specialized\CallableMap;
use \ion\Rendering\Layout\Descriptors\ModalType;
use \ion\Rendering\Layout\Descriptors\ModalActivationType;
use \ion\Rendering\Layout\UriAction;
use \ion\Rendering\Layout\ColourInterface;
use \ion\Rendering\Layout\Colour;
use \ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use \ion\Rendering\Layout\HorizontalAlignmentType;
use \ion\Rendering\Layout\VerticalAlignmentTypeInterface;
use \ion\Rendering\Layout\VerticalAlignmentType;
use \ion\Rendering\Layout\MediaContext;
use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
//use \ion\Rendering\Layout\Descriptors\Media\VideoDescriptorInterface;

class TestTemplateRenderAdapter extends TemplateRenderAdapter {
        
    protected function initialize(): void {

        $this->setCacheOpeningDelimiter("<?cache ");
        $this->setCacheClosingDelimiter("?>");
    
        return;
    }    
        
}
