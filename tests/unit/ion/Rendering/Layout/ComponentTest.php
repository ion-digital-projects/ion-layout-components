<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout;

/**
 * Description of ComponentTest
 *
 * @author Justus

 *  */
use PHPUnit\Framework\TestCase;
use \ion\ComponentTestCase;

class TestComponent extends Component {
    
}

class ComponentTest extends ComponentTestCase {
    
    
//    public function testCount() {
//        
//        $c1 = TestComponent::create("id1");
//        
//        $this->assertEquals(1, $c1->getId());
//        
//        $c2 = TestComponent::create("id2");
//        
//        $this->assertEquals(2, $c2->getId());
//        
//    }

    public function testAssets() {
        
        $c1 = TestComponent::create("id1");
        $c2 = TestComponent::create("id2");
        
        $this->assertTrue($c1->getStyleAssets()->hasKey('ion-components'));
        $this->assertTrue($c2->getStyleAssets()->hasKey('ion-components'));
        $this->assertTrue($c2->getStyleAssets()->hasKey('normalize'));
        
        $this->assertTrue($c1->getScriptAssets()->hasKey('ion-components'));
        $this->assertTrue($c2->getScriptAssets()->hasKey('ion-components'));
    }
    
}
