<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Sections;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\ComponentTestCase;

class ContentSectionComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        $t = ContentSectionComponent::create("content-section");
        
        $this->assertEquals(false, $t === null);        
    }

    
}
