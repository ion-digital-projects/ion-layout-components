<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Sections\Content;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptor;
use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptor;
use \ion\ComponentTestCase;

class ContentItemComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        $t = ContentItemComponent::create("content-item");
        
        $this->assertNotNull($t);        
    }

    public function testSetRemoveImage() {
        
        $t = ContentItemComponent::create('content-item');
        
        
        $t->setImage(new ImageDescriptor());
        $this->assertNotNull($t->getImage());
        $this->assertEquals(1, $t->getChildren()->count());
        
        $t->setImage(null);
        $this->assertNull($t->getImage());
        $this->assertEquals(0, $t->getChildren()->count());
    }
    
}
