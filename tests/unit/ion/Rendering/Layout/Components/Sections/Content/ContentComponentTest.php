<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Sections\Content;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptor;
use \ion\ComponentTestCase;

class ContentComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        
        $t = ContentComponent::create("content");
        
        $this->assertNotNull($t);        
    }

    public function testItems() {
        
        $t = ContentComponent::create('content');
        
        $t->getItems()->add(new ContentItemDescriptor("content-item-1"));
        $t->getItems()->add(new ContentItemDescriptor("content-item-2"));
        
        $this->assertEquals(2, $t->getItems()->count());
        $this->assertEquals(2, $t->getChildren()->count());
    }
    
}
