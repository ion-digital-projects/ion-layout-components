<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\TemplateManager;
use \ion\ComponentTestCase;

class LayoutComponentTest extends ComponentTestCase {
    
    
    public function testCreate() {
        $t = LayoutComponent::create("layout");
        
        $this->assertEquals(false, $t === null);
        $this->assertEquals(4, $t->getChildren()->count());

        $this->assertEquals($t, TemplateManager::getInstance()->getRegisteredRootTemplate());
    }

    
}
