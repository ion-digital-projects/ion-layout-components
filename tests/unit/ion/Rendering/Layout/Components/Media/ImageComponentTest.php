<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Media;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\ComponentTestCase;
use \ion\Rendering\Layout\TemplateManager;

class ImageComponentTest extends ComponentTestCase {
    
    public function setUp() {
        
        if(class_exists('ion\\Rendering\\Layout\\Adapters\\TestTemplateRenderAdapter')) {
            
            $tm = TemplateManager::getInstance();
            
            if(!$tm->hasActiveRenderAdapter()) {
                
                $tm->setActiveRenderAdapter(\ion\Rendering\Layout\Adapters\TestTemplateRenderAdapter::getClassname());
            }
        }        
    }
    
    public function testCreate() {
        
        $t = ImageComponent::create("image");
        
        $this->assertEquals(false, $t === null);        
    }

    
}
