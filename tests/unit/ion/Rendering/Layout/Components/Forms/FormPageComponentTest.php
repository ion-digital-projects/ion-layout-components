<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptor;
use \ion\ComponentTestCase;

class FormPageComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        
        $t = FormPageComponent::create("form-page");
        
        $this->assertEquals(false, $t === null);        
    }

    public function testGroups() {
        
        $t = FormPageComponent::create("form-page");
        
        $t->getGroups()->add(new FormGroupDescriptor($t));
        $t->getGroups()->add(new FormGroupDescriptor($t));
        
        $this->assertEquals(2, $t->getGroups()->count());
        $this->assertEquals(2, $t->getChildren()->count());
    }         
}
