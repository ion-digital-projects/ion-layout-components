<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptor;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptor;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptor;
use \ion\ComponentTestCase;

class FormGroupComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        $t = FormGroupComponent::create("form-group");
        
        $this->assertEquals(false, $t === null);        
    }


    public function testFields() {
        
        $t = FormGroupComponent::create("form-group");
        
        $t->getFields()->add(new EmailFormFieldDescriptor($t, "A"));
        $t->getFields()->add(new TextFormFieldDescriptor($t, "B"));
        
        $this->assertEquals(2, $t->getFields()->count());
        $this->assertEquals(2, $t->getChildren()->count());
    }     
}
