<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;

use \ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptor;
use \ion\ComponentTestCase;

class FormComponentTest extends ComponentTestCase {
       
    public function testCreate() {
        $t = FormComponent::create("form-component");
        
        $this->assertNotNull($t);        
    }

    public function testPages() {
        
        $t = FormComponent::create("form-component");
        
        $t->getPages()->add(new FormPageDescriptor($t));
        $t->getPages()->add(new FormPageDescriptor($t));
        
        $this->assertEquals(2, $t->getPages()->count());
        $this->assertEquals(2, $t->getChildren()->count());
    }     
}
