<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Lists;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Templates\EmptyTemplate;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptor;
use \ion\ComponentTestCase;


class ListComponentTest extends ComponentTestCase {
    
    public function testCreate() {
        $t = ListComponent::create("list");
        
        $this->assertEquals(false, $t === null);        
    }

    public function testItems() {
        
        $t = ListComponent::create("list");
        
        $t->getItems()->add(new ListItemDescriptor($t));
        $t->getItems()->add(new ListItemDescriptor($t));
        
        $this->assertEquals(2, $t->getItems()->count());
        $this->assertEquals(2, $t->getChildren()->count());
    }    
    
}
