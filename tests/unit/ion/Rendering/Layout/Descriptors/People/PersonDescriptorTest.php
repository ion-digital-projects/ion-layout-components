<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\People;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class PersonDescriptorTest extends TestCase {
     
    const DISPLAYNAME = "Maverick";
    const FIRSTNAME = "Jus";
    const LASTNAME = "Smith";    
    const SEPERATOR = '+';
    const FULLNAME = self::FIRSTNAME . self::SEPERATOR . self::LASTNAME;
    const URI = "https://ion.digital";
    const TEXT = "Some text";
    
    private $personDescriptor;
    
    public function setUp() {
        
        $this->personDescriptor = new PersonDescriptor(self::DISPLAYNAME);
        
    }
    
    public function testCreatePersonDescriptor() {

        $this->assertNotNull($this->personDescriptor);
    }
    
    public function testProperties() {

        $this->assertEquals(self::DISPLAYNAME, $this->personDescriptor->getDisplayName()->toString());
        
        $this->personDescriptor->getNames()->add(self::FIRSTNAME);
        $this->personDescriptor->getNames()->add(self::LASTNAME);
        
        $this->assertEquals(self::FULLNAME, $this->personDescriptor->getFullName(self::SEPERATOR)->toString());
        
        $this->personDescriptor->setUri(Uri::parse(self::URI));
        $this->assertEquals(self::URI, $this->personDescriptor->getUri()->toString());

        $this->personDescriptor->setText(self::TEXT);
        $this->assertEquals(self::TEXT, $this->personDescriptor->getText()->toString());        
    }

}