<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\People;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class AuthorDescriptorTest extends TestCase {
     
    const DISPLAYNAME = "Maverick";
    const FIRSTNAME = "Jus";
    const LASTNAME = "Smith";    
    const SEPERATOR = '+';
    const FULLNAME = self::FIRSTNAME . self::SEPERATOR . self::LASTNAME;
    const URI = "https://ion.digital";
    const TEXT = "Some text";
    
    private $authorDescriptor;
    
    public function setUp() {
        
        $this->authorDescriptor = new AuthorDescriptor(self::DISPLAYNAME);
        
    }
    
    public function testCreatePersonDescriptor() {

        $this->assertNotNull($this->authorDescriptor);
    }
    
    public function testProperties() {

        $this->assertEquals(self::DISPLAYNAME, $this->authorDescriptor->getDisplayName()->toString());
        
        $this->authorDescriptor->getNames()->add(self::FIRSTNAME);
        $this->authorDescriptor->getNames()->add(self::LASTNAME);
        
        $this->assertEquals(self::FULLNAME, $this->authorDescriptor->getFullName(self::SEPERATOR)->toString());
        
        $this->authorDescriptor->setUri(Uri::parse(self::URI));
        $this->assertEquals(self::URI, $this->authorDescriptor->getUri()->toString());

        $this->authorDescriptor->setText(self::TEXT);
        $this->assertEquals(self::TEXT, $this->authorDescriptor->getText()->toString());        
    }

}