<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;
use \ion\Rendering\Layout\Descriptors\People\AuthorDescriptor;
use \ion\Types\Arrays\Specialized\StringVector;

class TestAuthorProperty implements AuthorPropertyInterface {
    
    use AuthorPropertyTrait;
}

class AuthorPropertyTest extends TestCase {
    
    const DISPLAYNAME = "Maverick";
    const FIRSTNAME = "Jus";
    const LASTNAME = "Smith";    
    const SEPERATOR = '+';
    const FULLNAME = self::FIRSTNAME . self::SEPERATOR . self::LASTNAME;
    const URI = "https://ion.digital";
    const TEXT = "Some text";
    

    public function testSetGet() {
        
        $property = new TestAuthorProperty();
        
        $this->assertFalse($property->hasAuthor());
        
        $property->setAuthor(new AuthorDescriptor(self::DISPLAYNAME, StringVector::create([ self::FIRSTNAME, self::LASTNAME ]), Uri::parse(self::URI), self::TEXT));
        
        $this->assertTrue($property->hasAuthor());
        
        $this->assertNotNull($property->getAuthor());
        
        $this->assertEquals(static::DISPLAYNAME, $property->getAuthor()->getDisplayName()->toString());
        $this->assertEquals(static::FIRSTNAME, $property->getAuthor()->getFirstName()->toString());
        $this->assertEquals(static::LASTNAME, $property->getAuthor()->getLastName()->toString());
        $this->assertEquals(static::URI, $property->getAuthor()->getUri()->toString());
        $this->assertEquals(static::TEXT, $property->getAuthor()->getText()->toString());
        
        
    }

}