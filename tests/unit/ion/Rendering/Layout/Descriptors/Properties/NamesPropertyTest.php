<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestNamesProperty implements NamesPropertyInterface {
    
    use NamesPropertyTrait;
}

class NamesPropertyTest extends TestCase {
    
    const FIRSTNAME = 'Jus';
    const MIDDLENAME = 'Somebody';
    const LASTNAME = 'Smith';
    const SEPERATOR = '+';
    const FULLNAME = self::FIRSTNAME . self::SEPERATOR . self::MIDDLENAME . self::SEPERATOR . self::LASTNAME;

    public function testSetGet() {
        
        $property = new TestNamesProperty();
        
        $this->assertFalse($property->hasNames());
        
        $property->getNames()->add(self::FIRSTNAME);
        $property->getNames()->add(self::MIDDLENAME);
        $property->getNames()->add(self::LASTNAME);
        
        $this->assertTrue($property->hasNames());
        
        $this->assertEquals(3, $property->getNames()->count());
        
        $this->assertEquals(self::FIRSTNAME, $property->getNames()->get(0));
        $this->assertEquals(self::MIDDLENAME, $property->getNames()->get(1));
        $this->assertEquals(self::LASTNAME, $property->getNames()->get(2));
        
        
        $this->assertEquals(self::FULLNAME, $property->getFullName(self::SEPERATOR));
        $this->assertEquals(self::FIRSTNAME, $property->getFirstName());
        $this->assertEquals(self::LASTNAME, $property->getLastName());
    }

}