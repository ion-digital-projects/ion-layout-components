<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Types\DateTimeObject;


class TestDateTimeProperty implements DateTimePropertyInterface {
    
    use DateTimePropertyTrait;
}

class DateTimePropertyTest extends TestCase {
    
    const VALUE = '12 Nov 1981 00:00:00.00';
    

    public function testSetGet() {
        
        $property = new TestDateTimeProperty();
        
        $this->assertFalse($property->hasDateTime());
        
        $property->setDateTime(DateTimeObject::parse(static::VALUE));
        
        $this->assertTrue($property->hasDateTime());                
        
        //$this->assertEquals(static::VALUE, $property->getDateTime()->toString());
    }

}