<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;
use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptor;

class TestImageProperty implements ImagePropertyInterface {
    
    use ImagePropertyTrait;
}

class ImagePropertyTest extends TestCase {
    
    const CAPTION = "Caption";
    const URI = "https://ion.digital";
    const TEXT = "Title text";
    

    public function testSetGet() {
        
        $property = new TestImageProperty();
        
        $this->assertFalse($property->hasImage());
        
        $property->setImage(new ImageDescriptor(Uri::parse(self::URI), self::CAPTION, self::TEXT));
        
        $this->assertTrue($property->hasImage());
        
        $this->assertNotNull($property->getImage());
        
        $this->assertEquals(static::CAPTION, $property->getImage()->getCaption()->toString());
        $this->assertEquals(static::URI, $property->getImage()->getUri()->toString());
        $this->assertEquals(static::TEXT, $property->getImage()->getText()->toString());
        
        
    }

}