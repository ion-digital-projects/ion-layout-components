<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestTagProperty implements TagPropertyInterface {
    
    use TagPropertyTrait;
}

class TagPropertyTest extends TestCase {
    
    const VALUE = '123ABC';
    

    public function testSetGet() {
        
        $property = new TestTagProperty();
        
        $this->assertFalse($property->hasTag());
        
        $property->setTag(static::VALUE);
        
        $this->assertTrue($property->hasTag());
        
        $this->assertEquals(static::VALUE, $property->getTag()->toString());
    }

}