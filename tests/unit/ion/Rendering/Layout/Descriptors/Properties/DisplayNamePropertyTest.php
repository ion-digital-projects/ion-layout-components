<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestDisplayNameProperty implements DisplayNamePropertyInterface {
    
    use DisplayNamePropertyTrait;
}

class DisplayNamePropertyTest extends TestCase {
    
    const VALUE = 'Jus';
    

    public function testSetGet() {
        
        $property = new TestDisplayNameProperty();
        
        $this->assertFalse($property->hasDisplayName());
        
        $property->setDisplayName(static::VALUE);
        
        $this->assertTrue($property->hasDisplayName());
        
        $this->assertEquals(static::VALUE, $property->getDisplayName()->toString());
    }

}