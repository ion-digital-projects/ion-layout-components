<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestTextProperty implements TextPropertyInterface {
    
    use TextPropertyTrait;
}

class TextPropertyTest extends TestCase {
    
    const VALUE = '123ABC';
    

    public function testSetGet() {
        
        $property = new TestTextProperty();
        
        $this->assertFalse($property->hasText());
        
        $property->setText(static::VALUE);
        
        $this->assertTrue($property->hasText());
        
        $this->assertEquals(static::VALUE, $property->getText()->toString());
    }

}