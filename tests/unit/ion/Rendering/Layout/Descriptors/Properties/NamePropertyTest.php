<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestNameProperty implements NamePropertyInterface {
    
    use NamePropertyTrait;
}

class NamePropertyTest extends TestCase {
    
    const VALUE = '123ABC';
    

    public function testSetGet() {
        
        $property = new TestNameProperty();
        
        $this->assertFalse($property->hasName());
        
        $property->setName(static::VALUE);
        
        $this->assertTrue($property->hasName());
        
        $this->assertEquals(static::VALUE, $property->getName()->toString());
    }

}