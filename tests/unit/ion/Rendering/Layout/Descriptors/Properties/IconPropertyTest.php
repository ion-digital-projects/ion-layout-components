<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestIconProperty implements IconPropertyInterface {
    
    use IconPropertyTrait;
}

class IconPropertyTest extends TestCase {
    
    const VALUE = '123ABC';
    

    public function testSetGet() {

        $property = new TestIconProperty();
        
        $this->assertFalse($property->hasIcon());
        
        $property->setIcon(static::VALUE);
        
        $this->assertTrue($property->hasIcon());
        
        $this->assertEquals(static::VALUE, $property->getIcon()->toString());
                
        $property->setIconVisible(true);
        $this->assertTrue($property->isIconVisible());
        
        $property->setIconVisible(false);
        $this->assertFalse($property->isIconVisible());

        $property->setIconVisible(null);
        $this->assertNull($property->isIconVisible());        
        
        
        $property->setIconVisible(null, true, false, false);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertTrue($property->isIconVisible(true, false, false));
        $this->assertFalse($property->isIconVisible(false, true, false));
        $this->assertFalse($property->isIconVisible(false, false, true));
        
        $property->setIconVisible(null, true, true, false);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertTrue($property->isIconVisible(true, false, false));
        $this->assertTrue($property->isIconVisible(false, true, false));
        $this->assertFalse($property->isIconVisible(false, false, true));        
        
        $property->setIconVisible(null, true, true, true);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertTrue($property->isIconVisible(true, false, false));
        $this->assertTrue($property->isIconVisible(false, true, false));
        $this->assertTrue($property->isIconVisible(false, false, true));     
        
        $property->setIconVisible(null, false, true, true);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertFalse($property->isIconVisible(true, false, false));
        $this->assertTrue($property->isIconVisible(false, true, false));
        $this->assertTrue($property->isIconVisible(false, false, true));        
        
        $property->setIconVisible(null, false, false, true);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertFalse($property->isIconVisible(true, false, false));
        $this->assertFalse($property->isIconVisible(false, true, false));
        $this->assertTrue($property->isIconVisible(false, false, true));  
        
        $property->setIconVisible(null, false, false, false);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertFalse($property->isIconVisible(true, false, false));
        $this->assertFalse($property->isIconVisible(false, true, false));
        $this->assertFalse($property->isIconVisible(false, false, true));   

        $property->setIconVisible(null, false, true, false);
        $this->assertNull($property->isIconVisible(false, false, false)); 
        $this->assertFalse($property->isIconVisible(true, false, false));
        $this->assertTrue($property->isIconVisible(false, true, false));
        $this->assertFalse($property->isIconVisible(false, false, true));         
    }

}