<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class TestUriProperty implements UriPropertyInterface {
    
    use UriPropertyTrait;
}

class UriPropertyTest extends TestCase {
    
    const VALUE = 'https://ion.digital';
    

    public function testSetGet() {
        
        $property = new TestUriProperty();
        
        $this->assertFalse($property->hasUri());
        
        $property->setUri(Uri::parse(static::VALUE));
        
        $this->assertTrue($property->hasUri());
        
        $this->assertEquals(static::VALUE, $property->getUri()->toString());
    }

}