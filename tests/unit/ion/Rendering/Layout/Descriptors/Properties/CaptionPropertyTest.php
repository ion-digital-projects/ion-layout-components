<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;

class TestCaptionProperty implements CaptionPropertyInterface {
    
    use CaptionPropertyTrait;
}

class CaptionPropertyTest extends TestCase {
    
    const VALUE = '123ABC';
    

    public function testSetGet() {

        $property = new TestCaptionProperty();
        
        $this->assertFalse($property->hasCaption());
        
        $property->setCaption(static::VALUE);
        
        $this->assertTrue($property->hasCaption());
        
        $this->assertEquals(static::VALUE, $property->getCaption()->toString());
        
        $property->setCaptionVisible(true);
        $this->assertTrue($property->isCaptionVisible());
        
        $property->setCaptionVisible(false);
        $this->assertFalse($property->isCaptionVisible());

        $property->setCaptionVisible(null);
        $this->assertNull($property->isCaptionVisible());    
        
        $property->setCaptionVisible(null, true, false, false);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertTrue($property->isCaptionVisible(true, false, false));
        $this->assertFalse($property->isCaptionVisible(false, true, false));
        $this->assertFalse($property->isCaptionVisible(false, false, true));
        
        $property->setCaptionVisible(null, true, true, false);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertTrue($property->isCaptionVisible(true, false, false));
        $this->assertTrue($property->isCaptionVisible(false, true, false));
        $this->assertFalse($property->isCaptionVisible(false, false, true));        
        
        $property->setCaptionVisible(null, true, true, true);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertTrue($property->isCaptionVisible(true, false, false));
        $this->assertTrue($property->isCaptionVisible(false, true, false));
        $this->assertTrue($property->isCaptionVisible(false, false, true));     
        
        $property->setCaptionVisible(null, false, true, true);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertFalse($property->isCaptionVisible(true, false, false));
        $this->assertTrue($property->isCaptionVisible(false, true, false));
        $this->assertTrue($property->isCaptionVisible(false, false, true));        
        
        $property->setCaptionVisible(null, false, false, true);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertFalse($property->isCaptionVisible(true, false, false));
        $this->assertFalse($property->isCaptionVisible(false, true, false));
        $this->assertTrue($property->isCaptionVisible(false, false, true));  
        
        $property->setCaptionVisible(null, false, false, false);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertFalse($property->isCaptionVisible(true, false, false));
        $this->assertFalse($property->isCaptionVisible(false, true, false));
        $this->assertFalse($property->isCaptionVisible(false, false, true));   

        $property->setCaptionVisible(null, false, true, false);
        $this->assertNull($property->isCaptionVisible(false, false, false)); 
        $this->assertFalse($property->isCaptionVisible(true, false, false));
        $this->assertTrue($property->isCaptionVisible(false, true, false));
        $this->assertFalse($property->isCaptionVisible(false, false, true));           
    }

}