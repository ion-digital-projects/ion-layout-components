<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class ContentDescriptorTest extends TestCase {
     
    
    private $contentItems;
    
    public function setUp() {
        
        $this->contentItems = new ContentDescriptor();                
    }
    
    public function testCreateContentDescriptor() {

        $this->assertNotNull($this->contentItems);
    }
    
//    public function testItems() {
//        
//        $this->contentItems->addItem(self::CAPTION, Uri::parse(self::TARGET1), self::NAME, self::TAG);
//        
//        $this->assertEquals(1, $this->contentItems->getItems()->count());
//    }
}