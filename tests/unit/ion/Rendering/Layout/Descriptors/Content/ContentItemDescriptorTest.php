<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class ContentItemDescriptorTest extends TestCase {
    
    const CONTENT = 'Test content';
    
    private $contentItems;
    private $contentItem;
    
    public function setUp() {
        
        $this->contentItems = new ContentDescriptor();
        $this->contentItem = new ContentItemDescriptor(self::CONTENT);
        
//        $this->contentItems->addItem(self::CAPTION, Uri::parse(self::TARGET1), self::NAME, self::TAG);    
//        $this->menuItem = $this->contentItems->getItems()->get(0);
        
        $this->contentItems->getItems()->add($this->contentItem);
    }
    
    public function testCreateContentItemDescriptor() {
        

        $this->assertNotNull($this->contentItems);
        $this->assertNotNull($this->contentItem);
        $this->assertEquals(1, $this->contentItems->getItems()->count());
    }
    
    public function testProperties() {
                      
        $this->assertEquals(self::CONTENT, $this->contentItem->getText());
        
        
//        $this->assertEquals(self::NAME, $this->contentItem->getName());
//        $this->assertEquals(self::TAG, $this->contentItem->getTag());
//        $this->assertEquals(self::TARGET1, $this->contentItem->getTargetUri()->toString());
        
        
    }
}