<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class ImageDescriptorTest extends TestCase {
     
    const CAPTION = "Caption";
    const URI = "https://ion.digital";
    const TEXT = "Title text";
    
    private $imageDescriptor;
    
    public function setUp() {
        
        $this->imageDescriptor = new ImageDescriptor();                
    }
    
    public function testCreateImageDescriptor() {

        $this->assertNotNull($this->imageDescriptor);
    }
    
    public function testProperties() {
        
        $this->imageDescriptor->setCaption(self::CAPTION);
        $this->assertEquals(self::CAPTION, $this->imageDescriptor->getCaption()->toString());
        
        $this->imageDescriptor->setUri(Uri::parse(self::URI));
        $this->assertEquals(self::URI, $this->imageDescriptor->getUri()->toString());

        $this->imageDescriptor->setText(self::TEXT);
        $this->assertEquals(self::TEXT, $this->imageDescriptor->getText()->toString());        
    }

}