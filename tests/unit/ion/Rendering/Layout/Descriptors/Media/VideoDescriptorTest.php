<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class VideoDescriptorTest extends TestCase {
     
    const CAPTION = "Caption";
    const URI = "https://ion.digital";
    const TEXT = "Title text";
    
    private $videoDescriptor;
    
    public function setUp() {
        
        $this->videoDescriptor = new VideoDescriptor();                
    }
    
    public function testCreateVideoDescriptor() {

        $this->assertNotNull($this->videoDescriptor);
    }
    
    public function testProperties() {
        
        $this->videoDescriptor->setCaption(self::CAPTION);
        $this->assertEquals(self::CAPTION, $this->videoDescriptor->getCaption()->toString());
        
        $this->videoDescriptor->setUri(Uri::parse(self::URI));
        $this->assertEquals(self::URI, $this->videoDescriptor->getUri()->toString());

        $this->videoDescriptor->setText(self::TEXT);
        $this->assertEquals(self::TEXT, $this->videoDescriptor->getText()->toString());        
    }

}