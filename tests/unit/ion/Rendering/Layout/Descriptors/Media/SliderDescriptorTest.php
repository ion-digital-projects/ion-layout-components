<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;

class SliderDescriptorTest extends TestCase {
     
    const CAPTION = "Caption";
    const URI = "https://ion.digital";
    const TEXT = "Title text";
    
    private $sliderDescriptor;
    
    public function setUp() {
        
        $this->sliderDescriptor = new SliderDescriptor();                
    }
    
    public function testCreateSliderDescriptor() {

        $this->assertNotNull($this->sliderDescriptor);
    }
    
    public function testProperties() {
        
        $this->sliderDescriptor->setCaption(self::CAPTION);
        $this->assertEquals(self::CAPTION, $this->sliderDescriptor->getCaption()->toString());
        
        $this->sliderDescriptor->setUri(Uri::parse(self::URI));
        $this->assertEquals(self::URI, $this->sliderDescriptor->getUri()->toString());

        $this->sliderDescriptor->setText(self::TEXT);
        $this->assertEquals(self::TEXT, $this->sliderDescriptor->getText()->toString());        
    }

}