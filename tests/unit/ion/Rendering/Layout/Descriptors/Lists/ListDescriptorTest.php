<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;
use \ion\Rendering\Layout\Descriptors\Properties\UriAction;

class ListDescriptorTest extends TestCase {
    
    const CAPTION = 'my caption';
    const NAME = 'my name';
    const TAG = 'my tag';
    const TARGET1 = 'http://localhost:8000';    
    
    private $list;
    
    public function setUp() {
        
        $this->list = new ListDescriptor();                
    }
    
    public function testCreateListDescriptor() {

        $this->assertNotNull($this->list);
    }
    
    public function testItems() {
        
        $this->list->addItem(self::CAPTION, Uri::parse(self::TARGET1));
        
        $this->assertEquals(1, $this->list->getItems()->count());
    }
}