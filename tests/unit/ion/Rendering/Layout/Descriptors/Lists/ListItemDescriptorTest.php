<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\System\Remote\Uri;
use \ion\Rendering\Layout\Descriptors\Properties\UriAction;

class ListItemDescriptorTest extends TestCase {
    
    const CAPTION = 'my caption';
    //const NAME = 'my name';
    const TAG = 'my tag';
    const TARGET1 = 'http://localhost:8000';
    const ICON = 'my-icon-tag';
    const ORDER = 999;
    
    
    private $list;
    private $listItem;
    
    public function setUp() {
        
        $this->list = new ListDescriptor();
        
        $this->list->addItem(self::CAPTION, Uri::parse(self::TARGET1), null, null, self::ICON, true, true, self::ORDER, self::TAG);    
        $this->listItem = $this->list->getItems()->get(0);
        
    }
    
    public function testCreateListItemDescriptor() {
        

        $this->assertNotNull($this->list);
        $this->assertNotNull($this->listItem);
    }
    
    public function testProperties() {
        
        $this->assertEquals(self::CAPTION, $this->listItem->getCaption());
        //$this->assertEquals(self::NAME, $this->listItem->getName());
        $this->assertEquals(self::TAG, $this->listItem->getTag());
        $this->assertEquals(self::TARGET1, $this->listItem->getUri()->toString());
        $this->assertEquals(self::ICON, $this->listItem->getIcon());
        $this->assertEquals(self::ORDER, $this->listItem->getOrder());
    }
    
    public function testHierarchicalLists() {
        
        $item1 = $this->listItem->addItem(self::CAPTION, Uri::parse(self::TARGET1), null, null, self::ICON, true, true, self::ORDER, self::TAG);
        $item2 = $this->listItem->addItem(self::CAPTION, Uri::parse(self::TARGET1), null, null, self::ICON, true, true, self::ORDER, self::TAG);
        
        $this->assertEquals(2, $this->listItem->getItems()->count());
        $this->assertEquals(1, $this->list->getItems()->count());
    }
}