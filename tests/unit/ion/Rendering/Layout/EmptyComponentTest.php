<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout;

/**
 * Description of TestTemplates
 *
 * @author Justus
 */

use PHPUnit\Framework\TestCase;
use \ion\Rendering\Layout\Components\EmptyComponent;
use \ion\Rendering\Layout\Templates\DocumentTemplate;
use \ion\Rendering\Layout\Templates\HeaderTemplate;
use \ion\Rendering\Layout\Templates\ContentTemplate;
use \ion\Rendering\Layout\Templates\FooterTemplate;
use \ion\Rendering\Layout\Templates\ContainerTemplate;
use \ion\Rendering\Layout\Templates\Navigation\NavigationTemplate;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\StringRenderable;


class EmptyComponentTest extends TestCase {
    
    
    public function testEmpty() {
        
        
        
        $t = EmptyComponent::createFromRenderables('id1', (RenderableVector::create([ StringRenderable::create('A') ])));
        
        $this->assertEquals(false, $t === null);        
    
        $t->getChildren()->add(EmptyComponent::createFromRenderables('id2', RenderableVector::create([ StringRenderable::create('B') ])));
        $t->getChildren()->add(EmptyComponent::createFromRenderables('id3', RenderableVector::create([ StringRenderable::create('C') ])));
        $t->getChildren()->add(EmptyComponent::createFromRenderables('id4', RenderableVector::create([ StringRenderable::create('D') ])));
        
        $this->assertEquals(3, $t->getChildren()->count());
        
        $this->assertEquals('ABCD', $t->render()->toString());
    }
    
}
