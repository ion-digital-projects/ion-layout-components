<?php

/* 
 * See license information at the package root in LICENSE.md
 */

$coreIndexPath = __DIR__ . "/vendor/ion/core/index.php";

if(file_exists( $coreIndexPath )) {
    
    include_once( $coreIndexPath );
    exit;
}

http_response_code(404);
exit;