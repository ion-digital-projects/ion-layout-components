<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
interface EmptyComponentInterface extends ComponentInterface
{
    static function createFromRenderables(string $clientId, RenderableVectorBaseInterface $renderables, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : EmptyComponentInterface;
}