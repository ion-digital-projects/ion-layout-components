<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components;

/**
 * Description of DocumentTemplateHooks
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateHooks;
class LayoutHooks extends TemplateHooks
{
    const HEAD_HOOK = 1;
    const HEAD_PREPEND_HOOK = 2;
    const HEAD_APPEND_HOOK = 3;
    const HEAD_SCRIPTS_HOOK = 4;
    const HEAD_STYLES_HOOK = 5;
    const BODY_HOOK = 6;
    const BODY_PREPEND_HOOK = 7;
    const BODY_APPEND_HOOK = 8;
    const BODY_SCRIPTS_HOOK = 9;
    const HEADER_HOOK = 10;
    const CONTENT_HOOK = 11;
    const FOOTER_HOOK = 12;
}