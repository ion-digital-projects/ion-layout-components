<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
use ion\Types\Arrays\MapInterface;
use ion\Types\Arrays\Map;
use ion\ObservableInterface;
use ion\ObserverInterface;
class FormPageComponent extends Component implements FormPageComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorTrait;
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface
    {
        if (!$descriptor instanceof FormPageDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setCaption($descriptor->getCaption());
        return $component;
    }
    //    public function getParent(): ?TemplateInterface {
    //
    //        return parent::getParent();
    //
    ////        if($this->ListItemDescriptorTrait_getParentContainer() === null) {
    ////
    ////            return parent::getParent();
    ////        }
    ////
    ////        return $this->ListItemDescriptorTrait_getParentContainer();
    //    }
    //    public function setParent(TemplateInterface $parent = null): TemplateInterface {
    //
    //        if($parent !== null) {
    //
    //            $this->ListItemDescriptorTrait_setParentContainer($parent);
    //        }
    //
    //        return parent::setParent($parent);
    //    }
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        $this->observe($this->getGroups());
        if ($parent !== null) {
            $this->setForm($parent);
        }
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }
    protected function createItemComponentInstance(FormGroupDescriptorInterface $descriptor) : FormGroupComponentInterface
    {
        $clientId = static::generateClientId($descriptor, $this, $this->getGroups()->count());
        return FormGroupComponent::createFromDescriptor($clientId, $descriptor, null, $this);
    }
    protected function initialize() : ?TemplateInterface
    {
        return $this;
    }
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getGroups()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->add($instance);
        }
        return $this;
    }
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getGroups()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->insert($data->get('index'), $instance);
        }
        return $this;
    }
    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getGroups()) {
            $this->getChildren()->remove($data->get('index'));
        }
        return $this;
    }
}