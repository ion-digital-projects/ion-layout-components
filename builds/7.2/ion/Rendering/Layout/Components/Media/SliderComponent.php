<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Media;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVectorInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\RenderOptionsInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateVectorBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\Layout\Descriptors\Media\SliderDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\System\Remote\UriPathInterface;
use ion\System\Remote\UriPath;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\Rendering\Layout\Assets\ScriptAsset;
use ion\Types\Arrays\Specialized\StringVector;
class SliderComponent extends MediaComponent implements SliderComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Media\SliderDescriptorTrait;
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface
    {
        if (!$descriptor instanceof ImageDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        return $component;
    }
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        if (!$this->getStyleAssets()->isLocked()) {
            if (!$this->getStyleAssets()->hasKey('ion-components-slider')) {
                $path = UriPath::parse('/resources/styles/css/ion-components-slider.css');
                $this->getStyleAssets()->set('ion-components-slider', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
        if (!$this->getScriptAssets()->isLocked()) {
            if (!$this->getScriptAssets()->hasKey('ion-components-slider')) {
                $path = UriPath::parse('/resources/scripts/js/ion-components-slider.js');
                $this->getScriptAssets()->set('ion-components-slider', ScriptAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, true, TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
    }
    protected function initialize() : ?TemplateInterface
    {
        return null;
    }
}