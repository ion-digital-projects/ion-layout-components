<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections;

/**
 * Description of Form
 *
 * @author Justus
 */
class MainSectionDescriptor extends SectionDescriptor implements MainSectionDescriptorInterface
{
    public function __construct()
    {
        parent::__construct();
    }
}