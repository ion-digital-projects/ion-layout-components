<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\ActionDescriptorInterface;
interface NavigationDescriptorInterface extends ImagePropertyInterface, ColourPropertyInterface, UriPropertyInterface
{
    function setMenu(ListItemContainerInterface $items = null) : NavigationDescriptorInterface;
    function getMenu() : ListItemContainerInterface;
    function hasMenu() : bool;
    function setAction(ActionDescriptorInterface $action = null) : NavigationDescriptorInterface;
    function getAction() : ?ActionDescriptorInterface;
    function hasAction() : bool;
    function setSearchEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isSearchEnabled() : bool;
    function setAccountEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isAccountEnabled() : bool;
    function setAccountMenu(ListItemContainerInterface $menu = null) : NavigationDescriptorInterface;
    function getAccountMenu() : ListItemContainerInterface;
    function hasAccountMenu() : bool;
    function setAccountUri(UriInterface $uri = null) : NavigationDescriptorInterface;
    function getAccountUri() : UriInterface;
    function hasAccountUri() : bool;
    function setSearchParameterName(string $searchParameterName = null) : NavigationDescriptorInterface;
    function getSearchParameterName() : ?string;
    function hasSearchParameterName() : bool;
    function setActionEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isActionEnabled() : bool;
    function setInitialColourAlpha(int $initialColourAlpha = null) : NavigationDescriptorInterface;
    function getInitialColourAlpha() : ?int;
    function hasInitialColourAlpha() : bool;
    function setShadowEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isShadowEnabled() : bool;
    function getShadowEnabled() : ?bool;
    function setShadowEnabledInitially(bool $enabled = null) : NavigationDescriptorInterface;
    function isShadowEnabledInitially() : bool;
    function getShadowEnabledInitially() : ?bool;
    function setSmallEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isSmallEnabled() : bool;
    function getSmallEnabled() : ?bool;
    function setMediumEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isMediumEnabled() : bool;
    function getMediumEnabled() : ?bool;
    function setLargeEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    function isLargeEnabled() : bool;
    function getLargeEnabled() : ?bool;
    function setInitialColour(ColourInterface $colour = null) : NavigationDescriptorInterface;
    function getInitialColour() : ?ColourInterface;
    function hasInitialColour() : bool;
}