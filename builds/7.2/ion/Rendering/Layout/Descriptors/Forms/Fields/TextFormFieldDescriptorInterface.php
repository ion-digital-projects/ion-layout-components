<?php
namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
/**
 * Description of EmailFormFieldDescriptorTrait
 *
 * @author Justus
 */
interface TextFormFieldDescriptorInterface extends FormFieldDescriptorInterface
{
    function setMultiLine(bool $multiLine) : TextFormFieldDescriptorInterface;
    function isMultiLine() : bool;
}