<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

/**
 * Description of TextFormFieldDescriptor
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptor;
class TextFormFieldDescriptor extends FormFieldDescriptor implements TextFormFieldDescriptorInterface
{
    use TextFormFieldDescriptorTrait;
}