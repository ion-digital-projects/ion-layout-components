<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptor
 *
 * @author Justus
 */
class ActionDescriptor extends Descriptor implements ActionDescriptorInterface
{
    use ActionDescriptorTrait;
}