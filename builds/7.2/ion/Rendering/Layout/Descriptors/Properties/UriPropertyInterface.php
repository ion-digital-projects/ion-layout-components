<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\System\Remote\UriInterface;
interface UriPropertyInterface
{
    function getUri() : ?UriInterface;
    function setUri(UriInterface $uri = null) : UriPropertyInterface;
    function hasUri() : bool;
}