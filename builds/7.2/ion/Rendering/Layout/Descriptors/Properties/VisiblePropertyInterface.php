<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface VisiblePropertyInterface
{
    function getVisible() : ?bool;
    function setVisible(bool $visible = null) : VisiblePropertyInterface;
    function isVisible() : bool;
}