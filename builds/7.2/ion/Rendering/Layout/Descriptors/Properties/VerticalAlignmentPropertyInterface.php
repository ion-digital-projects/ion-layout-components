<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
interface VerticalAlignmentPropertyInterface
{
    function setVerticalAlignment(VerticalAlignmentTypeInterface $verticalAlignment = null) : VerticalAlignmentPropertyInterface;
    function getVerticalAlignment() : VerticalAlignmentTypeInterface;
    function hasVerticalAlignment() : bool;
}