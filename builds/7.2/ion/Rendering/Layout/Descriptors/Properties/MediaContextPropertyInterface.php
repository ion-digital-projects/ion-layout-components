<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\MediaContextInterface;
interface MediaContextPropertyInterface
{
    function getMediaContext() : ?MediaContextInterface;
    function setMediaContext(MediaContextInterface $mediaContext = null) : MediaContextPropertyInterface;
    function hasMediaContext() : bool;
}