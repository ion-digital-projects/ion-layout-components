<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
interface ImagePropertyInterface
{
    function getImage() : ?ImageDescriptorInterface;
    function setImage(ImageDescriptorInterface $image = null) : ImagePropertyInterface;
    function hasImage() : bool;
    function setImageVisible(bool $visible = null) : ImagePropertyInterface;
    function isImageVisible() : ?bool;
}