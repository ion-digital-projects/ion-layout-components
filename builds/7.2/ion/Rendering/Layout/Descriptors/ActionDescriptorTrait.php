<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\DescriptorException;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
trait ActionDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\ColorPropertyTrait;
    private $autoClickDelay = 0;
    private $autoClickCountdown = 0;
    public function setAutoClickDelay(int $autoClickDelay = 0) : ActionDescriptorInterface
    {
        $this->autoClickDelay = $autoClickDelay;
        return $this;
    }
    public function getAutoClickDelay() : int
    {
        return $this->autoClickDelay;
    }
    public function hasAutoClickDelay() : bool
    {
        return $this->autoClickDelay > 0;
    }
    public function setAutoClickCountdown(int $autoClickCountdown = 0) : ActionDescriptorInterface
    {
        $this->autoClickCountdown = $autoClickCountdown;
        return $this;
    }
    public function getAutoClickCountdown() : int
    {
        return $this->autoClickCountdown;
    }
    public function hasAutoClickCountdown() : bool
    {
        return $this->autoClickCountdown > 0;
    }
}