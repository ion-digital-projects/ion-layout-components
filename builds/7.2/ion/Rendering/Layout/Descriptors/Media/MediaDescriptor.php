<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Rendering\Layout\MediaContextInterface;
class MediaDescriptor extends Descriptor implements MediaDescriptorInterface
{
    use MediaDescriptorTrait;
    public function __construct(UriInterface $uri = null, string $caption = null, string $text = null, MediaContextInterface $mediaContext = null, bool $visible = null, MediaDescriptorInterface $thumbnail = null, UriInterface $targetUri = null)
    {
        parent::__construct();
        $this->setUri($uri);
        $this->setCaption($caption);
        $this->setText($text);
        $this->setVisible($visible);
        $this->setMediaContext($mediaContext);
        $this->setThumbnail($thumbnail);
        $this->setTargetUri($targetUri);
    }
}