<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
class ListDescriptor extends Descriptor implements ListDescriptorInterface
{
    use ListDescriptorTrait;
    public function __construct()
    {
        parent::__construct();
    }
}