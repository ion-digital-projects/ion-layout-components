<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormFieldDescriptorInterface
 *
 * @author Justus.Meyer
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Types\StringObject;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\UriActionInterface;
class ListItemDescriptor extends Descriptor implements ListItemDescriptorInterface
{
    use ListItemDescriptorTrait;
    public function __construct(ListItemContainerInterface $parent, string $caption = null, UriInterface $target = null, UriActionInterface $uriAction = null, string $uriModalName = null, string $icon = null, bool $showIcon = null, bool $showCaption = null, int $order = null, string $tag = null)
    {
        $this->setParentContainer($parent);
        $this->setCaption($caption);
        $this->setUri($target);
        $this->setUriAction($uriAction);
        $this->setUriModalName($uriModalName);
        $this->setIcon($icon);
        $this->setIconVisible($showIcon);
        $this->setCaptionVisible($showCaption);
        $this->setOrder($order);
        $this->setTag($tag);
    }
}