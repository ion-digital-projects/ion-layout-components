<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\Descriptors\LayoutDescriptorInterface;
interface LayoutComponentInterface extends ComponentInterface, LayoutDescriptorInterface
{
    function setParent(TemplateInterface $parent = null) : TemplateInterface;
    function getParent() : ?TemplateInterface;
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
}