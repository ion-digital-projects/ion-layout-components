<?php
namespace ion\Rendering\Layout\Components\Lists;

use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
interface ListComponentInterface extends ComponentInterface, ListDescriptorInterface
{
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
}