<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Sections\Content;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentPagingDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapInterface;
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Components\Media\ImageComponent;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
class ContentPagingComponent extends Component implements ContentPagingComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentPagingDescriptorTrait;
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface
    {
        if (!$descriptor instanceof ContentPagingDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        //        foreach($descriptor->getItems() as $itemDescriptor) {
        //
        //            $component->getItems()->add(ContentItemComponent);
        //        }
        return $component;
    }
    public static function createFromContentDescriptor(string $clientId, ContentDescriptorInterface $contentDescriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        return new static($clientId, $parent, $renderOptions, $hooks, $contentDescriptor);
    }
    private $contentDescriptor = null;
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null, ContentDescriptorInterface $contentDescriptor = null)
    {
        $this->setContentDescriptor($contentDescriptor);
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        //        if(!$this->getScriptAssets()->isLocked()) {
        //
        //            if(!$this->getScriptAssets()->hasKey('ion-action-click-event')) {
        //
        //                $value = WP::getOption('ion-action-click-script', null, WP::getUriPostId());
        //
        //                if(PHP::isEmpty($value)) {
        //
        //                    $value = get_theme_mod('ion-action-click-script');
        //                }
        //
        //                if(!PHP::isEmpty($value)) {
        //
        //                    $value = StringObject::create($value)->indent(2, true)->toString();
        //
        //                    $this->getScriptAssets()->set(
        //                            'ion-action-click-event',
        //                            ScriptAsset::inline("if(ion.hasModule('actions')) {\n\tion.actions.setOnClickHook(function() {\n{$value}\n\t});\n}", 50));
        //                }
        //            }
        //        }
    }
    protected final function setContentDescriptor(ContentDescriptorInterface $descriptor = null) : ContentPagingComponentInterface
    {
        $this->contentDescriptor = $descriptor;
        return $this;
    }
    public final function getContentDescriptor() : ?ContentDescriptorInterface
    {
        return $this->contentDescriptor;
    }
    public final function hasContentDescriptor() : bool
    {
        return $this->getContentDescriptor() !== null;
    }
    protected function initialize() : ?TemplateInterface
    {
        if ($this->hasContentDescriptor()) {
            return parent::initialize();
        }
        return parent::initialize();
    }
}