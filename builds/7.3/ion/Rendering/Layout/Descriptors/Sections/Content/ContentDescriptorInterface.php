<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Rendering\Layout\Descriptors\Lists\GridDescriptorInterface;
interface ContentDescriptorInterface extends GridDescriptorInterface
{
    function setImagesVisible(bool $visible = null) : ContentDescriptorInterface;
    function getImagesVisible() : ?bool;
    function areImagesVisible() : bool;
}