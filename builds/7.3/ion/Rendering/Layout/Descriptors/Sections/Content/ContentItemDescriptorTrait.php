<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of ContentDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait ContentItemDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\TextPropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\TagPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\DateTimePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\AuthorPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\IconPropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\OrderPropertyTrait;
    private $prioritized = null;
    private $index = null;
    private $singular = null;
    public function setPrioritized(bool $prioritized = null) : ContentItemDescriptorInterface
    {
        $this->prioritized = $prioritized;
        return $this;
    }
    public function getPrioritized() : ?bool
    {
        return $this->prioritized;
    }
    public function isPrioritized() : bool
    {
        return $this->prioritized === true;
    }
    public function setSingular(bool $singular = null) : ContentItemDescriptorInterface
    {
        $this->singular = $singular;
        return $this;
    }
    public function getSingular() : ?bool
    {
        return $this->singular;
    }
    public function isSingular() : bool
    {
        return $this->singular === true;
    }
}