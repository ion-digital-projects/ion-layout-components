<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\DescriptorException;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
trait NavigationDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait {
        getImage as getLogo;
        setImage as setLogo;
        hasImage as hasLogo;
        setImageVisible as setLogoVisible;
        isImageVisible as isLogoVisible;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait {
        getUri as getSearchUri;
        setUri as setSearchUri;
        hasUri as hasSearchUri;
    }
    private $menu = null;
    private $action = null;
    private $searchEnabled = null;
    private $actionEnabled = null;
    private $accountEnabled = null;
    private $accountMenu = null;
    private $accountUri = null;
    private $searchParameterName = null;
    private $dropDownStyle = null;
    private $initialColourAlpha = null;
    private $shadowEnabled = null;
    private $shadowEnabledInitially = null;
    private $smallEnabled = null;
    private $mediumEnabled = null;
    private $largeEnabled = null;
    private $initialColour = null;
    private static function setItemDefaults(ListItemContainerInterface $container) : ListItemContainerInterface
    {
        foreach ($container->getItems() as $item) {
            $item->setCaptionVisible(null, $item->isCaptionVisible(), true, true);
            $item->setIconVisible(null, $item->isIconVisible(), false, false);
            static::setItemDefaults($item);
        }
        return $container;
    }
    public function setMenu(ListItemContainerInterface $items = null) : NavigationDescriptorInterface
    {
        $this->menu = $items === null ? null : static::setItemDefaults($items);
        return $this;
    }
    public function getMenu() : ListItemContainerInterface
    {
        if ($this->menu === null) {
            throw new DescriptorException("No menu has been specified.");
        }
        return $this->menu;
    }
    public function hasMenu() : bool
    {
        return $this->menu !== null;
    }
    public function setAction(ActionDescriptorInterface $action = null) : NavigationDescriptorInterface
    {
        $this->action = $action;
        return $this;
    }
    public function getAction() : ?ActionDescriptorInterface
    {
        return $this->action;
    }
    public function hasAction() : bool
    {
        return $this->action !== null;
    }
    public function setSearchEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->searchEnabled = $enabled;
        return $this;
    }
    public function isSearchEnabled() : bool
    {
        return $this->searchEnabled === true;
    }
    public function setAccountEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->accountEnabled = $enabled;
        return $this;
    }
    public function isAccountEnabled() : bool
    {
        return $this->accountEnabled === true;
    }
    public function setAccountMenu(ListItemContainerInterface $menu = null) : NavigationDescriptorInterface
    {
        $this->accountMenu = $menu;
        return $this;
    }
    public function getAccountMenu() : ListItemContainerInterface
    {
        return $this->accountMenu;
    }
    public function hasAccountMenu() : bool
    {
        return $this->accountEnabled === true;
    }
    public function setAccountUri(UriInterface $uri = null) : NavigationDescriptorInterface
    {
        $this->accountUri = $uri;
        return $this;
    }
    public function getAccountUri() : UriInterface
    {
        return $this->accountUri;
    }
    public function hasAccountUri() : bool
    {
        return $this->accountUri === true;
    }
    public function setSearchParameterName(string $searchParameterName = null) : NavigationDescriptorInterface
    {
        $this->searchParameterName = $searchParameterName;
        return $this;
    }
    public function getSearchParameterName() : ?string
    {
        return $this->searchParameterName;
    }
    public function hasSearchParameterName() : bool
    {
        return $this->getSearchParameterName() !== null;
    }
    public function setActionEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->actionEnabled = $enabled;
        return $this;
    }
    public function isActionEnabled() : bool
    {
        return $this->actionEnabled === true;
    }
    public function setInitialColourAlpha(int $initialColourAlpha = null) : NavigationDescriptorInterface
    {
        $this->initialColourAlpha = $initialColourAlpha;
        return $this;
    }
    public function getInitialColourAlpha() : ?int
    {
        return $this->initialColourAlpha;
    }
    public function hasInitialColourAlpha() : bool
    {
        return $this->initialColourAlpha !== null;
    }
    //            bool $shadow = null,
    //            bool $initialShadow = null
    //            bool $disableLarge = null,
    //            bool $disableMedium = null
    public function setShadowEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->shadowEnabled = $enabled;
        return $this;
    }
    public function isShadowEnabled() : bool
    {
        return $this->shadowEnabled === true;
    }
    public function getShadowEnabled() : ?bool
    {
        return $this->shadowEnabled;
    }
    public function setShadowEnabledInitially(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->shadowEnabledInitially = $enabled;
        return $this;
    }
    public function isShadowEnabledInitially() : bool
    {
        return $this->shadowEnabledInitially === true;
    }
    public function getShadowEnabledInitially() : ?bool
    {
        return $this->shadowEnabledInitially;
    }
    public function setSmallEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->smallEnabled = $enabled;
        return $this;
    }
    public function isSmallEnabled() : bool
    {
        return $this->smallEnabled === true;
    }
    public function getSmallEnabled() : ?bool
    {
        return $this->smallEnabled;
    }
    public function setMediumEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->mediumEnabled = $enabled;
        return $this;
    }
    public function isMediumEnabled() : bool
    {
        return $this->mediumEnabled === true;
    }
    public function getMediumEnabled() : ?bool
    {
        return $this->mediumEnabled;
    }
    public function setLargeEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->largeEnabled = $enabled;
        return $this;
    }
    public function isLargeEnabled() : bool
    {
        return $this->largeEnabled === true;
    }
    public function getLargeEnabled() : ?bool
    {
        return $this->largeEnabled;
    }
    public function setInitialColour(ColourInterface $colour = null) : NavigationDescriptorInterface
    {
        $this->initialColour = $colour;
        return $this;
    }
    public function getInitialColour() : ?ColourInterface
    {
        return $this->initialColour;
    }
    public function hasInitialColour() : bool
    {
        return $this->initialColour !== null;
    }
}