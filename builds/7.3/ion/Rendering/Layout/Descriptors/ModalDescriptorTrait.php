<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Types\Arrays\Specialized\CallableMap;
use ion\Types\Arrays\Specialized\StringMapBaseInterface;
use ion\Types\Arrays\Specialized\StringMap;
use ion\Rendering\Layout\ColourInterface;
use ion\Types\StringObject;
use ion\Types\StringInterface;
trait ModalDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
    //
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setButtonColour;
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getButtonColour;
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasButtonColour;
    //    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasBackgroundColour;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::setHorizontalAlignment as setHorizontalContentAlignment;
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::getHorizontalAlignment as getHorizontalContentAlignment;
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::hasHorizontalAlignment as hasHorizontalContentAlignment;
    }
    private $modalType = null;
    private $modalActivationType = null;
    private $modalDelay = null;
    private $modalAllowMultipleActivations = false;
    private $modalContentPadding = null;
    private $modalBackgroundColour = null;
    private $modalButtons = null;
    private $buttonColour = null;
    private $cookie = null;
    private $activationFrequency = null;
    public function setActivationFrequency(int $frequency = null) : ModalDescriptorInterface
    {
        $this->activationFrequency = $frequency;
        return $this;
    }
    public function getActivationFrequency() : ?int
    {
        return $this->activationFrequency;
    }
    public function hasActivationFrequency() : bool
    {
        return $this->getActivationFrequency() !== null;
    }
    public function setCookie(string $cookie = null) : ModalDescriptorInterface
    {
        if ($cookie === null) {
            $this->cookie = null;
            return $this;
        }
        $obj = StringObject::create($cookie);
        if ($obj->isEmptyOrWhiteSpace()) {
            $this->cookie = null;
            return $this;
        }
        $this->cookie = $obj;
        return $this;
    }
    public function getCookie() : ?StringInterface
    {
        return $this->cookie;
    }
    public function hasCookie() : bool
    {
        return $this->getCookie() !== null && !$this->getCookie()->isEmptyOrWhiteSpace();
    }
    public function setButtonColour(ColourInterface $colour = null) : ModalDescriptorInterface
    {
        $this->buttonColour = $colour;
        return $this;
    }
    public function getButtonColour() : ?ColourInterface
    {
        return $this->buttonColour;
    }
    public function hasButtonColour() : bool
    {
        return $this->buttonColour !== null;
    }
    protected function setButtons(StringMapBaseInterface $buttons = null) : ModalDescriptorInterface
    {
        $this->modalButtons = $buttons === null ? StringMap::create() : $buttons;
        return $this;
    }
    public function getButtons() : StringMapBaseInterface
    {
        if ($this->modalButtons === null) {
            $this->modalButtons = StringMap::create();
        }
        return $this->modalButtons;
    }
    public function hasButtons() : bool
    {
        return $this->getButtons()->count() > 0;
    }
    public function getModalType() : ?ModalType
    {
        if ($this->modalType === null) {
            $this->setModalType(ModalType::DALOGInterface());
        }
        return $this->modalType;
    }
    public function setModalType(ModalType $modalType) : ModalDescriptorInterface
    {
        $this->modalType = $modalType;
        return $this;
    }
    public function getModalActivationType() : ModalActivationType
    {
        if ($this->modalActivationType === null) {
            $this->setModalActivationType(ModalActivationType::NONE());
        }
        return $this->modalActivationType;
    }
    public function setModalActivationType(ModalActivationType $modalActivationType) : ModalDescriptorInterface
    {
        $this->modalActivationType = $modalActivationType;
        return $this;
    }
    public function setModalDelay(int $delay) : ModalDescriptorInterface
    {
        $this->modalDelay = $delay;
        return $this;
    }
    public function getModalDelay() : int
    {
        if ($this->modalDelay === null) {
            $this->setModalDelay(0);
        }
        return $this->modalDelay;
    }
    public function setModalAllowMultipleActivations(bool $allowMultipleActivations) : ModalDescriptorInterface
    {
        $this->modalAllowMultipleActivations = $allowMultipleActivations;
        return $this;
    }
    public function getModalAllowMultipleActivations() : bool
    {
        return $this->modalAllowMultipleActivations;
    }
    public function setModalContentPadding(bool $modalContentPadding = null) : ModalDescriptorInterface
    {
        $this->modalContentPadding = $modalContentPadding;
        return $this;
    }
    public function getModalContentPadding() : ?bool
    {
        return $this->modalContentPadding;
    }
    public function hasModalContentPadding() : bool
    {
        return $this->getModalContentPadding() === true;
    }
}