<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
interface ListDescriptorInterface extends CaptionPropertyInterface, HorizontalAlignmentPropertyInterface, ListItemContainerInterface
{
}