<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

/**
 * Description of EmailFormFieldDescriptorTrait
 *
 * @author Justus
 */
trait EmailFormFieldDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptorTrait;
}