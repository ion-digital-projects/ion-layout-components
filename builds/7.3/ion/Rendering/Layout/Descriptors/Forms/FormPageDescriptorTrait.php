<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormPageDescriptor
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;
use ion\Rendering\Layout\LayoutException;
use ion\Rendering\Layout\DescriptorException;
trait FormPageDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    private $form = null;
    private $groups = null;
    public function getForm() : FormDescriptorInterface
    {
        if ($this->form === null) {
            throw new DescriptorException("Form page is not attached to a parent form.");
        }
        return $this->form;
    }
    public function getGroups() : FormGroupDescriptorVectorBaseInterface
    {
        if ($this->groups === null) {
            $this->groups = FormGroupDescriptorVector::create();
        }
        return $this->groups;
    }
    protected function setForm(FormDescriptorInterface $form = null) : FormPageDescriptorInterface
    {
        $this->form = $form;
        return $this;
    }
}