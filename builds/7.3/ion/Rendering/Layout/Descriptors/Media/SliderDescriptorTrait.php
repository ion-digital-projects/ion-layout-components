<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\ComponentException;
trait SliderDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    private $media = null;
    public function getMedia() : MediaDescriptorVectorBaseInterface
    {
        if ($this->media === null) {
            $this->media = MediaDescriptorVector::create();
        }
        return $this->media;
    }
}