<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\People\AuthorDescriptorInterface;
interface AuthorPropertyInterface
{
    function getAuthor() : ?AuthorDescriptorInterface;
    function setAuthor(AuthorDescriptorInterface $author = null) : AuthorPropertyInterface;
    function hasAuthor() : bool;
}