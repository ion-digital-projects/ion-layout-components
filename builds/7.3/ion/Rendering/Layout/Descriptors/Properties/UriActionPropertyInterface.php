<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\UriActionInterface;
interface UriActionPropertyInterface
{
    function setUriAction(UriActionInterface $action = null) : UriActionPropertyInterface;
    function getUriAction() : ?UriActionInterface;
    function hasUriAction() : bool;
    function setUriModalName(string $uriModalName = null) : UriActionPropertyInterface;
    function getUriModalName() : ?string;
    function hasUriModalName() : bool;
}