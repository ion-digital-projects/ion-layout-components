<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface OrderPropertyInterface
{
    function getOrder() : ?int;
    function setOrder(int $order = null) : OrderPropertyInterface;
    function hasOrder() : bool;
}