<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface IconPropertyInterface
{
    function getIcon() : ?StringInterface;
    function setIcon(string $icon = null) : IconPropertyInterface;
    function hasIcon() : bool;
    function setIconVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : IconPropertyInterface;
    function isIconVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : ?bool;
}