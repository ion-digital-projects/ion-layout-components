<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of DescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\ArrayObjectInterface;
use ion\Types\StringObject;
use ion\Types\Types;
use ion\Types\Arrays\Specialized\StringVector;
trait DescriptorTrait
{
    //ODOTrait: Eventually templates will become Dynamics - this is here as a stop-gap
    private $values = [];
    //    /* NOT final */
    //    protected function setPropertyValue(string $name, /* mixed*/ $value = null): DescriptorInterface {
    //
    //        //return $this->registerProperty($name, Types::MIXED(), null, true);
    //
    //        $this->values[$name] = $value;
    //        return $this;
    //    }
    //
    //    /* NOT final */
    //    protected function getPropertyValue(string $name) /* :mixed */ {
    //
    //        if(!array_key_exists($name, $this->values)) {
    //
    //            return null;
    //        }
    //
    //        return $this->values[$name];
    //    }
    /* NOT final */
    public function setStringPropertyValue(string $name, string $value = null) : DescriptorInterface
    {
        if ($value !== null) {
            return $this->setPropertyValue($name, StringObject::create($value));
        }
        return $this->setPropertyValue($name, null);
    }
}