<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\People;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Types\Arrays\Specialized\StringVectorInterface;
class PersonDescriptor extends Descriptor implements PersonDescriptorInterface
{
    use PersonDescriptorTrait;
    public function __construct(string $displayName = null, StringVectorInterface $names = null, UriInterface $uri = null, string $text = null)
    {
        parent::__construct();
        $this->setUri($uri);
        $this->setText($text);
        $this->setDisplayName($displayName);
        if ($names !== null) {
            $this->getNames()->addVector($names);
        }
    }
}