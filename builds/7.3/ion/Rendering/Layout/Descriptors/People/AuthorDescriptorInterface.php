<?php
namespace ion\Rendering\Layout\Descriptors\People;

use ion\Rendering\Layout\Descriptors\People\PersonDescriptorInterface;
interface AuthorDescriptorInterface extends PersonDescriptorInterface
{
}