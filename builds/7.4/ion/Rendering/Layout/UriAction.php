<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout;

/**
 * Description of UriAction
 *
 * @author Justus
 */
use ion\Types\EnumObject;
final class UriAction extends EnumObject implements UriActionInterface
{
    public const NONE = 0;
    public const MODAL = 1;
    public const NEW = 2;
}