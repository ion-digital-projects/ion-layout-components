<?php
namespace ion\Rendering\Layout\Components\Sections\Content;

use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\Components\Lists\GridComponentInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentDescriptorInterface;
interface ContentComponentInterface extends GridComponentInterface, ContentDescriptorInterface
{
    function setHideCaptions(bool $hideCaptions) : ContentComponentInterface;
    function getHideCaptions() : bool;
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
}