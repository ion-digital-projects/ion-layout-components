<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\TextPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\DateTimePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\AuthorPropertyInterface;
interface ContentItemDescriptorInterface extends ListItemDescriptorInterface, TextPropertyInterface, ImagePropertyInterface, DateTimePropertyInterface, AuthorPropertyInterface
{
    function setPrioritized(bool $prioritized = null) : ContentItemDescriptorInterface;
    function getPrioritized() : ?bool;
    function isPrioritized() : bool;
    function setSingular(bool $singular = null) : ContentItemDescriptorInterface;
    function getSingular() : ?bool;
    function isSingular() : bool;
}