<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections;

/**
 * Description of Form
 *
 * @author Justus
 */
class FooterSectionDescriptor extends SectionDescriptor implements FooterSectionDescriptorInterface
{
    public function __construct()
    {
        parent::__construct();
    }
}