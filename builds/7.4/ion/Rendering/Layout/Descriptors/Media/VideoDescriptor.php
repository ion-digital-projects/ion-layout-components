<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Descriptor;
class VideoDescriptor extends MediaDescriptor implements VideoDescriptorInterface
{
    use VideoDescriptorTrait;
    public function __construct(UriInterface $uri = null, string $caption = null, string $text = null, bool $visible = null)
    {
        parent::__construct($uri, $caption, $text, $visible);
    }
}