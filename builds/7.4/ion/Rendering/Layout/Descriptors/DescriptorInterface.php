<?php
namespace ion\Rendering\Layout\Descriptors;

interface DescriptorInterface
{
    function setStringPropertyValue(string $name, string $value = null) : DescriptorInterface;
}