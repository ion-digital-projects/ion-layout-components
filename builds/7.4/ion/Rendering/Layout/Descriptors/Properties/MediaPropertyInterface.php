<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
interface MediaPropertyInterface
{
    function getMedia() : ?MediaDescriptorInterface;
    function setMedia(MediaDescriptorInterface $media = null) : MediaPropertyInterface;
    function hasMedia() : bool;
}