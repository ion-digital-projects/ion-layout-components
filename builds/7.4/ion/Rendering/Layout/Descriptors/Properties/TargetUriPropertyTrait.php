<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
trait TargetUriPropertyTrait
{
    private $targetUri = null;
    public function getTargetUri() : ?UriInterface
    {
        return $this->targetUri;
    }
    public function setTargetUri(UriInterface $uri = null) : TargetUriPropertyInterface
    {
        $this->targetUri = $uri;
        return $this;
    }
    public function hasTargetUri() : bool
    {
        return $this->targetUri !== null;
    }
}