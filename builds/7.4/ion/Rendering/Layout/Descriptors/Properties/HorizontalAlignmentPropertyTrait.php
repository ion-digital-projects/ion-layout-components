<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use ion\Rendering\Layout\HorizontalAlignmentType;
trait HorizontalAlignmentPropertyTrait
{
    private $horizontalAlignment = null;
    public function setHorizontalAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null) : HorizontalAlignmentPropertyInterface
    {
        if ($horizontalAlignment === null) {
            $this->horizontalAlignment = HorizontalAlignmentType::NONE();
            return $this;
        }
        $this->horizontalAlignment = $horizontalAlignment;
        return $this;
    }
    public function getHorizontalAlignment() : HorizontalAlignmentTypeInterface
    {
        if ($this->horizontalAlignment === null) {
            $this->horizontalAlignment = HorizontalAlignmentType::NONE();
        }
        return $this->horizontalAlignment;
    }
    public function hasHorizontalAlignment() : bool
    {
        return !$this->getHorizontalAlignment()->equals(HorizontalAlignmentType::NONE());
    }
}