<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait ColumnsPropertyTrait
{
    private $columns = null;
    //    public function getColumns(): int {
    //
    //        return $this->columns;
    //    }
    //
    //    public function setColumns(int $columns): ColumnsPropertyInterface {
    //
    //        $this->columns = $columns;
    //        return $this;
    //    }
    public function getColumns() : ?int
    {
        if ($this->columns !== null && $this->columns < 1) {
            $this->columns = 1;
            return 1;
        }
        return $this->columns;
    }
    public function setColumns(int $columns = null) : ColumnsPropertyInterface
    {
        $this->columns = $columns;
        return $this;
    }
    public function hasColumns() : bool
    {
        return $this->columns !== null;
    }
}