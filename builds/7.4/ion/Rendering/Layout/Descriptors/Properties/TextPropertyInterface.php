<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface TextPropertyInterface
{
    function getText() : ?StringInterface;
    function setText(string $text = null, bool $escapeText = null) : TextPropertyInterface;
    function hasText() : bool;
    function setEscapeText(bool $escapeText = null) : TextPropertyInterface;
    function getEscapeText() : ?bool;
    function isTextEscaped() : bool;
}