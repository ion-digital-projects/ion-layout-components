<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
interface FormFieldDescriptorInterface extends CaptionPropertyInterface, NamePropertyInterface
{
    function getGroup() : FormGroupDescriptorInterface;
    function setValue(TypeObjectInterface $value = null) : FormFieldDescriptorInterface;
    function getValue() : ?TypeObjectInterface;
    function setRequired(bool $required = null) : FormFieldDescriptorInterface;
    function isRequired() : ?bool;
}