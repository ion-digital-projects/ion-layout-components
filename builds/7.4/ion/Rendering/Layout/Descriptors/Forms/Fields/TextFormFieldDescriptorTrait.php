<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

/**
 * Description of EmailFormFieldDescriptorTrait
 *
 * @author Justus
 */
trait TextFormFieldDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorTrait;
    private $multiLine = false;
    public function setMultiLine(bool $multiLine) : TextFormFieldDescriptorInterface
    {
        $this->multiLine = $multiLine;
        return $this;
    }
    public function isMultiLine() : bool
    {
        return $this->multiLine;
    }
}