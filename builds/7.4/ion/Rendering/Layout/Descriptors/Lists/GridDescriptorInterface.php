<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColumnsPropertyInterface;
/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
interface GridDescriptorInterface extends ListDescriptorInterface, ColumnsPropertyInterface
{
}