<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
trait ListDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\ColumnsPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait;
    use ListItemContainerTrait;
}