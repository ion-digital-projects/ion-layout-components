<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Lists;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\CopyableInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Markup\Html\HtmlHelper as HTML;
use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
class ListComponent extends Component implements ListComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Lists\ListDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        TemplateManager::getInstance()->registerFeature('lists');
        $this->observe($this->getItems());
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * 
     * @return ListItemComponentInterface
     */
    protected function createItemComponentInstance(ListItemDescriptorInterface $descriptor) : ListItemComponentInterface
    {
        $clientId = static::generateClientId($descriptor, $this, $this->getItems()->count());
        return ListItemComponent::createFromDescriptor($clientId, $descriptor);
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize() : ?TemplateInterface
    {
        //        TemplateManager::getInstance()->registerFeature('lists');
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getItems()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->add($instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getItems()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->insert($data->get('index'), $instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface
    {
        if ($observable === $this->getItems()) {
            $this->getChildren()->remove($data->get('index'));
        }
        return $this;
    }
}