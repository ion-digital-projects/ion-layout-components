<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components;

/**
 * Description of Header
 *
 * @author Justus
 */
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\Templates\DocumentTemplate;
use ion\WordPress\Site\LogoComponent;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\System\Remote\UriInterface;
use ion\System\Remote\Uri;
use ion\System\Remote\UriPath;
use ion\Types\Arrays\Specialized\StringVector;
use ion\WordPress\WordPressHelper as WP;
use ion\Rendering\StringRenderable;
use ion\Rendering\Markup\Html\HtmlHelper as HTML;
use ion\WordPress\Kilo\Templates\Brochure\SummaryTemplate;
use ion\WordPress\Site\FeaturedImageComponent;
use ion\WordPress\PostIdentifierInterface;
use ion\WordPress\PostIdentifier;
use ion\WordPress\Site\ActionComponent;
use ion\Rendering\Layout\Descriptors\LayoutDescriptorInterface;
use ion\PhpHelper as PHP;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use ion\Rendering\Layout\Descriptors\HeroDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
class HeroComponent extends Component implements HeroComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\HeroDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface
    {
        if (!$descriptor instanceof HeroDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setCaption($descriptor->getCaption() !== null ? $descriptor->getCaption()->toString() : null);
        //        $component->setText($descriptor->getText() !== null ? $descriptor->getText()->toString() : null, $descriptor->getEscapeText());
        $component->setMedia($descriptor->getMedia() !== null ? $descriptor->getMedia() : null);
        $component->setUri($descriptor->getUri() !== null ? $descriptor->getUri() : null);
        return $component;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null, MediaDescriptorInterface $media = null, string $caption = null, UriInterface $uri = null)
    {
        $tm = TemplateManager::getInstance();
        $tm->registerFeature('hero');
        if ($tm->hasRegisteredRootTemplate()) {
            if ($tm->getRegisteredRootTemplate() instanceof LayoutDescriptorInterface) {
                $tm->getRegisteredRootTemplate()->setFullHeight(true);
            }
        }
        $this->setCaption($caption);
        $this->setMedia($media);
        //        $this->setText($text);
        //        $this->setEscapeText($escapeText);
        $this->setUri($uri);
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        if (!$this->getStyleAssets()->isLocked()) {
            if (!$this->getStyleAssets()->hasKey('ion-components-hero')) {
                $path = UriPath::parse('/resources/styles/css/ion-components-hero.css');
                $this->getStyleAssets()->set('ion-components-hero', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize() : ?TemplateInterface
    {
        return parent::initialize();
    }
}