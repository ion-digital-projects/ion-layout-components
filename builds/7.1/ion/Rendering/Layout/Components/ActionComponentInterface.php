<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\ActionDescriptorInterface;
interface ActionComponentInterface extends ComponentInterface, ActionDescriptorInterface
{
}