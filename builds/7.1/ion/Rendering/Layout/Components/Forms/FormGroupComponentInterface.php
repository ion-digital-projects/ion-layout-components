<?php
namespace ion\Rendering\Layout\Components\Forms;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
interface FormGroupComponentInterface extends ComponentInterface, FormGroupDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface;
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null) : ObserverInterface;
}