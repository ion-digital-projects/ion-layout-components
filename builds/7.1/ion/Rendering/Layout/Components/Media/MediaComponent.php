<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Media;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVectorInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\RenderOptionsInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateVectorBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\System\Remote\UriPath;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\Rendering\Layout\TemplateManager;
use ion\Types\Arrays\Specialized\StringVector;
abstract class MediaComponent extends Component implements MediaComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface
    {
        if (!$descriptor instanceof MediaDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setCaption($descriptor->getCaption());
        $component->setUri($descriptor->getUri());
        $component->setTargetUri($descriptor->getTargetUri());
        $component->setText($descriptor->getText());
        $component->setVisible($descriptor->getVisible());
        $component->setMediaContext($descriptor->getMediaContext());
        $component->setThumbnail($descriptor->getThumbnail());
        return $component;
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        if (!$this->getStyleAssets()->isLocked()) {
            if (!$this->getStyleAssets()->hasKey('ion-components-media')) {
                $path = UriPath::parse('/resources/styles/css/ion-components-media.css');
                $this->getStyleAssets()->set('ion-components-media', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize() : ?TemplateInterface
    {
        return null;
    }
}