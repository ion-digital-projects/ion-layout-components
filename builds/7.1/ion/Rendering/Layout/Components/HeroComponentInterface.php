<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Descriptors\HeroDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
interface HeroComponentInterface extends ComponentInterface, HeroDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : ?ComponentInterface;
}