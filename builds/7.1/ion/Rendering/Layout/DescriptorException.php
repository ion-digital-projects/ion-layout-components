<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout;

use Exception;
use ion\BaseException;
class DescriptorException extends LayoutException implements DescriptorExceptionInterface
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $message = "", int $code = null, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}