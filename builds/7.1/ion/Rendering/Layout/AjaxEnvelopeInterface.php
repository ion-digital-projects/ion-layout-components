<?php
namespace ion\Rendering\Layout;

use ion\System\Remote\UriInterface;
use ion\Types\Arrays\Specialized\StringMapInterface;
use ion\Rendering\RenderableInterface;
interface AjaxEnvelopeInterface extends RenderableInterface
{
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getRedirect() : ?UriInterface;
    /**
     * method
     * 
     * @return ?int
     */
    function getErrorCode() : ?int;
    /**
     * method
     * 
     * @return ?string
     */
    function getErrorMessage() : ?string;
    /**
     * method
     * 
     * @return ?string
     */
    function getDataContext() : ?string;
    /**
     * method
     * 
     * @return StringMapInterface
     */
    function getData() : StringMapInterface;
}