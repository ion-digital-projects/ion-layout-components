<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
interface ActionDescriptorInterface extends CaptionPropertyInterface, UriPropertyInterface, ImagePropertyInterface, ColourPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    function setAutoClickDelay(int $autoClickDelay = 0) : ActionDescriptorInterface;
    /**
     * method
     * 
     * @return int
     */
    function getAutoClickDelay() : int;
    /**
     * method
     * 
     * @return bool
     */
    function hasAutoClickDelay() : bool;
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    function setAutoClickCountdown(int $autoClickCountdown = 0) : ActionDescriptorInterface;
    /**
     * method
     * 
     * @return int
     */
    function getAutoClickCountdown() : int;
    /**
     * method
     * 
     * @return bool
     */
    function hasAutoClickCountdown() : bool;
}