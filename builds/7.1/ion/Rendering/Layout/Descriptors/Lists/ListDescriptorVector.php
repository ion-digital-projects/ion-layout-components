<?php
/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\PhpHelper as PHP;
use ion\Types\TypeObjectInterface;
use ion\ImmutableInterface;
use ion\Types\Arrays\Specialized\ObjectVector;
final class ListDescriptorVector extends ListDescriptorVectorBase implements ListDescriptorVectorInterface
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$values = null, bool $immutable = false)
    {
        parent::__construct($values, $immutable);
    }
    /**
     *
     * Add a value to the end of the list.
     *
     * @param ListDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */
    public function add(ListDescriptorInterface $value) : ListDescriptorVectorInterface
    {
        return parent::_add($value);
    }
    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param ListDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */
    public function insert(int $index, ListDescriptorInterface $value) : ListDescriptorVectorInterface
    {
        return parent::_insert($index, $value);
    }
    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param ListDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */
    public function set(int $index, ListDescriptorInterface $value) : ListDescriptorVectorInterface
    {
        return parent::_set($index, $value);
    }
    /**
     *
     * Checks whether a list contains a value.
     *
     * @param ListDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */
    public function hasValue(ListDescriptorInterface $value, int $index = null) : bool
    {
        return $this->_hasValue($value, $index);
    }
    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param ListDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */
    public function removeValue(ListDescriptorInterface $value = null) : ListDescriptorVectorInterface
    {
        return $this->_removeValue($value);
    }
    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */
    public function addVector(ListDescriptorVectorBaseInterface $values) : ListDescriptorVectorInterface
    {
        return parent::_addVector($values);
    }
    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?ListDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */
    public function strip(ListDescriptorVectorBaseInterface $values = null) : ListDescriptorVectorInterface
    {
        return $this->_strip($values);
    }
}