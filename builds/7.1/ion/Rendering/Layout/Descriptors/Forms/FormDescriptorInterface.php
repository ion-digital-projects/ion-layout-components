<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Types\Arrays\MapBaseInterface;
use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorVectorBaseInterface;
interface FormDescriptorInterface extends CaptionPropertyInterface, UriPropertyInterface, NamePropertyInterface, ColourPropertyInterface
{
    /**
     * method
     * 
     * @return bool
     */
    function isAjaxEnabled() : bool;
    /**
     * method
     * 
     * @return bool
     */
    function isCaptchaEnabled() : bool;
    /**
     * method
     * 
     * 
     * @return FormDescriptorInterface
     */
    function setSubmitCaption(string $submitCaption = null) : FormDescriptorInterface;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getSubmitCaption() : ?StringInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasSubmitCaption() : bool;
    /**
     * method
     * 
     * @return FormPageDescriptorVectorBaseInterface
     */
    function getPages() : FormPageDescriptorVectorBaseInterface;
    /**
     * method
     * 
     * @return FormGroupDescriptorVectorBaseInterface
     */
    function getGroups() : FormGroupDescriptorVectorBaseInterface;
    /**
     * method
     * 
     * @return FormFieldDescriptorVectorBaseInterface
     */
    function getFields() : FormFieldDescriptorVectorBaseInterface;
    /**
     * method
     * 
     * @return MapBaseInterface
     */
    function getAdditionalValues() : MapBaseInterface;
}