<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Map;
use ion\Types\Arrays\MapBaseInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait FormDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait {
    //
    //        \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait::setUri as setActionUri;
    //        \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait::getUri as getActionUri;
    //        \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait::hasUri as hasActionUri;
    //    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setSubmitColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getSubmitColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasSubmitColour;
    }
    private $pages = null;
    private $additionalValues = null;
    private $ajaxEnabled = false;
    private $captchaEnabled = false;
    private $submitCaption = null;
    /**
     * method
     * 
     * 
     * @return FormDescriptorInterface
     */
    protected function setAjaxEnabled(bool $ajaxEnabled) : FormDescriptorInterface
    {
        $this->ajaxEnabled = $ajaxEnabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isAjaxEnabled() : bool
    {
        return $this->ajaxEnabled;
    }
    /**
     * method
     * 
     * 
     * @return FormDescriptorInterface
     */
    protected function setCaptchaEnabled(bool $captchaEnabled) : FormDescriptorInterface
    {
        $this->captchaEnabled = $captchaEnabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isCaptchaEnabled() : bool
    {
        return $this->captchaEnabled;
    }
    /**
     * method
     * 
     * 
     * @return FormDescriptorInterface
     */
    public function setSubmitCaption(string $submitCaption = null) : FormDescriptorInterface
    {
        $this->submitCaption = $submitCaption === null ? null : StringObject::create($submitCaption);
        return $this;
    }
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getSubmitCaption() : ?StringInterface
    {
        return $this->submitCaption;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasSubmitCaption() : bool
    {
        return $this->submitCaption !== null;
    }
    /**
     * method
     * 
     * @return FormPageDescriptorVectorBaseInterface
     */
    public function getPages() : FormPageDescriptorVectorBaseInterface
    {
        if ($this->pages === null) {
            $this->pages = FormPageDescriptorVector::create();
        }
        return $this->pages;
    }
    /**
     * method
     * 
     * @return FormGroupDescriptorVectorBaseInterface
     */
    public function getGroups() : FormGroupDescriptorVectorBaseInterface
    {
        $groups = FormGroupDescriptorVector::create();
        foreach ($this->getPages() as $page) {
            foreach ($page->getGroups() as $group) {
                $groups->add($group);
            }
        }
        return $groups->lock();
    }
    /**
     * method
     * 
     * @return FormFieldDescriptorVectorBaseInterface
     */
    public function getFields() : FormFieldDescriptorVectorBaseInterface
    {
        $fields = FormFieldDescriptorVector::create();
        foreach ($this->getGroups() as $group) {
            foreach ($group->getFields() as $field) {
                $fields->add($field);
            }
        }
        return $fields->lock();
    }
    /**
     * method
     * 
     * @return MapBaseInterface
     */
    public function getAdditionalValues() : MapBaseInterface
    {
        if ($this->additionalValues === null) {
            $this->additionalValues = Map::create();
        }
        return $this->additionalValues;
    }
}