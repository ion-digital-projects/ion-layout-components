<?php
/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\PhpHelper as PHP;
use ion\Types\TypeObjectInterface;
use ion\Types\Arrays\VectorException;
use ion\ImmutableInterface;
use ion\Rendering\Layout\Descriptors\DescriptorVectorBase;
abstract class FormFieldDescriptorVectorBase extends DescriptorVectorBase implements FormFieldDescriptorVectorBaseInterface
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$values = null, bool $immutable = false)
    {
        parent::__construct($values, $immutable);
    }
    /**
     * method
     * 
     * 
     * @return ?string
     */
    protected function validateArrayElement($element) : ?string
    {
        if (!PHP::isObject($element, '\\ion\\Rendering\\Layout\\Descriptors\\Forms\\FormFieldDescriptorInterface', true, true) && $element !== null) {
            return "Vector element needs to be of type 'FormFieldDescriptorInterface' - '" . gettype($element) . "' provided.";
        }
        return null;
    }
    /**
     * Return a value from the list at a specified index.
     *
     * @param int $index The index of the value to return.
     * @return mixed The value to be returned.
     *
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }
}