<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Rendering\Layout\DescriptorException;
trait FormGroupDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    private $fields = null;
    private $page = null;
    /**
     * method
     * 
     * @return FormFieldDescriptorVectorBaseInterface
     */
    public function getFields() : FormFieldDescriptorVectorBaseInterface
    {
        if ($this->fields === null) {
            $this->fields = FormFieldDescriptorVector::create();
        }
        return $this->fields;
    }
    /**
     * method
     * 
     * @return FormPageDescriptorInterface
     */
    public function getPage() : FormPageDescriptorInterface
    {
        if ($this->page === null) {
            throw new DescriptorException("Form group is not attached to a parent form page.");
        }
        return $this->page;
    }
    /**
     * method
     * 
     * 
     * @return FormGroupDescriptorInterface
     */
    protected function setPage(FormPageDescriptorInterface $page) : FormGroupDescriptorInterface
    {
        $this->page = $page;
        return $this;
    }
}