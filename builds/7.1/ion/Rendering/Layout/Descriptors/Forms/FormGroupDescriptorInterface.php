<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorInterface;
interface FormGroupDescriptorInterface extends CaptionPropertyInterface
{
    /**
     * method
     * 
     * @return FormFieldDescriptorVectorBaseInterface
     */
    function getFields() : FormFieldDescriptorVectorBaseInterface;
    /**
     * method
     * 
     * @return FormPageDescriptorInterface
     */
    function getPage() : FormPageDescriptorInterface;
}