<?php
/*
 * See license information at the package root in LCENSEInterface.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of Form
 *
 * @author Justus
 */
class LayoutDescriptor extends Descriptor implements LayoutDescriptorInterface
{
    use LayoutDescriptorTrait;
    /**
     * method
     * 
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }
}