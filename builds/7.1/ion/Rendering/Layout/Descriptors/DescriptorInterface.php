<?php
namespace ion\Rendering\Layout\Descriptors;

interface DescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return DescriptorInterface
     */
    function setStringPropertyValue(string $name, string $value = null) : DescriptorInterface;
}