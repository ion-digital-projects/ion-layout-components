<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Types\Arrays\Specialized\StringMapBaseInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyInterface;
use ion\Rendering\Layout\Descriptors\ModalType;
use ion\Rendering\Layout\Descriptors\ModalActivationType;
interface ModalDescriptorInterface extends CaptionPropertyInterface, NamePropertyInterface, ColourPropertyInterface, HorizontalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setActivationFrequency(int $frequency = null) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return ?int
     */
    function getActivationFrequency() : ?int;
    /**
     * method
     * 
     * @return bool
     */
    function hasActivationFrequency() : bool;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setCookie(string $cookie = null) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getCookie() : ?StringInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasCookie() : bool;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setButtonColour(ColourInterface $colour = null) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getButtonColour() : ?ColourInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasButtonColour() : bool;
    /**
     * method
     * 
     * @return StringMapBaseInterface
     */
    function getButtons() : StringMapBaseInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasButtons() : bool;
    /**
     * method
     * 
     * @return ?ModalType
     */
    function getModalType() : ?ModalType;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalType(ModalType $modalType) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return ModalActivationType
     */
    function getModalActivationType() : ModalActivationType;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalActivationType(ModalActivationType $modalActivationType) : ModalDescriptorInterface;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalDelay(int $delay) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return int
     */
    function getModalDelay() : int;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalAllowMultipleActivations(bool $allowMultipleActivations) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function getModalAllowMultipleActivations() : bool;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalContentPadding(bool $modalContentPadding = null) : ModalDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getModalContentPadding() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function hasModalContentPadding() : bool;
}