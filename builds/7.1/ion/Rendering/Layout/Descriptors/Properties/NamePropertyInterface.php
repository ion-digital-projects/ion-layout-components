<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface NamePropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getName() : ?StringInterface;
    /**
     * method
     * 
     * 
     * @return NamePropertyInterface
     */
    function setName(string $name = null) : NamePropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasName() : bool;
}