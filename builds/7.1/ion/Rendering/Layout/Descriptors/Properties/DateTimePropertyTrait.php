<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Types\DateTimeInterface;
use ion\Types\DateTimeObject;
trait DateTimePropertyTrait
{
    private $dateTime = null;
    /**
     * method
     * 
     * @return ?DateTimeInterface
     */
    public function getDateTime() : ?DateTimeInterface
    {
        return $this->dateTime;
    }
    /**
     * method
     * 
     * 
     * @return DateTimePropertyInterface
     */
    public function setDateTime(DateTimeInterface $dateTime = null) : DateTimePropertyInterface
    {
        $this->dateTime = $dateTime;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasDateTime() : bool
    {
        return $this->dateTime !== null;
    }
}