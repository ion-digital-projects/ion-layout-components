<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\UriActionInterface;
interface UriActionPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    function setUriAction(UriActionInterface $action = null) : UriActionPropertyInterface;
    /**
     * method
     * 
     * @return ?UriActionInterface
     */
    function getUriAction() : ?UriActionInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasUriAction() : bool;
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    function setUriModalName(string $uriModalName = null) : UriActionPropertyInterface;
    /**
     * method
     * 
     * @return ?string
     */
    function getUriModalName() : ?string;
    /**
     * method
     * 
     * @return bool
     */
    function hasUriModalName() : bool;
}