<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait TextPropertyTrait
{
    private $text = null;
    private $escapeText = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getText() : ?StringInterface
    {
        return $this->text;
    }
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    public function setText(string $text = null, bool $escapeText = null) : TextPropertyInterface
    {
        $this->setEscapeText($escapeText);
        if ($text === null) {
            $this->text = null;
            return $this;
        }
        $this->text = StringObject::create($text);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasText() : bool
    {
        return $this->text !== null;
    }
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    public function setEscapeText(bool $escapeText = null) : TextPropertyInterface
    {
        $this->escapeText = $escapeText;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getEscapeText() : ?bool
    {
        return $this->escapeText;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isTextEscaped() : bool
    {
        return $this->escapeText === true;
    }
}