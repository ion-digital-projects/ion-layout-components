<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
interface MediaPropertyInterface
{
    /**
     * method
     * 
     * @return ?MediaDescriptorInterface
     */
    function getMedia() : ?MediaDescriptorInterface;
    /**
     * method
     * 
     * 
     * @return MediaPropertyInterface
     */
    function setMedia(MediaDescriptorInterface $media = null) : MediaPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasMedia() : bool;
}