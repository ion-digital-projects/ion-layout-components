<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\ColourInterface;
interface ColourPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ColourPropertyInterface
     */
    function setColour(ColourInterface $colour = null) : ColourPropertyInterface;
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getColour() : ?ColourInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasColour() : bool;
}