<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface IconPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getIcon() : ?StringInterface;
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    function setIcon(string $icon = null) : IconPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasIcon() : bool;
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    function setIconVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : IconPropertyInterface;
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    function isIconVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : ?bool;
}