<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
trait MediaPropertyTrait
{
    private $media = null;
    private $mediaVisible = null;
    /**
     * method
     * 
     * @return ?MediaDescriptorInterface
     */
    public function getMedia() : ?MediaDescriptorInterface
    {
        return $this->media;
    }
    /**
     * method
     * 
     * 
     * @return MediaPropertyInterface
     */
    public function setMedia(MediaDescriptorInterface $media = null) : MediaPropertyInterface
    {
        $this->media = $media;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasMedia() : bool
    {
        return $this->media !== null;
    }
}