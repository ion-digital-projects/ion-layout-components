<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
trait UriPropertyTrait
{
    private $uri = null;
    /**
     * method
     * 
     * @return ?UriInterface
     */
    public function getUri() : ?UriInterface
    {
        return $this->uri;
    }
    /**
     * method
     * 
     * 
     * @return UriPropertyInterface
     */
    public function setUri(UriInterface $uri = null) : UriPropertyInterface
    {
        $this->uri = $uri;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasUri() : bool
    {
        return $this->uri !== null;
    }
}