<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
use ion\Types\Arrays\Specialized\StringVectorInterface;
interface NamesPropertyInterface
{
    /**
     * method
     * 
     * @return StringVectorInterface
     */
    function getNames() : StringVectorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasNames() : bool;
    /**
     * method
     * 
     * 
     * @return ?StringInterface
     */
    function getFullName(string $seperator = null) : ?StringInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasFirstName() : bool;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getFirstName() : ?StringInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasLastName() : bool;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getLastName() : ?StringInterface;
}