<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface TextPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getText() : ?StringInterface;
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    function setText(string $text = null, bool $escapeText = null) : TextPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasText() : bool;
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    function setEscapeText(bool $escapeText = null) : TextPropertyInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getEscapeText() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function isTextEscaped() : bool;
}