<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface ColumnsPropertyInterface
{
    /**
     * method
     * 
     * @return ?int
     */
    function getColumns() : ?int;
    /**
     * method
     * 
     * 
     * @return ColumnsPropertyInterface
     */
    function setColumns(int $columns = null) : ColumnsPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasColumns() : bool;
}