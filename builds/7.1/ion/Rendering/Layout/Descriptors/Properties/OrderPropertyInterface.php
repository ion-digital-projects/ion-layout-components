<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface OrderPropertyInterface
{
    /**
     * method
     * 
     * @return ?int
     */
    function getOrder() : ?int;
    /**
     * method
     * 
     * 
     * @return OrderPropertyInterface
     */
    function setOrder(int $order = null) : OrderPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasOrder() : bool;
}