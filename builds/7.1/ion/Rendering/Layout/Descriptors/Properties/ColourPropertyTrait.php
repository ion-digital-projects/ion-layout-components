<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
trait ColourPropertyTrait
{
    private $colour = null;
    /**
     * method
     * 
     * 
     * @return ColourPropertyInterface
     */
    public function setColour(ColourInterface $colour = null) : ColourPropertyInterface
    {
        $this->colour = $colour;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    public function getColour() : ?ColourInterface
    {
        return $this->colour;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasColour() : bool
    {
        return $this->colour !== null;
    }
}