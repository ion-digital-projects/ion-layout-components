<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
interface VerticalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return VerticalAlignmentPropertyInterface
     */
    function setVerticalAlignment(VerticalAlignmentTypeInterface $verticalAlignment = null) : VerticalAlignmentPropertyInterface;
    /**
     * method
     * 
     * @return VerticalAlignmentTypeInterface
     */
    function getVerticalAlignment() : VerticalAlignmentTypeInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasVerticalAlignment() : bool;
}