<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface CaptionPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getCaption() : ?StringInterface;
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    function setCaption(string $caption = null) : CaptionPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasCaption() : bool;
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    function setCaptionVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : CaptionPropertyInterface;
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    function isCaptionVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : ?bool;
}