<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
interface ImagePropertyInterface
{
    /**
     * method
     * 
     * @return ?ImageDescriptorInterface
     */
    function getImage() : ?ImageDescriptorInterface;
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    function setImage(ImageDescriptorInterface $image = null) : ImagePropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasImage() : bool;
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    function setImageVisible(bool $visible = null) : ImagePropertyInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function isImageVisible() : ?bool;
}