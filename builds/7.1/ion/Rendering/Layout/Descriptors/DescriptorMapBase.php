<?php
/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */
namespace ion\Rendering\Layout\Descriptors;

use ion\PhpHelper as PHP;
use ion\Types\Arrays\MapBaseInterface;
use ion\Types\Arrays\VectorBaseInterface;
use ion\Types\Arrays\MapException;
use ion\Types\StringObjectInterface;
use ion\Types\StringObject;
use ion\Types\Arrays\Specialized\ObjectVectorBase;
use ion\Rendering\Layout\DescriptorMapBase;
abstract class DescriptorMapBase extends ObjectMapBase implements DescriptorMapBaseInterface
{
    /**
     * method
     * 
     * 
     * @return VectorBaseInterface
     */
    protected static function createVector(array $values = null) : VectorBaseInterface
    {
        return DescriptorVector::create($values);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$values = null, bool $immutable = false)
    {
        parent::__construct($values, $immutable);
    }
    /**
     *
     * Return a value from the map by key.
     *
     * @param DescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */
    public function get(string $key)
    {
        return $this->_get($key);
    }
    /**
     * method
     * 
     * 
     * @return ?string
     */
    protected function validateArrayElement($element) : ?string
    {
        if (!PHP::isObject($element, '\\ion\\Rendering\\Layout\\Descriptors\\DescriptorInterface', true, true) && $element !== null) {
            return "Map element needs to be of type 'DescriptorInterface.'";
        }
        return null;
    }
}