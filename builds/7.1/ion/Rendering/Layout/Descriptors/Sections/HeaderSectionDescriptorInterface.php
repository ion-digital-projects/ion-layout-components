<?php
namespace ion\Rendering\Layout\Descriptors\Sections;

use ion\Rendering\Layout\Descriptors\Sections\SectionDescriptorInterface;
/**
 * Description of Form
 *
 * @author Justus
 */
interface HeaderSectionDescriptorInterface extends SectionDescriptorInterface
{
}