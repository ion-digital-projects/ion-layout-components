<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of ContentDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\Arrays\MapInterface;
use ion\Types\Arrays\Map;
use ion\ObservableInterface;
trait ContentDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Lists\GridDescriptorTrait;
    private $imagesVisible = null;
    //    private $items = null;
    //
    //    public function getItems(): ContentItemDescriptorVectorBaseInterface {
    //
    //        if($this->items === null) {
    //
    //            $this->items = ContentItemDescriptorVector::create();
    //        }
    //
    //        return $this->items;
    //    }
    /**
     * method
     * 
     * 
     * @return ContentDescriptorInterface
     */
    public function setImagesVisible(bool $visible = null) : ContentDescriptorInterface
    {
        $this->imagesVisible = $visible;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getImagesVisible() : ?bool
    {
        return $this->imagesVisible;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function areImagesVisible() : bool
    {
        return $this->imagesVisible === true;
    }
}