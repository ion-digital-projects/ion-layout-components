<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\TextPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\DateTimePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\AuthorPropertyInterface;
interface ContentItemDescriptorInterface extends ListItemDescriptorInterface, TextPropertyInterface, ImagePropertyInterface, DateTimePropertyInterface, AuthorPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ContentItemDescriptorInterface
     */
    function setPrioritized(bool $prioritized = null) : ContentItemDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getPrioritized() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function isPrioritized() : bool;
    /**
     * method
     * 
     * 
     * @return ContentItemDescriptorInterface
     */
    function setSingular(bool $singular = null) : ContentItemDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getSingular() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function isSingular() : bool;
}