<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Rendering\Layout\Descriptors\Lists\GridDescriptorInterface;
interface ContentDescriptorInterface extends GridDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ContentDescriptorInterface
     */
    function setImagesVisible(bool $visible = null) : ContentDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getImagesVisible() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function areImagesVisible() : bool;
}