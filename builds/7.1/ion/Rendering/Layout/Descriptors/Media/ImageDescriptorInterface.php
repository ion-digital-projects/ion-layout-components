<?php
namespace ion\Rendering\Layout\Descriptors\Media;

use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
interface ImageDescriptorInterface extends MediaDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    function setRound(bool $round = null) : ImageDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getRound() : ?bool;
    /**
     * method
     * 
     * @return bool
     */
    function isRound() : bool;
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    function setColour(ColourInterface $colour = null) : ImageDescriptorInterface;
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getColour() : ?ColourInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasColour() : bool;
}