<?php
namespace ion\Rendering\Layout\Descriptors\Media;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\TargetUriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\TextPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\VisiblePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\MediaContextPropertyInterface;
interface MediaDescriptorInterface extends CaptionPropertyInterface, UriPropertyInterface, TargetUriPropertyInterface, TextPropertyInterface, VisiblePropertyInterface, MediaContextPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return MediaDescriptorInterface
     */
    function setThumbnail(MediaDescriptorInterface $thumbnail = null) : MediaDescriptorInterface;
    /**
     * method
     * 
     * @return ?MediaDescriptorInterface
     */
    function getThumbnail() : ?MediaDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasThumbnail() : bool;
}