<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of Descriptor
 *
 * @author Justus
 */
use ion\Base;
abstract class Descriptor extends Base implements DescriptorInterface
{
    use DescriptorTrait;
    /**
     * method
     * 
     * @return mixed
     */
    public function __construct()
    {
        // empty!
    }
}