<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use \ion\Rendering\Layout\VerticalAlignmentTypeInterface;

interface AlignmentPropertyInterface {

    function setAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null, VerticalAlignmentTypeInterface $verticalAlignment = null): AlignmentPropertyInterface;

    function hasAlignment(): bool;

}
