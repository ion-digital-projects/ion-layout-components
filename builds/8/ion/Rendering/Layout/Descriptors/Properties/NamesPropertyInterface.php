<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\StringInterface;
use \ion\Types\Arrays\Specialized\StringVectorInterface;

interface NamesPropertyInterface {

    function getNames(): StringVectorInterface;

    function hasNames(): bool;

    function getFullName(string $seperator = null): ?StringInterface;

    function hasFirstName(): bool;

    function getFirstName(): ?StringInterface;

    function hasLastName(): bool;

    function getLastName(): ?StringInterface;

}
