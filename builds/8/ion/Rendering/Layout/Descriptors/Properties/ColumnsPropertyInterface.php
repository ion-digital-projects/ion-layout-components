<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

interface ColumnsPropertyInterface {

    function getColumns(): ?int;

    function setColumns(int $columns = null): ColumnsPropertyInterface;

    function hasColumns(): bool;

}
