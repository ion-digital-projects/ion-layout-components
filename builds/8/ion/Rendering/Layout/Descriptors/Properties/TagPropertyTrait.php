<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait TagPropertyTrait {

    private $tag = null;
    
    public function getTag(): ?StringInterface {
        
        return $this->tag;
    }
    
    public function setTag(string $tag = null): TagPropertyInterface {
        
        if($tag === null) {
            
            $this->tag = null;
            return $this;
        }
        
        $this->tag = StringObject::create($tag);
        return $this;
    }
    
    public function hasTag(): bool {
        
        return ($this->tag !== null);
    }    
}