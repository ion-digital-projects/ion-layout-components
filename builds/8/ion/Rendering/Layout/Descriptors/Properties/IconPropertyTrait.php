<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait IconPropertyTrait {

    private $icon = null;
    private $iconVisible = null;
    
    public function getIcon(): ?StringInterface {
        
        if($this->icon === null) {
            
            return null;
        }
        
        return $this->icon;
    }
    
    public function setIcon(string $icon = null): IconPropertyInterface {
        
        if($icon === null) {
            
            $this->icon = null;
            return $this;
        }
        
        $this->icon = StringObject::create($icon);
        return $this;
    }
    
    public function hasIcon(): bool {
        
        return ($this->icon !== null);
    }    
    
    
    public function setIconVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null): IconPropertyInterface {
                        
        $this->iconVisible = [
            'default' => $default,
            'large' => $onLargeScreens,
            'medium' => $onMediumScreens,
            'small' => $onSmallScreens
        ];
        
        return $this;
    }
    
    public function isIconVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null): ?bool {
        
        if($this->iconVisible === null) {
            
            return null;
        }        
        
        $result = null;
        
        if($onLargeScreens === true) {
            
            if($this->iconVisible['large'] === false) {
                
                return false;
            }
            
            $result = true;
        }
        
        if($onMediumScreens === true) {
            
            if($this->iconVisible['medium'] === false) {
                
                return false;
            }        
            
            $result = true;
        }

        if($onSmallScreens === true) {
            
            if($this->iconVisible['small'] === false) {
                
                return false;
            }     
            
            $result = true;
        }        
        
        if($result !== null) {
            
            return $result;
        }
        
        return $this->iconVisible['default'];
    }    
}