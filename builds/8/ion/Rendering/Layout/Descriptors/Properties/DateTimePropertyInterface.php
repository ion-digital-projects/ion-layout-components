<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\DateTimeInterface;

interface DateTimePropertyInterface {

    function getDateTime(): ?DateTimeInterface;

    function setDateTime(DateTimeInterface $dateTime = null): DateTimePropertyInterface;

    function hasDateTime(): bool;

}
