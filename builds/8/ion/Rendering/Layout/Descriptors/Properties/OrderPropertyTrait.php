<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait OrderPropertyTrait {

    private $order = null;
    
    public function getOrder(): ?int {
        
        return $this->order;
    }
    
    public function setOrder(int $order = null): OrderPropertyInterface {
        
        if($order === null) {
            
            $this->order = null;
            return $this;
        }
        
        $this->order = $order;
        return $this;
    }
    
    public function hasOrder(): bool {
        
        return ($this->order !== null);
    }    
}