<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\System\Remote\UriInterface;

interface TargetUriPropertyInterface {

    function getTargetUri(): ?UriInterface;

    function setTargetUri(UriInterface $uri = null): TargetUriPropertyInterface;

    function hasTargetUri(): bool;

}
