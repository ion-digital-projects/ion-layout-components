<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\System\Remote\UriInterface;
use \ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;

trait UriPropertyTrait {

    private $uri = null;
    
    public function getUri(): ?UriInterface {
        
        return $this->uri;
    }
    
    public function setUri(UriInterface $uri = null): UriPropertyInterface {
        
        $this->uri = $uri;
        return $this;
    }
    
    public function hasUri(): bool {
        
        return ($this->uri !== null);
    }

}