<?php

namespace ion\Rendering\Layout\Descriptors\Properties;

use \ion\Types\StringInterface;

interface NamePropertyInterface {

    function getName(): ?StringInterface;

    function setName(string $name = null): NamePropertyInterface;

    function hasName(): bool;

}
