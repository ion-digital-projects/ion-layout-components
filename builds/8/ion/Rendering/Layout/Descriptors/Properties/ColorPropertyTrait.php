<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Types\StringInterface;
use \ion\Types\StringObject;

trait ColorPropertyTrait {

    use ColourPropertyTrait {
        
        getColour as getColor;
        setColour as setColor;
        hasColour as hasColor;        
    }
}