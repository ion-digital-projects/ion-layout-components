<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;

trait ImagePropertyTrait {

//    use MediaPropertyTrait {
//        
//        MediaPropertyTrait::setMedia as private MediaPropertyTrait_setMedia;
//        MediaPropertyTrait::getMedia as private MediaPropertyTrait_getMedia;
//        MediaPropertyTrait::hasMedia as private MediaPropertyTrait_hasMedia;
//        MediaPropertyTrait::setMediaVisible as private MediaPropertyTrait_setMediaVisible;
//        MediaPropertyTrait::isMediaVisible as private MediaPropertyTrait_isMediaVisible;
//    }
    
    private $image = null;
    private $imageVisible = null;
    
    public function getImage(): ?ImageDescriptorInterface {
        
        return $this->image;
    }
    
    public function setImage(ImageDescriptorInterface $image = null): ImagePropertyInterface {
        
        $this->image = $image;
        return $this;
    }
    
    public function hasImage(): bool {
        
        return ($this->image !== null);
    }    
    
    public function setImageVisible(bool $visible = null): ImagePropertyInterface {
        
        $this->imageVisible = $visible;
        return $this;
    }
    
    public function isImageVisible(): ?bool {
        
        return $this->imageVisible;
    }
 
}