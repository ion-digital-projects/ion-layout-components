<?php //

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Properties\VisibleInterface;

trait VisiblePropertyTrait {

    private $visible = null;
    
    public function getVisible(): ?bool {
        
        return $this->visible;
    }
    
    public function setVisible(bool $visible = null): VisiblePropertyInterface {
        
        $this->visible = $visible;
        return $this;
    }
    
    public function isVisible(): bool {
        
        return ($this->getVisible() !== false);
    }    
 
}