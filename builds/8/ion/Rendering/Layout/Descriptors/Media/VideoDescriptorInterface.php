<?php

namespace ion\Rendering\Layout\Descriptors\Media;

use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;

interface VideoDescriptorInterface extends MediaDescriptorInterface {

    // No public methods!

}
