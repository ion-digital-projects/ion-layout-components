<?php

/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */

namespace ion\Rendering\Layout\Descriptors\Media;

use \ion\PhpHelper as PHP;
use \ion\Types\Arrays\MapBaseInterface;
use \ion\Types\Arrays\VectorBaseInterface;
use \ion\Types\Arrays\MapException;
use \ion\Types\StringObjectInterface;
use \ion\Types\StringObject;
use \ion\Rendering\Layout\DescriptorMapBase;

abstract class MediaDescriptorMapBase extends MediaDescriptorMapBase implements MediaDescriptorMapBaseInterface {

    protected static function createVector(array $values = null): VectorBaseInterface {
    
        return MediaDescriptorVector::create($values);
    }        


    public function __construct(array &$values = null, bool $immutable = false) {
    
        parent::__construct($values, $immutable);
    }   
                
            

    /**
     *
     * Return a value from the map by key.
     *
     * @param MediaDescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */
  
    public function get(string $key): ?object {
    
        return $this->_get($key);
    }      
    
    protected function validateArrayElement($element): ?string {

        if(!(PHP::isObject($element, '\ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface', true, true)) && ($element !== null)) {
        
            return "Map element needs to be of type 'MediaDescriptorInterface.'";
        }

        return null;
    }        
    
}