<?php

namespace ion\Rendering\Layout\Descriptors\Forms;

use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;

interface FormFieldDescriptorVectorInterface {

    /**
     *
     * Add a value to the end of the list.
     *
     * @param FormFieldDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */

    function add(FormFieldDescriptorInterface $value): FormFieldDescriptorVectorInterface;

    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param FormFieldDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */

    function insert(int $index, FormFieldDescriptorInterface $value): FormFieldDescriptorVectorInterface;

    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param FormFieldDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */

    function set(int $index, FormFieldDescriptorInterface $value): FormFieldDescriptorVectorInterface;

    /**
     *
     * Checks whether a list contains a value.
     *
     * @param FormFieldDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(FormFieldDescriptorInterface $value, int $index = null): bool;

    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param FormFieldDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */

    function removeValue(FormFieldDescriptorInterface $value = null): FormFieldDescriptorVectorInterface;

    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */

    function addVector(FormFieldDescriptorVectorBaseInterface $values): FormFieldDescriptorVectorInterface;

    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?FormFieldDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */

    function strip(FormFieldDescriptorVectorBaseInterface $values = null): FormFieldDescriptorVectorInterface;

}
