<?php

namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

use \ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptorInterface;


/**
 * Description of EmailFormFieldDescriptorTrait
 *
 * @author Justus
 */
interface EmailFormFieldDescriptorInterface extends TextFormFieldDescriptorInterface {

    // No public methods!

}
