<?php

namespace ion\Rendering\Layout\Descriptors\Forms;

use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;

interface FormGroupDescriptorVectorInterface {

    /**
     *
     * Add a value to the end of the list.
     *
     * @param FormGroupDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */

    function add(FormGroupDescriptorInterface $value): FormGroupDescriptorVectorInterface;

    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param FormGroupDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */

    function insert(int $index, FormGroupDescriptorInterface $value): FormGroupDescriptorVectorInterface;

    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param FormGroupDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */

    function set(int $index, FormGroupDescriptorInterface $value): FormGroupDescriptorVectorInterface;

    /**
     *
     * Checks whether a list contains a value.
     *
     * @param FormGroupDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */

    function hasValue(FormGroupDescriptorInterface $value, int $index = null): bool;

    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param FormGroupDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */

    function removeValue(FormGroupDescriptorInterface $value = null): FormGroupDescriptorVectorInterface;

    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */

    function addVector(FormGroupDescriptorVectorBaseInterface $values): FormGroupDescriptorVectorInterface;

    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?FormGroupDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */

    function strip(FormGroupDescriptorVectorBaseInterface $values = null): FormGroupDescriptorVectorInterface;

}
