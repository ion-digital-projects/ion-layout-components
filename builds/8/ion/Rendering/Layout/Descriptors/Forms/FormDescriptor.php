<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of Form
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Descriptors\Descriptor;

class FormDescriptor extends Descriptor implements FormDescriptorInterface {
    
    use FormDescriptorTrait;
    
    public function __construct() {
        
        parent::__construct();
    }   
}
