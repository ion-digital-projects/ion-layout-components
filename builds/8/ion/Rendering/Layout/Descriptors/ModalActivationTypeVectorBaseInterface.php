<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Rendering\Layout\Descriptors\DescriptorVectorBaseInterface;

interface ModalActivationTypeVectorBaseInterface extends DescriptorVectorBaseInterface {

    /**
     
     * Return a value from the list at a specified index.
     *
     * @param int $index The index of the value to return.
     * @return mixed The value to be returned.
     *
     */

    function get(int $index): ?object;

}
