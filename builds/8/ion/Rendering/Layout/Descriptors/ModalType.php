<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalType
 *
 * @author Justus
 */

use \ion\Types\EnumObject;

class ModalType extends EnumObject {
    
    public const DIALOG = 1;
    public const NOTICE = 2;
}
