<?php

namespace ion\Rendering\Layout\Descriptors;

use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;

interface ActionDescriptorInterface extends 

    CaptionPropertyInterface,
    UriPropertyInterface,
    ImagePropertyInterface,
    ColourPropertyInterface

 {

    function setAutoClickDelay(int $autoClickDelay = 0): ActionDescriptorInterface;

    function getAutoClickDelay(): int;

    function hasAutoClickDelay(): bool;

    function setAutoClickCountdown(int $autoClickCountdown = 0): ActionDescriptorInterface;

    function getAutoClickCountdown(): int;

    function hasAutoClickCountdown(): bool;

}
