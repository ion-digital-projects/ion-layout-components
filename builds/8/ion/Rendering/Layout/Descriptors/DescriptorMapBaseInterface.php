<?php

namespace ion\Rendering\Layout\Descriptors;

interface DescriptorMapBaseInterface {

    /**
     *
     * Return a value from the map by key.
     *
     * @param DescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */

    function get(string $key): ?object;

}
