<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of ContentDescriptor
 *
 * @author Justus
 */

use \ion\Rendering\Layout\Descriptors\Descriptor;

class ContentDescriptor extends Descriptor implements ContentDescriptorInterface {
    
    use ContentDescriptorTrait;
    
    public function __construct() {
        
        parent::__construct();
    } 
}
