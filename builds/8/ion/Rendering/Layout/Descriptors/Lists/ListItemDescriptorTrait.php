<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */

use \ion\Rendering\Layout\ComponentException;

trait ListItemDescriptorTrait {

    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\TagPropertyTrait;  
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriActionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\IconPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\OrderPropertyTrait;
    use ListItemContainerTrait;
    

    private $list = null;
    
    public function getParentContainer(): ListItemContainerInterface {
        
        if($this->list === null) {
            
            throw new ComponentException("Item does not have a parent container.");
        }
        
        return $this->list;
    }
    
    protected function setParentContainer(ListItemContainerInterface $list): ListItemDescriptorInterface {
        
        $this->list = $list;
        return $this;
    }                
    
    public function setIndex(int $index = null) : ListItemDescriptorInterface {
        
        $this->index = $index;
        return $this;
    }
    
    public function getIndex(): ?int {
        
        return $this->index;
    }
    
    public function hasIndex(): bool {
        
        return ($this->index !== null);
    }    
}
