<?php

namespace ion\Rendering\Layout;

use \ion\Types\EnumObjectInterface;

interface MediaContextInterface extends EnumObjectInterface {

    // No public methods!

}
