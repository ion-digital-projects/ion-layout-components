<?php

namespace ion\Rendering\Layout;

use \ion\System\Remote\UriInterface;
use \ion\Types\Arrays\Specialized\StringMapInterface;
use \ion\Rendering\RenderableInterface;

interface AjaxEnvelopeInterface extends RenderableInterface {

    function getRedirect(): ?UriInterface;

    function getErrorCode(): ?int;

    function getErrorMessage(): ?string;

    function getDataContext(): ?string;

    function getData(): StringMapInterface;

}
