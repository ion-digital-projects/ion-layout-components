<?php

namespace ion\Rendering\Layout\Components\Media;

use \ion\Rendering\RenderOptionsInterface;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Media\SliderDescriptorInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;

interface SliderComponentInterface extends SliderDescriptorInterface {

    static function createFromDescriptor(

        string $clientId,
        DescriptorInterface $descriptor,
        RenderOptionsInterface $renderOptions = null,
        TemplateInterface $parent = null,
        CallableMapBaseInterface $hooks = null

    ): ?ComponentInterface;

}
