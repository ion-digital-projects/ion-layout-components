<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Media;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */

use \ion\Rendering\RenderableVectorBaseInterface;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Template;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\Layout\TemplateVectorBaseInterface;
use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Component;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;

class VideoComponent extends MediaComponent implements ImageComponentInterface {

    use \ion\Rendering\Layout\Descriptors\Media\ImageDescriptorTrait;

    public static function createFromDescriptor(
            
            string $clientId, 
            DescriptorInterface $descriptor, 
            RenderOptionsInterface $renderOptions = null,
            TemplateInterface $parent = null, 
            CallableMapBaseInterface $hooks = null
            
        ): ?ComponentInterface {
        
        if(!($descriptor instanceof ImageDescriptorInterface)) {
            
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        
        $component = parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
                
        return $component;
    }    
    
    public function __construct(
            
            string $clientId,
            RenderOptionsInterface $renderOptions = null,
            TemplateInterface $parent = null,
            CallableMapBaseInterface $hooks = null
            
        ) {

        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }    
    
    protected function initialize(): ?TemplateInterface {
        
        return null;
    }    

}
