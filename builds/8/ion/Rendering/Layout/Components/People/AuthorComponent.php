<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\People;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */

use \ion\Rendering\RenderableVectorBaseInterface;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Template;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\Layout\TemplateVectorBaseInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\Rendering\Layout\Component;

class AuthorComponent extends PersonComponent implements AuthorComponentInterface {

    use \ion\Rendering\Layout\Descriptors\People\AuthorDescriptorTrait;

    public function __construct(string $clientId, TemplateInterface $parent = null, RenderOptionsInterface $renderOptions = null, CallableMapBaseInterface $hooks = null) {

        parent::__construct($clientId, $parent, $renderOptions, $hooks);
    }    
    
    protected function initialize(): ?TemplateInterface {
        
        return null;
    }    

}
