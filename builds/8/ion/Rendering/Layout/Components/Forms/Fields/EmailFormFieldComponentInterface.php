<?php

namespace ion\Rendering\Layout\Components\Forms\Fields;

use \ion\Rendering\Layout\Components\Forms\FormFieldComponentInterface;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptorInterface;

interface EmailFormFieldComponentInterface extends FormFieldComponentInterface, EmailFormFieldDescriptorInterface {

    // No public methods!

}
