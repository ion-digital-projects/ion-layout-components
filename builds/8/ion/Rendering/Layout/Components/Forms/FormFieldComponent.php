<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */

use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Component;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;

abstract class FormFieldComponent extends Component implements FormFieldComponentInterface {
    
    use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorTrait;
    
    public static function createFromDescriptor(
            
            string $clientId,
            DescriptorInterface $descriptor,
            RenderOptionsInterface $renderOptions = null,
            TemplateInterface $parent = null,
            CallableMapBaseInterface $hooks = null
            
        ): ?ComponentInterface {
        
        if(!($descriptor instanceof FormFieldDescriptorInterface)) {
            
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        
        
        $component->setCaption($descriptor->getCaption());
        $component->setName($descriptor->getName());
        $component->setGroup($descriptor->getGroup());
        $component->setValue($descriptor->getValue());
        
        return $component;
    }
    
    public function __construct(
            
            string $clientId,
            RenderOptionsInterface $renderOptions = null,
            TemplateInterface $parent = null,
            CallableMapBaseInterface $hooks = null
            
        ) {

        if($parent !== null) {
                    
            $this->setGroup($parent);         
        }
        
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }      
    
    protected function initialize(): ?TemplateInterface {

        return null;
    }
}
