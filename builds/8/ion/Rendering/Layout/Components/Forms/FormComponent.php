<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */

use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Component;
use \ion\Rendering\Layout\ComponentException;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Types\Arrays\MapInterface;
use \ion\Types\Arrays\Map;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\ObservableInterface;
use \ion\ObserverInterface;
use \ion\System\Remote\UriPath;
use \ion\Rendering\Layout\Assets\StyleAsset;
use \ion\Rendering\Layout\Assets\ScriptAsset;

use \ion\Types\StringInterface;
use \ion\System\Remote\Uri;
use \ion\PhpHelper as PHP;

use \ion\Rendering\Layout\TemplateManager;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;

use \ion\Types\Arrays\Specialized\StringVector;

use \ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptorInterface;

use \ion\Rendering\Layout\Components\Forms\FormFieldComponentInterface;
use \ion\Rendering\Layout\Components\Forms\Fields\EmailFormFieldComponent;
use \ion\Rendering\Layout\Components\Forms\Fields\TextFormFieldComponent;


class FormComponent extends Component implements FormComponentInterface {
    
    use \ion\Rendering\Layout\Descriptors\Forms\FormDescriptorTrait;
    
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null): ?ComponentInterface {
        
        if(!($descriptor instanceof FormDescriptorInterface)) {
            
            throw new ComponentException("The descriptor does not implement 'ion\\Rendering\\Layout\\Components\\Forms\\FormDescriptorInterface.'");
        }
        
        $form = static::create($clientId, $renderOptions, $parent, $hooks);
        
        $form->setCaption($descriptor->getCaption());
        
        foreach($descriptor->getPages() as $page) {
            
            $form->getPages()->add($page);
            
            // TODO: Confirm that we only have to add pages - shouldn't we copy these objects?
            
//            foreach($page->getGroups() as $group) {
//                
//                foreach($group->getFields() as $field) {
//                    
//                    
//                }
//            }
        }
        
        return $form;
    }    
    
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) {
        
        TemplateManager::getInstance()->registerFeature('forms');
        
        $this->observe($this->getPages());    
        $this->setCacheEnabled(false);                
        
        parent::__construct($clientId, $renderOptions, $parent, $hooks);

//        $this->setClientIdRequired(true);
        
//        if(!$this->hasName()) {
//            
//            throw new ComponentException("A form requires a name (use \$this->setName() in the initialize() method).");
//        }
        
//        if($this->hasName()) {
//       
//            $this->getAdditionalValues()->set('form-name', $this->getName()->toString());
//        }
        
        if(!$this->getStyleAssets()->hasKey('ion-components-form')) {

            $path = UriPath::parse('/resources/styles/css/ion-components-form.css');
            
            $this->getStyleAssets()->set(
                    'ion-components-form', 
                    StyleAsset::reference(
                            TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(),
                            15,
                            StringVector::create(['screen']),
                            TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')
                            ));
        }
        
        if(!$this->getScriptAssets()->hasKey('ion-components-form')) {        
        
            $path = UriPath::parse('/resources/scripts/js/ion-components-form.js');
            
            $this->getScriptAssets()->set(
                    'ion-components-form', 
                    ScriptAsset::reference(
                            TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(),
                            15,
                            true,
                            TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')
                            ));  
        }                
    }        
    
    protected function createItemComponentInstance(FormPageDescriptorInterface $descriptor): FormPageComponentInterface {
        
        $clientId = static::generateClientId($descriptor, $this, $this->getPages()->count());
        
        return FormPageComponent::createFromDescriptor($clientId, $descriptor, null, $this);
    }        
    
    protected function initialize(): ?TemplateInterface {        
        
        return $this;
    }    
    
//    public function getClientId(): ?StringInterface {
//        
////        if(!$this->hasClientId()) {
////            
////            return StringObject::create(static::class)->toDashed()->append("-{$this->getId()}");
////        }
//        
//        return parent::getClientId();
//    }         
    
    
    protected function onRender(StringInterface $buffer, RenderOptionsInterface $renderOptions): StringInterface {

        $formLocation = null;
        
        if(!PHP::isEmpty(PHP::getServerRequestUri())) {
            
            $formLocation = Uri::parse(PHP::getServerRequestUri());
        }
        
        $toDelete = [];

        $this->getAdditionalValues()->set(
                
            'client-id',
            $this->getClientId()->toString()
        );
        
        if(!PHP::isEmpty($formLocation)) {
            
            foreach($formLocation->getQuery()->getKeys() as $key) {

                if(PHP::strStartsWith($key, 'form-')) {

                    $toDelete[] = $key;
                }
            }

            foreach($toDelete as $key) {

                $formLocation->getQuery()->remove($key);
            }
            
            $this->getAdditionalValues()->set('form-location', $formLocation->toString());
        }
        
        return parent::onRender($buffer, $renderOptions);
    }     
    
    
    
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null): ObserverInterface {
        
        if($observable === $this->getPages()) {
            
            $instance = $data->get('value');
            
            if(!($instance instanceof ComponentInterface)) {
                
                $instance = $this->createItemComponentInstance($instance);
            }
            
            $this->getChildren()->add($instance);
        }
        
        return $this;
    }
    
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null): ObserverInterface {
        
        if($observable === $this->getPages()) {
            
            $instance = $data->get('value');
            
            if(!($instance instanceof ComponentInterface)) {
                
                $instance = $this->createItemComponentInstance($instance);
            }        
            
            $this->getChildren()->insert($data->get('index'), $instance);
        }
        
        return $this;
    }     

    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null): ObserverInterface {
        
        if($observable === $this->getPages()) {
            
            $this->getChildren()->remove($data->get('index'));
        }
        
        return $this;
    }        

}
