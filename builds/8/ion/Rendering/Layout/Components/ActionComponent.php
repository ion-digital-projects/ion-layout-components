<?php

/*
 * See license information at the package root in LICENSE.md
 */

namespace ion\Rendering\Layout\Components;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */

use \ion\PhpHelper as PHP;
use \ion\Rendering\RenderableVectorBaseInterface;
use \ion\Rendering\RenderableVectorInterface;
use \ion\Rendering\RenderableVector;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\Template;
use \ion\Rendering\Layout\TemplateVectorInterface;
use \ion\Rendering\Layout\TemplateVectorBaseInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\Rendering\Layout\Component;
use \ion\Rendering\Layout\ColourInterface;
use \ion\System\Remote\UriPath;
use \ion\Rendering\Layout\Assets\StyleAsset;
use \ion\Rendering\Layout\Assets\ScriptAsset;
use \ion\Rendering\Layout\TemplateManager;
use \ion\Types\Arrays\Specialized\StringVector;
use \ion\Types\StringObject;
use \ion\System\Remote\UriInterface;

class ActionComponent extends Component implements ActionComponentInterface {
        
    use \ion\Rendering\Layout\Descriptors\ActionDescriptorTrait;
    
    public function __construct(
            
            string $clientId,
            RenderOptionsInterface $renderOptions = null, 
            TemplateInterface $parent = null, 
            CallableMapBaseInterface $hooks = null,
            string $caption = null, 
            ColourInterface $colour = null,
            UriInterface $uri = null,
            string $onClickScript = null,
            int $autoClickDelay = 0,
            int $autoClickCountdown = 0
    ) {  

        $this->setCaption($caption);
        $this->setColour($colour);
        $this->setUri($uri);
        $this->setAutoClickDelay($autoClickDelay);
        $this->setAutoClickCountdown($autoClickCountdown);

        parent::__construct($clientId, $renderOptions, $parent, $hooks);        
        
        if(!$this->getStyleAssets()->isLocked()) {        
        
            if(!$this->getStyleAssets()->hasKey('ion-components-action')) {

                $path = UriPath::parse('/resources/styles/css/ion-components-action.css');

                $this->getStyleAssets()->set(
                        'ion-components-action', 
                        StyleAsset::reference(
                                TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(),
                                15,
                                StringVector::create(['screen']),
                                TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')
                                ));
            }
        }        
        
        if(!$this->getScriptAssets()->isLocked()) {
            
            if(!$this->getScriptAssets()->hasKey('ion-components-action')) {        

                $path = UriPath::parse('/resources/scripts/js/ion-components-action.js');

                $this->getScriptAssets()->set(
                        'ion-components-action', 
                        ScriptAsset::reference(
                                TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(),
                                15,
                                true,
                                TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')
                                ));  
            }
            
            if(!$this->getScriptAssets()->hasKey('ion-action-click-event')) {        
                
                if(!PHP::isEmpty($onClickScript)) {

                    $value = StringObject::create($onClickScript)->indent(2, true)->toString();
                    
                    $this->getScriptAssets()->set(
                            'ion-action-click-event', 
                            ScriptAsset::inline("if(ion.hasModule('actions')) {\n\tion.actions.setActivationHook(function() {\n{$value}\n\t});\n}", 50));                         
                }
            }            
        }               
    }    
    
    protected function initialize(): ?TemplateInterface {
        
        return $this;
    }    
}
