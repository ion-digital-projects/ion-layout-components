<?php

namespace ion\Rendering\Layout\Components\Lists;

use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Descriptors\Lists\GridDescriptorInterface;

interface GridComponentInterface extends ComponentInterface, GridDescriptorInterface {

    // No public methods!

}
