<?php

namespace ion\Rendering\Layout\Components\Sections\Content;

use \ion\Rendering\Layout\TemplateInterface;
use \ion\Rendering\Layout\ComponentInterface;
use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentPagingDescriptorInterface;
use \ion\Rendering\Layout\Descriptors\DescriptorInterface;
use \ion\Rendering\RenderOptionsInterface;
use \ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use \ion\Rendering\Layout\Components\Sections\Content\ContentDescriptorInterface;

interface ContentPagingComponentInterface extends ComponentInterface, ContentPagingDescriptorInterface {

    static function createFromDescriptor(

        string $clientId,
        DescriptorInterface $descriptor,
        RenderOptionsInterface $renderOptions = null,
        TemplateInterface $parent = null,
        CallableMapBaseInterface $hooks = null

    ): ?ComponentInterface;

    static function createFromContentDescriptor(

        string $clientId,
        ContentDescriptorInterface $contentDescriptor,
        RenderOptionsInterface $renderOptions = null,
        TemplateInterface $parent = null,
        CallableMapBaseInterface $hooks = null

    );

    function getContentDescriptor(): ?ContentDescriptorInterface;

    function hasContentDescriptor(): bool;

}
