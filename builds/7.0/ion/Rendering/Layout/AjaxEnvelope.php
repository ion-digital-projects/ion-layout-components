<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout;

/**
 * Description of AjaxEnvelope
 *
 * @author Justus
 */
use ion\Rendering\Markup\Json\JsonDocument;
use ion\Rendering\Markup\Json\JsonNamedDocumentNodeVector;
use ion\Rendering\Markup\Json\JsonNamedDocumentNode;
use ion\Rendering\Markup\Json\JsonNumericalDocumentNode;
use ion\Rendering\Markup\Json\JsonStringDocumentNode;
use ion\Rendering\Markup\Json\JsonNullDocumentNode;
use ion\Rendering\Markup\Json\JsonObjectDocumentNode;
use ion\Rendering\Renderable;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\StringInterface;
use ion\System\Remote\UriInterface;
use ion\Types\Arrays\Specialized\StringMapInterface;
use ion\Types\Arrays\Specialized\StringMap;
class AjaxEnvelope extends Renderable implements AjaxEnvelopeInterface
{
    private $errorCode = null;
    private $errorMessage = null;
    private $redirect = null;
    private $data = null;
    private $dataContext = null;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(int $errorCode = null, string $errorMessage = null, UriInterface $redirect = null, string $dataContext = null, StringMapInterface $data = null)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->redirect = $redirect;
        $this->dataContext = $dataContext;
        $this->data = $data;
        if ($data === null) {
            $this->data = StringMap::create();
        }
    }
    /**
     * method
     * 
     * @return ?UriInterface
     */
    public function getRedirect()
    {
        return $this->redirect;
    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    /**
     * method
     * 
     * @return ?string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
    /**
     * method
     * 
     * @return ?string
     */
    public function getDataContext()
    {
        return $this->dataContext;
    }
    /**
     * method
     * 
     * @return StringMapInterface
     */
    public function getData() : StringMapInterface
    {
        return $this->data;
    }
    /**
     * method
     * 
     * 
     * @return StringInterface
     */
    protected function onRender(StringInterface $buffer, RenderOptionsInterface $renderOptions) : StringInterface
    {
        $data = new JsonNullDocumentNode();
        if ($this->getData()->count() > 0) {
            $data = new JsonObjectDocumentNode();
            foreach ($this->getData() as $key => $value) {
                $data->getChildren()->add(new JsonNamedDocumentNode($key, $value === null ? new JsonNullDocumentNode() : new JsonStringDocumentNode($value)));
            }
        }
        $json = JsonDocument::createAsObject(JsonNamedDocumentNodeVector::create([new JsonNamedDocumentNode('error-code', $this->getErrorCode() === null ? new JsonNullDocumentNode() : new JsonNumericalDocumentNode($this->getErrorCode())), new JsonNamedDocumentNode('error-message', $this->getErrorMessage() === null ? new JsonNullDocumentNode() : new JsonStringDocumentNode($this->getErrorMessage())), new JsonNamedDocumentNode('redirect', $this->getRedirect() === null ? new JsonNullDocumentNode() : new JsonStringDocumentNode($this->getRedirect())), new JsonNamedDocumentNode('data-context', $this->getDataContext() === null ? new JsonNullDocumentNode() : new JsonStringDocumentNode($this->getDataContext())), new JsonNamedDocumentNode('data', $data)]));
        return $json->render($renderOptions, $buffer);
    }
}