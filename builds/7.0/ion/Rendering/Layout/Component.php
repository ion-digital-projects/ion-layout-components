<?php
/* 
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout;

/**
 * Description of Header
 *
 * @author Justus
 */
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\Rendering\Layout\Assets\ScriptAsset;
use ion\System\Remote\UriPathInterface;
use ion\System\Remote\UriPath;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ComponentException;
use ion\Types\Arrays\Specialized\StringMapInterface;
use ion\Types\Arrays\Specialized\StringMap;
use ion\Types\Type;
use ion\PhpHelper as PHP;
use ion\System\Path;
abstract class Component extends Template implements ComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\DescriptorTrait;
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    public static function create(string $clientId = null, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : TemplateInterface
    {
        return new static($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        return static::create($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    public static function createFromTemplate(TemplateInterface $template, string $clientId = null) : TemplateInterface
    {
        if ($template instanceof ComponentInterface) {
            $instance = static::createFromDescriptor($template->getClientId(), $template, $template->getRenderOptions(), $template->getParent(), $template->getHooks());
            if ($instance->getChildren()->isLocked() && $template->getChildren()->count() > 0) {
                throw new ComponentException("Could not create a new Template instance from an existing Template instance - the new instance's children list has been locked; but the input template has children.");
            }
            foreach ($template->getChildren() as $child) {
                $instance->getChildren()->add($child);
            }
            return $instance;
        }
        return parent::createFromTemplate($template, $clientId);
    }
    //FIXME
    /**
     * method
     * 
     * 
     * @return string
     */
    protected static function generateClientId(DescriptorInterface $descriptor, ComponentInterface $parent = null, int $intValue = null) : string
    {
        //       $traces = debug_backtrace(DEBUG_BACKTRACE_PROVDEInterface_OBJECT, 0);
        //       return md5($trace['file'] . $trace['line']);
        $id = '';
        if ($parent !== null) {
            $id .= $parent->getClientId();
        }
        return md5($id . get_class($descriptor) . ($intValue === null ? '' : (string) $intValue));
    }
    //    protected static $componentCount = [];
    //
    //    protected static function getComponentCount(bool $increment = false): int {
    //
    //        if(!array_key_exists(static::class, static::$componentCount)) {
    //
    //            static::$componentCount[static::class] = 0;
    //        }
    //
    //        if($increment) {
    //
    //            static::$componentCount[static::class]++;
    //        }
    //
    //        return static::$componentCount[static::class];
    //    }
    //    private $id = null;
    //    private $clientId = null;
    //    private $clientIdRequired = false;
    //    private $generatedClientId = null;
    //    private $padding = false;
    //    private $scripts = null;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId = null, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        //        $this->scripts = StringMap::create();
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        //        $this->setClientId($clientId);
        //        if($this->clientIdRequired !== true) {
        //
        //            $this->clientIdRequired = false;
        //        }
        if (PHP::isEmpty($clientId)) {
            $class = static::class;
            throw new ComponentException("No client ID was provided - any class deriving from '{$class}' requires a client ID.");
        }
        $this->addProperty('Padding', Type::BOOL(), true, null);
        if (!$this->getStyleAssets()->isLocked()) {
            if (!$this->getStyleAssets()->hasKey('normalize')) {
                $path = '/resources/styles/css/normalize.css';
                $this->getStyleAssets()->set('normalize', StyleAsset::reference(TemplateManager::getAssetRemotePath(UriPath::parse($path), 'layout-components', 'ion')->toUri(), 5, StringVector::create(['screen']), TemplateManager::getAssetLocalPath(Path::parse($path), 'layout-components', 'ion')));
            }
            if (!$this->getStyleAssets()->hasKey('ion')) {
                $path = '/resources/styles/css/ion.css';
                $this->getStyleAssets()->set('ion', StyleAsset::reference(TemplateManager::getAssetRemotePath(UriPath::parse($path), 'layout-components', 'ion')->toUri(), 10, StringVector::create(['screen']), TemplateManager::getAssetLocalPath(Path::parse($path), 'layout-components', 'ion')));
            }
            if (!$this->getStyleAssets()->hasKey('ion-components')) {
                $path = UriPath::parse('/resources/styles/css/ion-components.css');
                $this->getStyleAssets()->set('ion-components', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
        if (!$this->getScriptAssets()->isLocked()) {
            if (!$this->getScriptAssets()->hasKey('ion')) {
                $path = '/resources/scripts/js/ion.js';
                $this->getScriptAssets()->set('ion', ScriptAsset::reference(TemplateManager::getAssetRemotePath(UriPath::parse($path), 'layout-components', 'ion')->toUri(), 10, true, TemplateManager::getAssetLocalPath(Path::parse($path), 'layout-components', 'ion')));
            }
            if (!$this->getScriptAssets()->hasKey('ion-components')) {
                $path = UriPath::parse('/resources/scripts/js/ion-components.js');
                $this->getScriptAssets()->set('ion-components', ScriptAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, true, TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
            }
        }
    }
}