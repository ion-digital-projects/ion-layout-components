<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\ComponentException;
use ion\Rendering\Layout\ColourInterface;
trait ImageDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Media\MediaDescriptorTrait;
    private $colour = null;
    private $round = false;
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    public function setRound(bool $round = null) : ImageDescriptorInterface
    {
        $this->round = $round;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getRound()
    {
        return $this->round;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isRound() : bool
    {
        return $this->round === true;
    }
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    public function setColour(ColourInterface $colour = null) : ImageDescriptorInterface
    {
        $this->colour = $colour;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    public function getColour()
    {
        return $this->colour;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasColour() : bool
    {
        return $this->colour !== null;
    }
}