<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Media;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\ComponentException;
trait MediaDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\TargetUriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\TextPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\VisiblePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\MediaContextPropertyTrait;
    private $thumbnail = null;
    /**
     * method
     * 
     * 
     * @return MediaDescriptorInterface
     */
    public function setThumbnail(MediaDescriptorInterface $thumbnail = null) : MediaDescriptorInterface
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }
    /**
     * method
     * 
     * @return ?MediaDescriptorInterface
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasThumbnail() : bool
    {
        return $this->getThumbnail() !== null;
    }
}