<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Types\Arrays\Specialized\ObjectVectorBaseInterface;
interface ContentItemDescriptorVectorBaseInterface extends ObjectVectorBaseInterface
{
    /**
     * Return a value from the list at a specified index.
     *
     * @param int $index The index of the value to return.
     * @return mixed The value to be returned.
     *
     */
    function get(int $index);
}