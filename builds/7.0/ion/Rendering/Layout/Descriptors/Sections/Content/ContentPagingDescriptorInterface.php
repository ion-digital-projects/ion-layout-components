<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\System\Remote\UriInterface;
use ion\System\Remote\UriMapBaseInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
interface ContentPagingDescriptorInterface extends ColourPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setPageSize(int $pageSize = null) : ContentPagingDescriptorInterface;
    /**
     * method
     * 
     * @return ?int
     */
    function getPageSize();
    /**
     * method
     * 
     * @return bool
     */
    function hasPageSize() : bool;
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setEndSize(int $endSize = null) : ContentPagingDescriptorInterface;
    /**
     * method
     * 
     * @return ?int
     */
    function getEndSize();
    /**
     * method
     * 
     * @return bool
     */
    function hasEndSize() : bool;
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setNextPreviousEnabled(bool $nextPrevious) : ContentPagingDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isNextPreviousEnabled() : bool;
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getNextLink();
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getPreviousLink();
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setItemCount(int $itemCount) : ContentPagingDescriptorInterface;
    /**
     * method
     * 
     * @return int
     */
    function getItemCount() : int;
    /**
     * method
     * 
     * @return int
     */
    function getPageCount() : int;
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setCurrentPage(int $pageIndex) : ContentPagingDescriptorInterface;
    /**
     * method
     * 
     * @return int
     */
    function getCurrentPage() : int;
    /**
     * method
     * 
     * @return UriMapBaseInterface
     */
    function getPageLinks() : UriMapBaseInterface;
}