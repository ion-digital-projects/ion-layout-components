<?php
/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\PhpHelper as PHP;
use ion\Types\Arrays\MapBaseInterface;
use ion\Types\Arrays\VectorBaseInterface;
use ion\Types\Arrays\MapException;
use ion\Types\StringObjectInterface;
use ion\Types\StringObject;
use ion\Types\Arrays\Specialized\ObjectVectorBase;
use ion\Rendering\Layout\ContentItemDescriptorMapBase;
abstract class ContentItemDescriptorMapBase extends ObjectMapBase implements ContentItemDescriptorMapBaseInterface
{
    /**
     * method
     * 
     * 
     * @return VectorBaseInterface
     */
    protected static function createVector(array $values = null) : VectorBaseInterface
    {
        return ContentItemDescriptorVector::create($values);
    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$values = null, bool $immutable = false)
    {
        parent::__construct($values, $immutable);
    }
    /**
     *
     * Return a value from the map by key.
     *
     * @param ContentItemDescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */
    public function get(string $key)
    {
        return $this->_get($key);
    }
    /**
     * method
     * 
     * 
     * @return ?string
     */
    protected function validateArrayElement($element)
    {
        if (!PHP::isObject($element, '\\ion\\Rendering\\Layout\\Descriptors\\Sections\\Content\\ContentItemDescriptorInterface', true, true) && $element !== null) {
            return "Map element needs to be of type 'ContentItemDescriptorInterface.'";
        }
        return null;
    }
}