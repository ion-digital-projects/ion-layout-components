<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
abstract class SectionDescriptor extends Descriptor implements SectionDescriptorInterface
{
    /**
     * method
     * 
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }
}