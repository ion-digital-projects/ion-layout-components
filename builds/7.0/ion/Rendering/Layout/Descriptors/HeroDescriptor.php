<?php
/*
 * See license information at the package root in LCENSEInterface.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptor
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
class HeroDescriptor extends Descriptor implements HeroDescriptorInterface
{
    use HeroDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(MediaDescriptorInterface $media = null, string $caption = null, string $text = null, UriInterface $uri = null, bool $escapeText = null)
    {
        $this->setMedia($media);
        $this->setCaption($caption);
        $this->setText($text);
        $this->setEscapeText($escapeText);
        $this->setUri($uri);
        parent::__construct();
    }
}