<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorVectorInterface;
interface LayoutDescriptorInterface extends ColourPropertyInterface, ImagePropertyInterface
{
    /**
     * method
     * 
     * @return ModalDescriptorVectorInterface
     */
    function getModals() : ModalDescriptorVectorInterface;
    /**
     * method
     * 
     * @return NavigationSectionDescriptorInterface
     */
    function getNavigation() : NavigationSectionDescriptorInterface;
    /**
     * method
     * 
     * @return HeaderSectionDescriptorInterface
     */
    function getHeader() : HeaderSectionDescriptorInterface;
    /**
     * method
     * 
     * @return MainSectionDescriptorInterface
     */
    function getMain() : MainSectionDescriptorInterface;
    /**
     * method
     * 
     * @return FooterSectionDescriptorInterface
     */
    function getFooter() : FooterSectionDescriptorInterface;
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    function setTitle(string $title = null) : LayoutDescriptorInterface;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getTitle();
    /**
     * method
     * 
     * @return bool
     */
    function hasTitle() : bool;
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    function setFullHeight(bool $fullHeight = null) : LayoutDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function getFullHeight();
    /**
     * method
     * 
     * @return bool
     */
    function hasFullHeight() : bool;
}