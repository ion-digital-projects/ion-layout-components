<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\ActionDescriptorInterface;
interface NavigationDescriptorInterface extends ImagePropertyInterface, ColourPropertyInterface, UriPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setMenu(ListItemContainerInterface $items = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    function getMenu() : ListItemContainerInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasMenu() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAction(ActionDescriptorInterface $action = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ?ActionDescriptorInterface
     */
    function getAction();
    /**
     * method
     * 
     * @return bool
     */
    function hasAction() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSearchEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isSearchEnabled() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isAccountEnabled() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountMenu(ListItemContainerInterface $menu = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    function getAccountMenu() : ListItemContainerInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasAccountMenu() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountUri(UriInterface $uri = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return UriInterface
     */
    function getAccountUri() : UriInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasAccountUri() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSearchParameterName(string $searchParameterName = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ?string
     */
    function getSearchParameterName();
    /**
     * method
     * 
     * @return bool
     */
    function hasSearchParameterName() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setActionEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isActionEnabled() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setInitialColourAlpha(int $initialColourAlpha = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ?int
     */
    function getInitialColourAlpha();
    /**
     * method
     * 
     * @return bool
     */
    function hasInitialColourAlpha() : bool;
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setShadowEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isShadowEnabled() : bool;
    /**
     * method
     * 
     * @return ?bool
     */
    function getShadowEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setShadowEnabledInitially(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isShadowEnabledInitially() : bool;
    /**
     * method
     * 
     * @return ?bool
     */
    function getShadowEnabledInitially();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSmallEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isSmallEnabled() : bool;
    /**
     * method
     * 
     * @return ?bool
     */
    function getSmallEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setMediumEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isMediumEnabled() : bool;
    /**
     * method
     * 
     * @return ?bool
     */
    function getMediumEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setLargeEnabled(bool $enabled = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return bool
     */
    function isLargeEnabled() : bool;
    /**
     * method
     * 
     * @return ?bool
     */
    function getLargeEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setInitialColour(ColourInterface $colour = null) : NavigationDescriptorInterface;
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getInitialColour();
    /**
     * method
     * 
     * @return bool
     */
    function hasInitialColour() : bool;
}