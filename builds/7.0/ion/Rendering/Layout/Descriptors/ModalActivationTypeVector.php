<?php
/*
 * See license information at the package root in LICENSE.md.
 *
 * This file has been auto-generated using a template build tool and
 * will be overwritten on the next build - any changes will be lost!
 *
 */
namespace ion\Rendering\Layout\Descriptors;

use ion\PhpHelper as PHP;
use ion\Types\TypeObjectInterface;
use ion\ImmutableInterface;
use ion\Types\Arrays\Specialized\ObjectVector;
final class ModalActivationTypeVector extends ModalActivationTypeVectorBase implements ModalActivationTypeVectorInterface
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(array &$values = null, bool $immutable = false)
    {
        parent::__construct($values, $immutable);
    }
    /**
     *
     * Add a value to the end of the list.
     *
     * @param ModalActivationType $value The value to be added to the list.
     * @return  The modified vector.
     *
     */
    public function add(ModalActivationType $value) : ModalActivationTypeVectorInterface
    {
        return parent::_add($value);
    }
    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param ModalActivationType $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */
    public function insert(int $index, ModalActivationType $value) : ModalActivationTypeVectorInterface
    {
        return parent::_insert($index, $value);
    }
    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param ModalActivationType $value The value to set.
     * @return  The modified vector.
     *
     */
    public function set(int $index, ModalActivationType $value) : ModalActivationTypeVectorInterface
    {
        return parent::_set($index, $value);
    }
    /**
     *
     * Checks whether a list contains a value.
     *
     * @param ModalActivationType $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */
    public function hasValue(ModalActivationType $value, int $index = null) : bool
    {
        return $this->_hasValue($value, $index);
    }
    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param ModalActivationType $value The value to look for in the list.
     * @return  The modified vector.
     *
     */
    public function removeValue(ModalActivationType $value = null) : ModalActivationTypeVectorInterface
    {
        return $this->_removeValue($value);
    }
    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */
    public function addVector(ModalActivationTypeVectorBaseInterface $values) : ModalActivationTypeVectorInterface
    {
        return parent::_addVector($values);
    }
    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?ModalActivationType $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */
    public function strip(ModalActivationTypeVectorBaseInterface $values = null) : ModalActivationTypeVectorInterface
    {
        return $this->_strip($values);
    }
}