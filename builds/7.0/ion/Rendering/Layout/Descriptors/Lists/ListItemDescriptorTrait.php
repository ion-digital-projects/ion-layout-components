<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
use ion\Rendering\Layout\ComponentException;
trait ListItemDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\TagPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriActionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\IconPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\OrderPropertyTrait;
    use ListItemContainerTrait;
    private $list = null;
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    public function getParentContainer() : ListItemContainerInterface
    {
        if ($this->list === null) {
            throw new ComponentException("Item does not have a parent container.");
        }
        return $this->list;
    }
    /**
     * method
     * 
     * 
     * @return ListItemDescriptorInterface
     */
    protected function setParentContainer(ListItemContainerInterface $list) : ListItemDescriptorInterface
    {
        $this->list = $list;
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ListItemDescriptorInterface
     */
    public function setIndex(int $index = null) : ListItemDescriptorInterface
    {
        $this->index = $index;
        return $this;
    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getIndex()
    {
        return $this->index;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasIndex() : bool
    {
        return $this->index !== null;
    }
}