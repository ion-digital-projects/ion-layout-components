<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
interface ListDescriptorVectorInterface
{
    /**
     *
     * Add a value to the end of the list.
     *
     * @param ListDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */
    function add(ListDescriptorInterface $value) : ListDescriptorVectorInterface;
    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param ListDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */
    function insert(int $index, ListDescriptorInterface $value) : ListDescriptorVectorInterface;
    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param ListDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */
    function set(int $index, ListDescriptorInterface $value) : ListDescriptorVectorInterface;
    /**
     *
     * Checks whether a list contains a value.
     *
     * @param ListDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */
    function hasValue(ListDescriptorInterface $value, int $index = null) : bool;
    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param ListDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */
    function removeValue(ListDescriptorInterface $value = null) : ListDescriptorVectorInterface;
    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */
    function addVector(ListDescriptorVectorBaseInterface $values) : ListDescriptorVectorInterface;
    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?ListDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */
    function strip(ListDescriptorVectorBaseInterface $values = null) : ListDescriptorVectorInterface;
}