<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriActionInterface;
use ion\CopyableInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorVectorBaseInterface;
interface ListItemContainerInterface
{
    /**
     * method
     * 
     * 
     * @return ListItemDescriptorVectorBaseInterface
     */
    function getItems(int $startLevel = 0, int $endLevel = null) : ListItemDescriptorVectorBaseInterface;
    /**
     * method
     * 
     * @return CopyableInterface
     */
    function copy() : CopyableInterface;
    /**
     * method
     * 
     * 
     * @return ListItemContainerInterface
     */
    function addItem(string $caption, UriInterface $target, UriActionInterface $uriAction = null, string $uriActionModal = null, string $icon = null, bool $showIcon = null, bool $showCaption = null, int $order = null, string $tag = null) : ListItemContainerInterface;
}