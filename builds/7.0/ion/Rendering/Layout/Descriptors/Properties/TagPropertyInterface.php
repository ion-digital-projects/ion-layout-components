<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface TagPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getTag();
    /**
     * method
     * 
     * 
     * @return TagPropertyInterface
     */
    function setTag(string $tag = null) : TagPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasTag() : bool;
}