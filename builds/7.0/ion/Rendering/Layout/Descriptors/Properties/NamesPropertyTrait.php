<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
trait NamesPropertyTrait
{
    private $names = null;
    /**
     * method
     * 
     * @return StringVectorInterface
     */
    public function getNames() : StringVectorInterface
    {
        if ($this->names === null) {
            $this->names = StringVector::create();
        }
        return $this->names;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasNames() : bool
    {
        if ($this->names === null) {
            return false;
        }
        return $this->names->count() > 0;
    }
    /**
     * method
     * 
     * 
     * @return ?StringInterface
     */
    public function getFullName(string $seperator = null)
    {
        if (!$this->hasNames()) {
            return null;
        }
        return StringObject::join($this->getNames(), $seperator === null ? ' ' : $seperator)->copy();
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasFirstName() : bool
    {
        if (!$this->hasNames()) {
            return null;
        }
        return $this->names->count() > 0;
    }
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getFirstName()
    {
        if (!$this->hasNames()) {
            return null;
        }
        return StringObject::create($this->getNames()->get(0))->copy();
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasLastName() : bool
    {
        if (!$this->hasNames()) {
            return null;
        }
        return $this->names->count() > 1;
    }
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getLastName()
    {
        if (!$this->hasNames() || $this->hasNames() && $this->getNames()->count() === 1) {
            return null;
        }
        return StringObject::create($this->getNames()->get($this->getNames()->count() - 1))->copy();
    }
}