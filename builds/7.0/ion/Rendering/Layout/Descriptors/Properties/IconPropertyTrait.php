<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait IconPropertyTrait
{
    private $icon = null;
    private $iconVisible = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getIcon()
    {
        if ($this->icon === null) {
            return null;
        }
        return $this->icon;
    }
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    public function setIcon(string $icon = null) : IconPropertyInterface
    {
        if ($icon === null) {
            $this->icon = null;
            return $this;
        }
        $this->icon = StringObject::create($icon);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasIcon() : bool
    {
        return $this->icon !== null;
    }
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    public function setIconVisible(bool $default = null, bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null) : IconPropertyInterface
    {
        $this->iconVisible = ['default' => $default, 'large' => $onLargeScreens, 'medium' => $onMediumScreens, 'small' => $onSmallScreens];
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    public function isIconVisible(bool $onLargeScreens = null, bool $onMediumScreens = null, bool $onSmallScreens = null)
    {
        if ($this->iconVisible === null) {
            return null;
        }
        $result = null;
        if ($onLargeScreens === true) {
            if ($this->iconVisible['large'] === false) {
                return false;
            }
            $result = true;
        }
        if ($onMediumScreens === true) {
            if ($this->iconVisible['medium'] === false) {
                return false;
            }
            $result = true;
        }
        if ($onSmallScreens === true) {
            if ($this->iconVisible['small'] === false) {
                return false;
            }
            $result = true;
        }
        if ($result !== null) {
            return $result;
        }
        return $this->iconVisible['default'];
    }
}