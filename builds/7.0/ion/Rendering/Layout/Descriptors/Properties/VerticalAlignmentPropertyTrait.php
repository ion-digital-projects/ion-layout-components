<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
use ion\Rendering\Layout\VerticalAlignmentType;
trait VerticalAlignmentPropertyTrait
{
    private $verticalAlignment = null;
    /**
     * method
     * 
     * 
     * @return VerticalAlignmentPropertyInterface
     */
    public function setVerticalAlignment(VerticalAlignmentTypeInterface $verticalAlignment = null) : VerticalAlignmentPropertyInterface
    {
        if ($verticalAlignment === null) {
            $this->verticalAlignment = VerticalAlignmentType::NONE();
            return $this;
        }
        $this->verticalAlignment = $verticalAlignment;
        return $this;
    }
    /**
     * method
     * 
     * @return VerticalAlignmentTypeInterface
     */
    public function getVerticalAlignment() : VerticalAlignmentTypeInterface
    {
        if ($this->verticalAlignment === null) {
            $this->verticalAlignment = VerticalAlignmentType::NONE();
        }
        return $this->verticalAlignment;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasVerticalAlignment() : bool
    {
        return !$this->getVerticalAlignment()->equals(VerticalAlignmentType::NONE());
    }
}