<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\MediaContextInterface;
interface MediaContextPropertyInterface
{
    /**
     * method
     * 
     * @return ?MediaContextInterface
     */
    function getMediaContext();
    /**
     * method
     * 
     * 
     * @return MediaContextPropertyInterface
     */
    function setMediaContext(MediaContextInterface $mediaContext = null) : MediaContextPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasMediaContext() : bool;
}