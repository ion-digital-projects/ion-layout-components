<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait NamePropertyTrait
{
    private $name = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * method
     * 
     * 
     * @return NamePropertyInterface
     */
    public function setName(string $name = null) : NamePropertyInterface
    {
        if ($name === null) {
            $this->name = null;
            return $this;
        }
        $this->name = StringObject::create($name);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasName() : bool
    {
        return $this->name !== null;
    }
}