<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface DisplayNamePropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getDisplayName();
    /**
     * method
     * 
     * 
     * @return DisplayNamePropertyInterface
     */
    function setDisplayName(string $displayName = null) : DisplayNamePropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasDisplayName() : bool;
}