<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\AlignmentTypeInterface;
use ion\Rendering\Layout\AlignmentType;
use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
trait AlignmentPropertyTrait
{
    private $alignment = null;
    /**
     * method
     * 
     * 
     * @return AlignmentPropertyInterface
     */
    public function setAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null, VerticalAlignmentTypeInterface $verticalAlignment = null) : AlignmentPropertyInterface
    {
        $this->setHorizontalAlignment($horizontalAlignment);
        $this->setVerticalAlignment($verticalAlignment);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAlignment() : bool
    {
        return !$this->getHorizontalAlignment()->equals(AlignmentType::NONE()) || !$this->getVerticalAlignment()->equals(AlignmentType::NONE());
    }
}