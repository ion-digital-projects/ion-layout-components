<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
interface HorizontalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return HorizontalAlignmentPropertyInterface
     */
    function setHorizontalAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null) : HorizontalAlignmentPropertyInterface;
    /**
     * method
     * 
     * @return HorizontalAlignmentTypeInterface
     */
    function getHorizontalAlignment() : HorizontalAlignmentTypeInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasHorizontalAlignment() : bool;
}