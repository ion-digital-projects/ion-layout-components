<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\System\Remote\UriInterface;
interface UriPropertyInterface
{
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getUri();
    /**
     * method
     * 
     * 
     * @return UriPropertyInterface
     */
    function setUri(UriInterface $uri = null) : UriPropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasUri() : bool;
}