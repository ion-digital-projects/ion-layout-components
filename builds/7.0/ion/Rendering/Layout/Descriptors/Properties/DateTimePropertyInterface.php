<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\DateTimeInterface;
interface DateTimePropertyInterface
{
    /**
     * method
     * 
     * @return ?DateTimeInterface
     */
    function getDateTime();
    /**
     * method
     * 
     * 
     * @return DateTimePropertyInterface
     */
    function setDateTime(DateTimeInterface $dateTime = null) : DateTimePropertyInterface;
    /**
     * method
     * 
     * @return bool
     */
    function hasDateTime() : bool;
}