<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
interface ColorPropertyInterface extends ColourPropertyInterface
{
}