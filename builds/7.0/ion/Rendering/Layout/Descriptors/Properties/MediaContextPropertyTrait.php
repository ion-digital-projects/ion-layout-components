<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
use ion\Rendering\Layout\MediaContextInterface;
trait MediaContextPropertyTrait
{
    private $mediaContext = null;
    /**
     * method
     * 
     * @return ?MediaContextInterface
     */
    public function getMediaContext()
    {
        return $this->mediaContext;
    }
    /**
     * method
     * 
     * 
     * @return MediaContextPropertyInterface
     */
    public function setMediaContext(MediaContextInterface $mediaContext = null) : MediaContextPropertyInterface
    {
        $this->mediaContext = $mediaContext;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasMediaContext() : bool
    {
        return $this->mediaContext !== null;
    }
}