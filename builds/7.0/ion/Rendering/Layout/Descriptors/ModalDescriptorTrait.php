<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Types\Arrays\Specialized\CallableMap;
use ion\Types\Arrays\Specialized\StringMapBaseInterface;
use ion\Types\Arrays\Specialized\StringMap;
use ion\Rendering\Layout\ColourInterface;
use ion\Types\StringObject;
use ion\Types\StringInterface;
trait ModalDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    //    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
    //
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setButtonColour;
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getButtonColour;
    //        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasButtonColour;
    //    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasBackgroundColour;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::setHorizontalAlignment as setHorizontalContentAlignment;
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::getHorizontalAlignment as getHorizontalContentAlignment;
        \ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyTrait::hasHorizontalAlignment as hasHorizontalContentAlignment;
    }
    private $modalType = null;
    private $modalActivationType = null;
    private $modalDelay = null;
    private $modalAllowMultipleActivations = false;
    private $modalContentPadding = null;
    private $modalBackgroundColour = null;
    private $modalButtons = null;
    private $buttonColour = null;
    private $cookie = null;
    private $activationFrequency = null;
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setActivationFrequency(int $frequency = null) : ModalDescriptorInterface
    {
        $this->activationFrequency = $frequency;
        return $this;
    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getActivationFrequency()
    {
        return $this->activationFrequency;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasActivationFrequency() : bool
    {
        return $this->getActivationFrequency() !== null;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setCookie(string $cookie = null) : ModalDescriptorInterface
    {
        if ($cookie === null) {
            $this->cookie = null;
            return $this;
        }
        $obj = StringObject::create($cookie);
        if ($obj->isEmptyOrWhiteSpace()) {
            $this->cookie = null;
            return $this;
        }
        $this->cookie = $obj;
        return $this;
    }
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getCookie()
    {
        return $this->cookie;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasCookie() : bool
    {
        return $this->getCookie() !== null && !$this->getCookie()->isEmptyOrWhiteSpace();
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setButtonColour(ColourInterface $colour = null) : ModalDescriptorInterface
    {
        $this->buttonColour = $colour;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    public function getButtonColour()
    {
        return $this->buttonColour;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasButtonColour() : bool
    {
        return $this->buttonColour !== null;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    protected function setButtons(StringMapBaseInterface $buttons = null) : ModalDescriptorInterface
    {
        $this->modalButtons = $buttons === null ? StringMap::create() : $buttons;
        return $this;
    }
    /**
     * method
     * 
     * @return StringMapBaseInterface
     */
    public function getButtons() : StringMapBaseInterface
    {
        if ($this->modalButtons === null) {
            $this->modalButtons = StringMap::create();
        }
        return $this->modalButtons;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasButtons() : bool
    {
        return $this->getButtons()->count() > 0;
    }
    /**
     * method
     * 
     * @return ?ModalType
     */
    public function getModalType()
    {
        if ($this->modalType === null) {
            $this->setModalType(ModalType::DALOGInterface());
        }
        return $this->modalType;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setModalType(ModalType $modalType) : ModalDescriptorInterface
    {
        $this->modalType = $modalType;
        return $this;
    }
    /**
     * method
     * 
     * @return ModalActivationType
     */
    public function getModalActivationType() : ModalActivationType
    {
        if ($this->modalActivationType === null) {
            $this->setModalActivationType(ModalActivationType::NONE());
        }
        return $this->modalActivationType;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setModalActivationType(ModalActivationType $modalActivationType) : ModalDescriptorInterface
    {
        $this->modalActivationType = $modalActivationType;
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setModalDelay(int $delay) : ModalDescriptorInterface
    {
        $this->modalDelay = $delay;
        return $this;
    }
    /**
     * method
     * 
     * @return int
     */
    public function getModalDelay() : int
    {
        if ($this->modalDelay === null) {
            $this->setModalDelay(0);
        }
        return $this->modalDelay;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setModalAllowMultipleActivations(bool $allowMultipleActivations) : ModalDescriptorInterface
    {
        $this->modalAllowMultipleActivations = $allowMultipleActivations;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function getModalAllowMultipleActivations() : bool
    {
        return $this->modalAllowMultipleActivations;
    }
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    public function setModalContentPadding(bool $modalContentPadding = null) : ModalDescriptorInterface
    {
        $this->modalContentPadding = $modalContentPadding;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getModalContentPadding()
    {
        return $this->modalContentPadding;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasModalContentPadding() : bool
    {
        return $this->getModalContentPadding() === true;
    }
}