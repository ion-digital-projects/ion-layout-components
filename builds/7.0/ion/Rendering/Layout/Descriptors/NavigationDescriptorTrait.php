<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\DescriptorException;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
trait NavigationDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait {
        getImage as getLogo;
        setImage as setLogo;
        hasImage as hasLogo;
        setImageVisible as setLogoVisible;
        isImageVisible as isLogoVisible;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait {
        getUri as getSearchUri;
        setUri as setSearchUri;
        hasUri as hasSearchUri;
    }
    private $menu = null;
    private $action = null;
    private $searchEnabled = null;
    private $actionEnabled = null;
    private $accountEnabled = null;
    private $accountMenu = null;
    private $accountUri = null;
    private $searchParameterName = null;
    private $dropDownStyle = null;
    private $initialColourAlpha = null;
    private $shadowEnabled = null;
    private $shadowEnabledInitially = null;
    private $smallEnabled = null;
    private $mediumEnabled = null;
    private $largeEnabled = null;
    private $initialColour = null;
    /**
     * method
     * 
     * 
     * @return ListItemContainerInterface
     */
    private static function setItemDefaults(ListItemContainerInterface $container) : ListItemContainerInterface
    {
        foreach ($container->getItems() as $item) {
            $item->setCaptionVisible(null, $item->isCaptionVisible(), true, true);
            $item->setIconVisible(null, $item->isIconVisible(), false, false);
            static::setItemDefaults($item);
        }
        return $container;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setMenu(ListItemContainerInterface $items = null) : NavigationDescriptorInterface
    {
        $this->menu = $items === null ? null : static::setItemDefaults($items);
        return $this;
    }
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    public function getMenu() : ListItemContainerInterface
    {
        if ($this->menu === null) {
            throw new DescriptorException("No menu has been specified.");
        }
        return $this->menu;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasMenu() : bool
    {
        return $this->menu !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAction(ActionDescriptorInterface $action = null) : NavigationDescriptorInterface
    {
        $this->action = $action;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ActionDescriptorInterface
     */
    public function getAction()
    {
        return $this->action;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAction() : bool
    {
        return $this->action !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSearchEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->searchEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isSearchEnabled() : bool
    {
        return $this->searchEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->accountEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isAccountEnabled() : bool
    {
        return $this->accountEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountMenu(ListItemContainerInterface $menu = null) : NavigationDescriptorInterface
    {
        $this->accountMenu = $menu;
        return $this;
    }
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    public function getAccountMenu() : ListItemContainerInterface
    {
        return $this->accountMenu;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAccountMenu() : bool
    {
        return $this->accountEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountUri(UriInterface $uri = null) : NavigationDescriptorInterface
    {
        $this->accountUri = $uri;
        return $this;
    }
    /**
     * method
     * 
     * @return UriInterface
     */
    public function getAccountUri() : UriInterface
    {
        return $this->accountUri;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAccountUri() : bool
    {
        return $this->accountUri === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSearchParameterName(string $searchParameterName = null) : NavigationDescriptorInterface
    {
        $this->searchParameterName = $searchParameterName;
        return $this;
    }
    /**
     * method
     * 
     * @return ?string
     */
    public function getSearchParameterName()
    {
        return $this->searchParameterName;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasSearchParameterName() : bool
    {
        return $this->getSearchParameterName() !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setActionEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->actionEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isActionEnabled() : bool
    {
        return $this->actionEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setInitialColourAlpha(int $initialColourAlpha = null) : NavigationDescriptorInterface
    {
        $this->initialColourAlpha = $initialColourAlpha;
        return $this;
    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getInitialColourAlpha()
    {
        return $this->initialColourAlpha;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasInitialColourAlpha() : bool
    {
        return $this->initialColourAlpha !== null;
    }
    //            bool $shadow = null,
    //            bool $initialShadow = null
    //            bool $disableLarge = null,
    //            bool $disableMedium = null
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setShadowEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->shadowEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isShadowEnabled() : bool
    {
        return $this->shadowEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getShadowEnabled()
    {
        return $this->shadowEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setShadowEnabledInitially(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->shadowEnabledInitially = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isShadowEnabledInitially() : bool
    {
        return $this->shadowEnabledInitially === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getShadowEnabledInitially()
    {
        return $this->shadowEnabledInitially;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSmallEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->smallEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isSmallEnabled() : bool
    {
        return $this->smallEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getSmallEnabled()
    {
        return $this->smallEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setMediumEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->mediumEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isMediumEnabled() : bool
    {
        return $this->mediumEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getMediumEnabled()
    {
        return $this->mediumEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setLargeEnabled(bool $enabled = null) : NavigationDescriptorInterface
    {
        $this->largeEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isLargeEnabled() : bool
    {
        return $this->largeEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getLargeEnabled()
    {
        return $this->largeEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setInitialColour(ColourInterface $colour = null) : NavigationDescriptorInterface
    {
        $this->initialColour = $colour;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    public function getInitialColour()
    {
        return $this->initialColour;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasInitialColour() : bool
    {
        return $this->initialColour !== null;
    }
}