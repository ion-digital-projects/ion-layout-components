<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptor
 *
 * @author Justus
 */
class ModalDescriptor extends Descriptor implements ModalDescriptorInterface
{
    use ModalDescriptorTrait;
}