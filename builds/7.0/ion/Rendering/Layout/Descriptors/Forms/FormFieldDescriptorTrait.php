<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormFieldDescriptorInterface
 *
 * @author Justus.Meyer
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\DescriptorException;
trait FormFieldDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\NamePropertyTrait;
    private $group = null;
    private $value = null;
    //    public function __construct(FormGroupDescriptorInterface $group, string $caption, string $name = null) {
    //
    //        $this->setGroup($group);
    //        $this->setCaption($caption);
    //        $this->setName(($name !== null ? $name : StringObject::create($name)->toDashed()->toString()));
    //    }
    /**
     * method
     * 
     * @return FormGroupDescriptorInterface
     */
    public function getGroup() : FormGroupDescriptorInterface
    {
        if ($this->group === null) {
            throw new DescriptorException("Form field is not attached to a parent form group.");
        }
        return $this->group;
    }
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    protected function setGroup(FormGroupDescriptorInterface $group = null) : FormFieldDescriptorInterface
    {
        $this->group = $group;
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    public function setValue(TypeObjectInterface $value = null) : FormFieldDescriptorInterface
    {
        $this->value = $value;
        return $this;
    }
    /**
     * method
     * 
     * @return ?TypeObjectInterface
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    public function setRequired(bool $required = null) : FormFieldDescriptorInterface
    {
        $this->required = $required;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function isRequired()
    {
        return $this->required;
    }
}