<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
interface FormFieldDescriptorInterface extends CaptionPropertyInterface, NamePropertyInterface
{
    /**
     * method
     * 
     * @return FormGroupDescriptorInterface
     */
    function getGroup() : FormGroupDescriptorInterface;
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    function setValue(TypeObjectInterface $value = null) : FormFieldDescriptorInterface;
    /**
     * method
     * 
     * @return ?TypeObjectInterface
     */
    function getValue();
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    function setRequired(bool $required = null) : FormFieldDescriptorInterface;
    /**
     * method
     * 
     * @return ?bool
     */
    function isRequired();
}