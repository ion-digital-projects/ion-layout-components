<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
class FormGroupDescriptor extends Descriptor implements FormGroupDescriptorInterface
{
    use FormGroupDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FormPageDescriptorInterface $page)
    {
        parent::__construct();
        $this->setPage($page);
    }
}