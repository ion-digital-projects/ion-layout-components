<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;
interface FormPageDescriptorInterface extends CaptionPropertyInterface
{
    /**
     * method
     * 
     * @return FormDescriptorInterface
     */
    function getForm() : FormDescriptorInterface;
    /**
     * method
     * 
     * @return FormGroupDescriptorVectorBaseInterface
     */
    function getGroups() : FormGroupDescriptorVectorBaseInterface;
}