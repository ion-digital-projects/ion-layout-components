<?php
namespace ion\Rendering\Layout\Components\People;

use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\People\PersonDescriptorInterface;
interface PersonComponentInterface extends ComponentInterface, PersonDescriptorInterface
{
}