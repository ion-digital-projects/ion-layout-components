<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Sections\Content;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapInterface;
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Components\Media\ImageComponent;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Components\Lists\ListItemComponent;
class ContentItemComponent extends ListItemComponent implements ContentItemComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorTrait {
        \ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorTrait::setImage as private ContentItemDescriptorTrait_setImage;
    }
    private $hideCaption;
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        if (!$descriptor instanceof ContentItemDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setCaption($descriptor->getCaption() !== null ? $descriptor->getCaption()->toString() : null);
        $component->setUri($descriptor->getUri());
        $component->setText($descriptor->getText() !== null ? $descriptor->getText()->toString() : null);
        $component->setImage($descriptor->getImage());
        $component->setTag($descriptor->getTag());
        $component->setAuthor($descriptor->getAuthor());
        $component->setDateTime($descriptor->getDateTime());
        //        $component->setIcon($descriptor->getIcon());
        $component->setOrder($descriptor->getOrder());
        $component->setPrioritized($descriptor->getPrioritized());
        $component->setSingular($descriptor->getSingular());
        $component->setIndex($descriptor->getIndex());
        return $component;
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ContentComponentInterface
     */
    public function setHideCaption(bool $hideCaptions) : ContentComponentInterface
    {
        $this->hideCaption = $hideCaption;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function getHideCaption() : bool
    {
        return $this->hideCaption;
    }
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    public function setImage(ImageDescriptorInterface $image = null) : ImagePropertyInterface
    {
        $instance = null;
        if ($image !== null && !$image instanceof ComponentInterface) {
            $instance = ImageComponent::createFromDescriptor(static::generateClientId($image, $this, $this->getIndex()), $image);
        }
        if ($instance === null) {
            $childIndex = null;
            foreach ($this->getChildren() as $index => $child) {
                if ($child === $this->getImage()) {
                    $childIndex = $index;
                    break;
                }
            }
            if ($childIndex !== null) {
                $this->getChildren()->remove($childIndex);
            }
        } else {
            $this->getChildren()->add($instance);
        }
        $this->ContentItemDescriptorTrait_setImage($instance);
        return $this;
    }
}