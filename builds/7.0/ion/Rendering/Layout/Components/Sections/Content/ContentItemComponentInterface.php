<?php
namespace ion\Rendering\Layout\Components\Sections\Content;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Components\Lists\ListItemComponentInterface;
use ion\Rendering\Layout\Components\Sections\Content\ContentComponentInterface;
interface ContentItemComponentInterface extends ListItemComponentInterface, ContentItemDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null);
    /**
     * method
     * 
     * 
     * @return ContentComponentInterface
     */
    function setHideCaption(bool $hideCaptions) : ContentComponentInterface;
    /**
     * method
     * 
     * @return bool
     */
    function getHideCaption() : bool;
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    function setImage(ImageDescriptorInterface $image = null) : ImagePropertyInterface;
}