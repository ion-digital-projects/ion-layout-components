<?php
namespace ion\Rendering\Layout\Components\Sections;

use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
interface HeaderSectionComponentInterface extends ComponentInterface, HeaderSectionDescriptorInterface
{
}