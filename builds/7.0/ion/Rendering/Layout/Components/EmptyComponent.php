<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components;

/**
 * Description of EmptyTemplate
 *
 * @author Justus
 */
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVectorInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\RenderOptionsInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateVectorBaseInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Component;
class EmptyComponent extends Component implements EmptyComponentInterface
{
    /**
     * method
     * 
     * 
     * @return EmptyComponentInterface
     */
    public static function createFromRenderables(string $clientId, RenderableVectorBaseInterface $renderables, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null) : EmptyComponentInterface
    {
        return new static($clientId, $renderOptions, $parent, $hooks, $renderables);
    }
    private $renderables;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null, RenderableVectorInterface $renderables = null)
    {
        parent::__construct($clientId, $parent, $renderOptions, $hooks, $renderables);
        $this->renderables = $renderables === null ? RenderableVector::create() : $renderables;
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return RenderableVectorBaseInterface
     */
    protected function createRenderables(RenderableVectorBaseInterface $children) : RenderableVectorBaseInterface
    {
        $result = RenderableVector::create();
        $result->addVector($this->renderables);
        $result->addVector($children);
        return $result;
    }
}