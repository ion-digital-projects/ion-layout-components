<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\TemplateInterface;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\Rendering\Layout\Descriptors\NavigationDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Components\ActionComponentInterface;
use ion\Rendering\Layout\Components\RenderOptionsInterface;
interface NavigationComponentInterface extends ComponentInterface, NavigationDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return mixed
     */
    static function createWithMenu(string $clientId, ListItemContainerInterface $items, ImageDescriptorInterface $logo = null, ActionComponentInterface $action = null, ColourInterface $colour = null, ColourInterface $initialColour = null, bool $actionEnabled = null, bool $searchEnabled = null, string $searchParameterName = null, UriInterface $searchUri = null, bool $shadowEnabled = null, bool $initialShadowEnabled = null, bool $enableSmall = null, bool $enableMedium = null, bool $enableLarge = null, TemplateInterface $parent = null, RenderOptionsInterface $renderOptions = null, CallableMapBaseInterface $hooks = null);
}