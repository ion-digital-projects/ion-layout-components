<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Lists;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\CopyableInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Markup\Html\HtmlHelper as HTML;
use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
class GridComponent extends Component implements GridComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Lists\GridDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(string $clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        TemplateManager::getInstance()->registerFeature('grids');
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        //        TemplateManager::getInstance()->registerFeature('lists');
        return $this;
    }
}