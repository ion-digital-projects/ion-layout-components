<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Lists;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
class ListItemComponent extends Component implements ListItemComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorTrait {
        \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorTrait::setParentContainer as private ListItemDescriptorTrait_setParentContainer;
        \ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorTrait::getParentContainer as private ListItemDescriptorTrait_getParentContainer;
    }
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        if (!$descriptor instanceof ContentItemDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setParentContainer($parent);
        $component->setCaption($descriptor->getCaption() !== null ? $descriptor->getCaption()->toString() : null);
        $component->setUri($descriptor->getUri());
        $component->setText($descriptor->getText() !== null ? $descriptor->getText()->toString() : null);
        $component->setIcon($descriptor->getIcon());
        $component->setIconVisible($descriptor->isIconVisible(), $descriptor->isIconVisible(null, true), $descriptor->isIconVisible(null, null, true), $descriptor->isIconVisible(null, null, null, true));
        $component->setCaptionVisible($descriptor->isCaptionVisible(), $descriptor->isCaptionVisible(null, true), $descriptor->isCaptionVisible(null, null, true), $descriptor->isCaptionVisible(null, null, null, true));
        $component->setTag($descriptor->getTag());
        return $component;
    }
    //    public function getParent(): ?TemplateInterface {
    //
    //        return parent::getParent();
    //
    ////        if($this->ListItemDescriptorTrait_getParentContainer() === null) {
    ////
    ////            return parent::getParent();
    ////        }
    ////
    ////        return $this->ListItemDescriptorTrait_getParentContainer();
    //    }
    //    public function setParent(TemplateInterface $parent = null): TemplateInterface {
    //
    //        if($parent !== null) {
    //
    //            $this->ListItemDescriptorTrait_setParentContainer($parent);
    //        }
    //
    //        return parent::setParent($parent);
    //    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return null;
    }
}