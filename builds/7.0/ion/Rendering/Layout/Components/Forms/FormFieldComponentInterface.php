<?php
namespace ion\Rendering\Layout\Components\Forms;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
interface FormFieldComponentInterface extends ComponentInterface, FormFieldDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null);
}