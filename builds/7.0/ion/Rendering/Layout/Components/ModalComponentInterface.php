<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
interface ModalComponentInterface extends ComponentInterface, ModalDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor(string $clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null);
}