<?php
namespace ion\Rendering\Layout;

use ion\System\Remote\UriInterface;
use ion\Types\Arrays\Specialized\StringMapInterface;
use ion\Rendering\RenderableInterface;
interface AjaxEnvelopeInterface extends RenderableInterface
{
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getRedirect();
    /**
     * method
     * 
     * @return ?int
     */
    function getErrorCode();
    /**
     * method
     * 
     * @return ?string
     */
    function getErrorMessage();
    /**
     * method
     * 
     * @return ?string
     */
    function getDataContext();
    /**
     * method
     * 
     * @return StringMapInterface
     */
    function getData() : StringMapInterface;
}