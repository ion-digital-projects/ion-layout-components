<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components;

/**
 * Description of ContainerTemplate
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\RenderableVectorInterface;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Assets\ScriptAssetInterface;
use ion\Rendering\Layout\Assets\StyleAssetInterface;
use ion\Rendering\Layout\TemplateException;
use ion\Rendering\Layout\TemplateVectorBaseInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\Components\Sections\HeaderSectionComponentInterface;
use ion\Rendering\Layout\Components\Sections\ContentSectionComponentInterface;
use ion\Rendering\Layout\Components\Sections\FooterSectionComponentInterface;
use ion\Rendering\Layout\Components\Sections\NavigationSectionComponent;
use ion\Rendering\Layout\Components\Sections\HeaderSectionComponent;
use ion\Rendering\Layout\Components\Sections\ContentSectionComponent;
use ion\Rendering\Layout\Components\Sections\FooterSectionComponent;
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\ComponentException;
class LayoutComponent extends Component implements LayoutComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\LayoutDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        if (!TemplateManager::getInstance()->hasRegisteredRootTemplate()) {
            TemplateManager::getInstance()->registerRootTemplate($this);
        }
        $this->observe($this->getModals());
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * @return ?StringVectorInterface
     */
    protected function getValidHooks()
    {
        return LayoutHooks::getNames();
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        $this->setTitle(null);
        $navigationClientId = static::generateClientId($this);
        $headerClientId = static::generateClientId($this);
        $mainClientId = static::generateClientId($this);
        $footerClientId = static::generateClientId($this);
        //        $this->setModal(ModalComponent::create($this));
        $this->setNavigation(NavigationSectionComponent::create($navigationClientId, null, $this));
        $this->setHeader(HeaderSectionComponent::create($headerClientId, null, $this));
        $this->setMain(ContentSectionComponent::create($mainClientId, null, $this));
        $this->setFooter(FooterSectionComponent::create($footerClientId, null, $this));
        $this->getChildren()->add($this->getNavigation());
        $this->getChildren()->add($this->getHeader());
        $this->getChildren()->add($this->getMain());
        $this->getChildren()->add($this->getFooter());
        //        $this->getChildren()->add($this->getModal());
        parent::initialize();
        return $this->getMain();
    }
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    public function setParent(TemplateInterface $parent = null)
    {
        if ($parent !== null) {
            throw new ComponentException("A layout component cannot have a parent.");
        }
        return $this;
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    public function getParent()
    {
        return null;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getModals()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $clientId = static::generateClientId($instance, $this, $this->getChildren()->count());
                $instance = ModalComponent::createFromDescriptor($clientId, $instance);
            }
            $this->getChildren()->add($instance->setRenderSeperately(true));
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getModals()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $clientId = static::generateClientId($descriptor, $this, $this->getChildren()->count());
                $instance = ModalComponent::createFromDescriptor($clientId, $instance);
            }
            $this->getChildren()->insert($data->get('index'), $instance->setRenderSeperately(true));
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getModals()) {
            $this->getChildren()->remove($data->get('index'));
        }
        return $this;
    }
}