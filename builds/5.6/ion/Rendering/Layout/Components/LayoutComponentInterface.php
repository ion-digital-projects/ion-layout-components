<?php
namespace ion\Rendering\Layout\Components;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\Descriptors\LayoutDescriptorInterface;
interface LayoutComponentInterface extends ComponentInterface, LayoutDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    function setParent(TemplateInterface $parent = null);
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    function getParent();
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null);
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null);
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null);
}