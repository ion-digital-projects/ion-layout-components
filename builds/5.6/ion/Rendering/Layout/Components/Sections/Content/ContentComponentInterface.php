<?php
namespace ion\Rendering\Layout\Components\Sections\Content;

use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\Components\Lists\GridComponentInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentDescriptorInterface;
interface ContentComponentInterface extends GridComponentInterface, ContentDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ContentComponentInterface
     */
    function setHideCaptions($hideCaptions);
    /**
     * method
     * 
     * @return bool
     */
    function getHideCaptions();
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onAddObserved(ObservableInterface $observable, MapInterface $data = null);
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onInsertObserved(ObservableInterface $observable, MapInterface $data = null);
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null);
}