<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Sections\Content;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\RenderableVector;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\TemplateManager;
use ion\Rendering\Markup\Html\HtmlHelper as HTML;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Sections\Content\ContentItemDescriptorInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Components\Lists\ListComponent;
use ion\Rendering\Layout\Components\Lists\GridComponent;
use ion\Rendering\Layout\Components\Lists\ListItemComponentInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorInterface;
class ContentComponent extends GridComponent implements ContentComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Sections\Content\ContentDescriptorTrait;
    private $hideCaptions = false;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null, $hideCaptions = false)
    {
        TemplateManager::getInstance()->registerFeature('content');
        $this->observe($this->getItems());
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        $this->setHideCaptions($hideCaptions);
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ContentComponentInterface
     */
    public function setHideCaptions($hideCaptions)
    {
        $this->hideCaptions = $hideCaptions;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function getHideCaptions()
    {
        return $this->hideCaptions;
    }
    /**
     * method
     * 
     * 
     * @return ListItemComponentInterface
     */
    protected function createItemComponentInstance(ListItemDescriptorInterface $descriptor)
    {
        $clientId = static::generateClientId($descriptor, $this, $this->getItems()->count());
        return ContentItemComponent::createFromDescriptor($clientId, $descriptor);
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getItems()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $instance->setParent($this);
            $this->getChildren()->add($instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getItems()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $instance->setParent($this);
            $this->getChildren()->insert($data->get('index'), $instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getItems()) {
            $this->getChildren()->remove($data->get('index'));
        }
        return $this;
    }
}