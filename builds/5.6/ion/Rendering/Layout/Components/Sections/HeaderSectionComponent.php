<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Sections;

/**
 * Description of HeaderTemplate
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Component;
class HeaderSectionComponent extends Component implements HeaderSectionComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorTrait;
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        parent::initialize();
        return $this;
    }
}