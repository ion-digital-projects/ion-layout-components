<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Sections;

/**
 * Description of HeaderTemplate
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Template;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Component;
use ion\System\Remote\UriPath;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\Rendering\Layout\Assets\ScriptAsset;
use ion\Rendering\Layout\TemplateManager;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
class NavigationSectionComponent extends Component implements NavigationSectionComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        if (!$this->getStyleAssets()->hasKey('ion-components-nav')) {
            $path = UriPath::parse('/resources/styles/css/ion-components-nav.css');
            $this->getStyleAssets()->set('ion-components-nav', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
        }
        if (!$this->getScriptAssets()->hasKey('ion-components-nav')) {
            $path = UriPath::parse('/resources/scripts/js/ion-components-nav.js');
            $this->getScriptAssets()->set('ion-components-nav', ScriptAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, true, TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
        }
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        $attachmentPoint = parent::initialize();
        return $attachmentPoint;
    }
}