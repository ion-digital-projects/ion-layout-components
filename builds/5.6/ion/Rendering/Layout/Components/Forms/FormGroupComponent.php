<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Forms;

/**
 * Description of LogoTemplateBase
 *
 * @author Justus
 */
use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\ComponentException;
use ion\Rendering\RenderableVectorBaseInterface;
use ion\Rendering\Layout\TemplateVectorInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Forms\Fields\TextFormFieldDescriptor;
use ion\Rendering\Layout\Descriptors\Forms\Fields\EmailFormFieldDescriptor;
use ion\Rendering\Layout\Components\Forms\Fields\TextFormFieldComponent;
use ion\Rendering\Layout\Components\Forms\Fields\EmailFormFieldComponent;
use ion\ObservableInterface;
use ion\ObserverInterface;
use ion\Types\Arrays\MapInterface;
use ion\Types\Arrays\Map;
use ion\WordPress\Connect\Data\DataAccess;
class FormGroupComponent extends Component implements FormGroupComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor($clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        if (!$descriptor instanceof FormGroupDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $renderOptions, $parent, $hooks);
        $component->setCaption($descriptor->getCaption());
        return $component;
    }
    //    public function getParent(): ?TemplateInterface {
    //
    //        return parent::getParent();
    //
    ////        if($this->ListItemDescriptorTrait_getParentContainer() === null) {
    ////
    ////            return parent::getParent();
    ////        }
    ////
    ////        return $this->ListItemDescriptorTrait_getParentContainer();
    //    }
    //    public function setParent(TemplateInterface $parent = null): TemplateInterface {
    //
    //        if($parent !== null) {
    //
    //            $this->ListItemDescriptorTrait_setParentContainer($parent);
    //        }
    //
    //        return parent::setParent($parent);
    //    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        $this->observe($this->getFields());
        if ($parent !== null) {
            $this->setPage($parent);
        }
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
    }
    /**
     * method
     * 
     * 
     * @return FormFieldComponentInterface
     */
    protected function createItemComponentInstance(FormFieldDescriptorInterface $descriptor)
    {
        if (!$descriptor instanceof FormFieldDescriptorInterface) {
            throw new ComponentException("The descriptor does not implement '\\ion\\Rendering\\Layout\\Descriptors\\Forms\\FormFieldDescriptorInterface.'");
        }
        $clientId = static::generateClientId($descriptor, $this, $this->getFields()->count());
        if ($descriptor instanceof EmailFormFieldDescriptorInterface) {
            return EmailFormFieldComponent::createFromDescriptor($clientId, $descriptor, null, $this);
        }
        if ($descriptor instanceof TextFormFieldDescriptorInterface) {
            return TextFormFieldComponent::createFromDescriptor($clientId, $descriptor, null, $this);
        }
        throw new ComponentException("Could not determine which field component instance to create for the descriptor: '{$descriptor->getObjectName()->toString()}.'");
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onAddObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getFields()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->add($instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onInsertObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getFields()) {
            $instance = $data->get('value');
            if (!$instance instanceof ComponentInterface) {
                $instance = $this->createItemComponentInstance($instance);
            }
            $this->getChildren()->insert($data->get('index'), $instance);
        }
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ObserverInterface
     */
    public function onRemoveObserved(ObservableInterface $observable, MapInterface $data = null)
    {
        if ($observable === $this->getFields()) {
            $this->getChildren()->remove($data->get('index'));
        }
        return $this;
    }
}