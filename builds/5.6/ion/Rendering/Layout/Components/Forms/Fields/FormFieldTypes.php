<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components\Forms\Fields;

/**
 * Description of FormFieldTypes
 *
 * @author Justus
 */
use ion\Types\EnumObject;
class FormFieldTypes extends EnumObject
{
    const TEXT = 1;
    const EMAIL = 2;
}