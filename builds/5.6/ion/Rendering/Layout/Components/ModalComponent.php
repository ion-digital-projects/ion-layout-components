<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Components;

/**
 * Description of ModalComponent
 *
 * @author Justus
 */
use ion\Rendering\Layout\ComponentInterface;
use ion\Rendering\Layout\Component;
use ion\Rendering\Layout\TemplateInterface;
use ion\System\Remote\UriPath;
use ion\Rendering\Layout\Assets\StyleAsset;
use ion\Rendering\Layout\Assets\ScriptAsset;
use ion\Rendering\Layout\TemplateManager;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
use ion\Types\Arrays\Specialized\StringMapBaseInterface;
use ion\Types\Arrays\Specialized\StringMap;
use ion\Rendering\Layout\Descriptors\ModalType;
class ModalComponent extends Component implements ModalComponentInterface
{
    use \ion\Rendering\Layout\Descriptors\ModalDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    public static function createFromDescriptor($clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null)
    {
        if (!$descriptor instanceof ModalDescriptorInterface) {
            return parent::createFromDescriptor($clientId, $descriptor, $renderOptions, $parent, $hooks);
        }
        $component = static::create($clientId, $parent, $renderOptions, $hooks);
        $component->setCaption($descriptor->getCaption() !== null ? $descriptor->getCaption()->toString() : null);
        $component->setName($descriptor->getName() !== null ? $descriptor->getName()->toString() : null);
        $component->setButtons($descriptor->getButtons());
        $component->setCookie($descriptor->hasCookie() ? $descriptor->getCookie() : null);
        $component->setActivationFrequency($descriptor->hasActivationFrequency() ? $descriptor->getActivationFrequency() : null);
        return $component;
    }
    //    public static function createFromTemplate(TemplateInterface $template): TemplateInterface {
    //
    //        $instance = static::create($template->getParent(), $template->getRenderOptions(), $template->getHooks());
    //
    //        foreach($template->getChildren() as $child) {
    //
    //            $instance->getChildren()->add($child->copy());
    //        }
    //
    //        return $instance;
    //    }
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($clientId, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null, StringMapBaseInterface $buttons = null, ModalType $modalType = null, ColourInterface $buttonColour = null, $cookie = null, $activationFrequency = null)
    {
        $this->setButtons($buttons);
        $this->setModalType($modalType === null ? ModalType::DIALOG() : $modalType);
        $this->setButtonColour($buttonColour);
        $this->setCookie($cookie);
        $this->setActivationFrequency($activationFrequency);
        parent::__construct($clientId, $renderOptions, $parent, $hooks);
        if (!$this->getStyleAssets()->hasKey('ion-components-modal')) {
            $path = UriPath::parse('/resources/styles/css/ion-components-modal.css');
            $this->getStyleAssets()->set('ion-components-modal', StyleAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, StringVector::create(['screen']), TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
        }
        if (!$this->getScriptAssets()->hasKey('ion-components-modal')) {
            $path = UriPath::parse('/resources/scripts/js/ion-components-modal.js');
            $this->getScriptAssets()->set('ion-components-modal', ScriptAsset::reference(TemplateManager::getAssetRemotePath($path, 'layout-components', 'ion')->toUri(), 15, true, TemplateManager::getAssetLocalPath($path, 'layout-components', 'ion')));
        }
    }
    /**
     * method
     * 
     * @return ?TemplateInterface
     */
    protected function initialize()
    {
        return $this;
    }
}