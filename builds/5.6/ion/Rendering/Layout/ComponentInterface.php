<?php
namespace ion\Rendering\Layout;

use ion\Rendering\Layout\TemplateInterface;
use ion\Rendering\Layout\Descriptors\DescriptorInterface;
use ion\Rendering\RenderOptionsInterface;
use ion\Types\Arrays\Specialized\CallableMapBaseInterface;
interface ComponentInterface extends TemplateInterface, DescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    static function create($clientId = null, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null);
    /**
     * method
     * 
     * 
     * @return ?ComponentInterface
     */
    static function createFromDescriptor($clientId, DescriptorInterface $descriptor, RenderOptionsInterface $renderOptions = null, TemplateInterface $parent = null, CallableMapBaseInterface $hooks = null);
    /**
     * method
     * 
     * 
     * @return TemplateInterface
     */
    static function createFromTemplate(TemplateInterface $template, $clientId = null);
}