<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Types\Arrays\Specialized\StringMapBaseInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\HorizontalAlignmentPropertyInterface;
use ion\Rendering\Layout\Descriptors\ModalType;
use ion\Rendering\Layout\Descriptors\ModalActivationType;
interface ModalDescriptorInterface extends CaptionPropertyInterface, NamePropertyInterface, ColourPropertyInterface, HorizontalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setActivationFrequency($frequency = null);
    /**
     * method
     * 
     * @return ?int
     */
    function getActivationFrequency();
    /**
     * method
     * 
     * @return bool
     */
    function hasActivationFrequency();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setCookie($cookie = null);
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getCookie();
    /**
     * method
     * 
     * @return bool
     */
    function hasCookie();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setButtonColour(ColourInterface $colour = null);
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getButtonColour();
    /**
     * method
     * 
     * @return bool
     */
    function hasButtonColour();
    /**
     * method
     * 
     * @return StringMapBaseInterface
     */
    function getButtons();
    /**
     * method
     * 
     * @return bool
     */
    function hasButtons();
    /**
     * method
     * 
     * @return ?ModalType
     */
    function getModalType();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalType(ModalType $modalType);
    /**
     * method
     * 
     * @return ModalActivationType
     */
    function getModalActivationType();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalActivationType(ModalActivationType $modalActivationType);
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalDelay($delay);
    /**
     * method
     * 
     * @return int
     */
    function getModalDelay();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalAllowMultipleActivations($allowMultipleActivations);
    /**
     * method
     * 
     * @return bool
     */
    function getModalAllowMultipleActivations();
    /**
     * method
     * 
     * 
     * @return ModalDescriptorInterface
     */
    function setModalContentPadding($modalContentPadding = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getModalContentPadding();
    /**
     * method
     * 
     * @return bool
     */
    function hasModalContentPadding();
}