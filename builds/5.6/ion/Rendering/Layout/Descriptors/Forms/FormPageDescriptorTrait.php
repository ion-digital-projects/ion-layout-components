<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormPageDescriptor
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Rendering\Layout\Descriptors\Forms\FormDescriptorInterface;
use ion\Rendering\Layout\LayoutException;
use ion\Rendering\Layout\DescriptorException;
trait FormPageDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    private $form = null;
    private $groups = null;
    /**
     * method
     * 
     * @return FormDescriptorInterface
     */
    public function getForm()
    {
        if ($this->form === null) {
            throw new DescriptorException("Form page is not attached to a parent form.");
        }
        return $this->form;
    }
    /**
     * method
     * 
     * @return FormGroupDescriptorVectorBaseInterface
     */
    public function getGroups()
    {
        if ($this->groups === null) {
            $this->groups = FormGroupDescriptorVector::create();
        }
        return $this->groups;
    }
    /**
     * method
     * 
     * 
     * @return FormPageDescriptorInterface
     */
    protected function setForm(FormDescriptorInterface $form = null)
    {
        $this->form = $form;
        return $this;
    }
}