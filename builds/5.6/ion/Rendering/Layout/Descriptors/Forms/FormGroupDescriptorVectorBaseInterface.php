<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Rendering\Layout\Descriptors\DescriptorVectorBaseInterface;
interface FormGroupDescriptorVectorBaseInterface extends DescriptorVectorBaseInterface
{
    /**
     * Return a value from the list at a specified index.
     *
     * @param int $index The index of the value to return.
     * @return mixed The value to be returned.
     *
     */
    function get($index);
}