<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorInterface;
interface FormFieldDescriptorInterface extends CaptionPropertyInterface, NamePropertyInterface
{
    /**
     * method
     * 
     * @return FormGroupDescriptorInterface
     */
    function getGroup();
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    function setValue(TypeObjectInterface $value = null);
    /**
     * method
     * 
     * @return ?TypeObjectInterface
     */
    function getValue();
    /**
     * method
     * 
     * 
     * @return FormFieldDescriptorInterface
     */
    function setRequired($required = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function isRequired();
}