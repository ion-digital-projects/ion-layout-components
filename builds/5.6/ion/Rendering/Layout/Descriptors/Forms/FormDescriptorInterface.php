<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Types\Arrays\MapBaseInterface;
use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\NamePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormGroupDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorVectorBaseInterface;
interface FormDescriptorInterface extends CaptionPropertyInterface, UriPropertyInterface, NamePropertyInterface, ColourPropertyInterface
{
    /**
     * method
     * 
     * @return bool
     */
    function isAjaxEnabled();
    /**
     * method
     * 
     * @return bool
     */
    function isCaptchaEnabled();
    /**
     * method
     * 
     * 
     * @return FormDescriptorInterface
     */
    function setSubmitCaption($submitCaption = null);
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getSubmitCaption();
    /**
     * method
     * 
     * @return bool
     */
    function hasSubmitCaption();
    /**
     * method
     * 
     * @return FormPageDescriptorVectorBaseInterface
     */
    function getPages();
    /**
     * method
     * 
     * @return FormGroupDescriptorVectorBaseInterface
     */
    function getGroups();
    /**
     * method
     * 
     * @return FormFieldDescriptorVectorBaseInterface
     */
    function getFields();
    /**
     * method
     * 
     * @return MapBaseInterface
     */
    function getAdditionalValues();
}