<?php
namespace ion\Rendering\Layout\Descriptors\Forms\Fields;

use ion\Rendering\Layout\Descriptors\Forms\FormFieldDescriptorInterface;
/**
 * Description of EmailFormFieldDescriptorTrait
 *
 * @author Justus
 */
interface TextFormFieldDescriptorInterface extends FormFieldDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return TextFormFieldDescriptorInterface
     */
    function setMultiLine($multiLine);
    /**
     * method
     * 
     * @return bool
     */
    function isMultiLine();
}