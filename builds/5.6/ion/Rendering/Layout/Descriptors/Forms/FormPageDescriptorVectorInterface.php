<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorVectorBaseInterface;
use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorInterface;
interface FormPageDescriptorVectorInterface
{
    /**
     *
     * Add a value to the end of the list.
     *
     * @param FormPageDescriptorInterface $value The value to be added to the list.
     * @return  The modified vector.
     *
     */
    function add(FormPageDescriptorInterface $value);
    /**
     *
     * Insert a value to a specific position in the list.
     *
     * @param int $index The index where to insert the value.
     * @param FormPageDescriptorInterface $value The value to be inserted into the list.
     * @return  The modified vector.
     *
     */
    function insert($index, FormPageDescriptorInterface $value);
    /**
     *
     * Replace an existing value in the list by index.
     *
     * @param int $index The index of the value to replace.
     * @param FormPageDescriptorInterface $value The value to set.
     * @return  The modified vector.
     *
     */
    function set($index, FormPageDescriptorInterface $value);
    /**
     *
     * Checks whether a list contains a value.
     *
     * @param FormPageDescriptorInterface $value The value to look for in the list.
     * @param ?int $index Combine the value with an index and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the value exists, __FALSE__ otherwise.
     *
     */
    function hasValue(FormPageDescriptorInterface $value, $index = null);
    /**
     *
     * Remove all references of a value from the Vector.
     *
     * @param FormPageDescriptorInterface $value The value to look for in the list.
     * @return  The modified vector.
     *
     */
    function removeValue(FormPageDescriptorInterface $value = null);
    /**
     *
     * Add a range of values to the list from an .
     *
     * @param  $values The  to be added to the list.
     * @return  The modified vector.
     *
     */
    function addVector(FormPageDescriptorVectorBaseInterface $values);
    /**
     *
     * Strip empty elements from the list.
     *
     * @param ?FormPageDescriptorInterface $value Remove only items that match a certain value - otherwise remove all empty elements (if NULL).
     * @return  This object.
     *
     */
    function strip(FormPageDescriptorVectorBaseInterface $values = null);
}