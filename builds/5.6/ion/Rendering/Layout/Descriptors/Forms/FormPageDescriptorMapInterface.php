<?php
namespace ion\Rendering\Layout\Descriptors\Forms;

use ion\Rendering\Layout\Descriptors\Forms\FormPageDescriptorInterface;
interface FormPageDescriptorMapInterface
{
    /**
     *
     * Set and associate a value in the map with a key.
     *
     * @param FormPageDescriptorInterface $key The key to be associated with the value.
     * @param FormPageDescriptorInterface $value The string value to be defined in the map.
     * @return MapInterface The modified map.
     *
     */
    function set($key, FormPageDescriptorInterface $value);
    /**
     *
     * Checks whether a map contains either just a key, or a key and value combination.
     *
     * @param FormPageDescriptorInterface $value The string value to look for in the map.
     * @param ?string $key Combine the value with a key and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the key and/or value exists, __FALSE__ otherwise.
     *
     */
    function hasValue(FormPageDescriptorInterface $value, $key = null);
    /**
     *
     * Remove all references of a value from the map
     *
     * @param FormPageDescriptorInterface $value The value to look for in the map.
     * @return  The modified map
     *
     */
    function removeValue(FormPageDescriptorInterface $value = null);
}