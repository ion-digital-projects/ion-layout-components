<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Forms;

/**
 * Description of FormFieldDescriptorInterface
 *
 * @author Justus.Meyer
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Types\TypeObjectInterface;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Types\StringObject;
abstract class FormFieldDescriptor extends Descriptor implements FormFieldDescriptorInterface
{
    use FormFieldDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct(FormGroupDescriptorInterface $group, $caption, $name = null)
    {
        $this->setGroup($group);
        $this->setCaption($caption);
        $this->setName($name !== null ? $name : StringObject::create($name)->toDashed()->toString());
    }
}