<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\DescriptorException;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
trait NavigationDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait {
        getImage as getLogo;
        setImage as setLogo;
        hasImage as hasLogo;
        setImageVisible as setLogoVisible;
        isImageVisible as isLogoVisible;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait {
        getUri as getSearchUri;
        setUri as setSearchUri;
        hasUri as hasSearchUri;
    }
    private $menu = null;
    private $action = null;
    private $searchEnabled = null;
    private $actionEnabled = null;
    private $accountEnabled = null;
    private $accountMenu = null;
    private $accountUri = null;
    private $searchParameterName = null;
    private $dropDownStyle = null;
    private $initialColourAlpha = null;
    private $shadowEnabled = null;
    private $shadowEnabledInitially = null;
    private $smallEnabled = null;
    private $mediumEnabled = null;
    private $largeEnabled = null;
    private $initialColour = null;
    /**
     * method
     * 
     * 
     * @return ListItemContainerInterface
     */
    private static function setItemDefaults(ListItemContainerInterface $container)
    {
        foreach ($container->getItems() as $item) {
            $item->setCaptionVisible(null, $item->isCaptionVisible(), true, true);
            $item->setIconVisible(null, $item->isIconVisible(), false, false);
            static::setItemDefaults($item);
        }
        return $container;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setMenu(ListItemContainerInterface $items = null)
    {
        $this->menu = $items === null ? null : static::setItemDefaults($items);
        return $this;
    }
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    public function getMenu()
    {
        if ($this->menu === null) {
            throw new DescriptorException("No menu has been specified.");
        }
        return $this->menu;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasMenu()
    {
        return $this->menu !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAction(ActionDescriptorInterface $action = null)
    {
        $this->action = $action;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ActionDescriptorInterface
     */
    public function getAction()
    {
        return $this->action;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAction()
    {
        return $this->action !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSearchEnabled($enabled = null)
    {
        $this->searchEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isSearchEnabled()
    {
        return $this->searchEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountEnabled($enabled = null)
    {
        $this->accountEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isAccountEnabled()
    {
        return $this->accountEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountMenu(ListItemContainerInterface $menu = null)
    {
        $this->accountMenu = $menu;
        return $this;
    }
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    public function getAccountMenu()
    {
        return $this->accountMenu;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAccountMenu()
    {
        return $this->accountEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setAccountUri(UriInterface $uri = null)
    {
        $this->accountUri = $uri;
        return $this;
    }
    /**
     * method
     * 
     * @return UriInterface
     */
    public function getAccountUri()
    {
        return $this->accountUri;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAccountUri()
    {
        return $this->accountUri === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSearchParameterName($searchParameterName = null)
    {
        $this->searchParameterName = $searchParameterName;
        return $this;
    }
    /**
     * method
     * 
     * @return ?string
     */
    public function getSearchParameterName()
    {
        return $this->searchParameterName;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasSearchParameterName()
    {
        return $this->getSearchParameterName() !== null;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setActionEnabled($enabled = null)
    {
        $this->actionEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isActionEnabled()
    {
        return $this->actionEnabled === true;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setInitialColourAlpha($initialColourAlpha = null)
    {
        $this->initialColourAlpha = $initialColourAlpha;
        return $this;
    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getInitialColourAlpha()
    {
        return $this->initialColourAlpha;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasInitialColourAlpha()
    {
        return $this->initialColourAlpha !== null;
    }
    //            bool $shadow = null,
    //            bool $initialShadow = null
    //            bool $disableLarge = null,
    //            bool $disableMedium = null
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setShadowEnabled($enabled = null)
    {
        $this->shadowEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isShadowEnabled()
    {
        return $this->shadowEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getShadowEnabled()
    {
        return $this->shadowEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setShadowEnabledInitially($enabled = null)
    {
        $this->shadowEnabledInitially = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isShadowEnabledInitially()
    {
        return $this->shadowEnabledInitially === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getShadowEnabledInitially()
    {
        return $this->shadowEnabledInitially;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setSmallEnabled($enabled = null)
    {
        $this->smallEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isSmallEnabled()
    {
        return $this->smallEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getSmallEnabled()
    {
        return $this->smallEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setMediumEnabled($enabled = null)
    {
        $this->mediumEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isMediumEnabled()
    {
        return $this->mediumEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getMediumEnabled()
    {
        return $this->mediumEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setLargeEnabled($enabled = null)
    {
        $this->largeEnabled = $enabled;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isLargeEnabled()
    {
        return $this->largeEnabled === true;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getLargeEnabled()
    {
        return $this->largeEnabled;
    }
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    public function setInitialColour(ColourInterface $colour = null)
    {
        $this->initialColour = $colour;
        return $this;
    }
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    public function getInitialColour()
    {
        return $this->initialColour;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasInitialColour()
    {
        return $this->initialColour !== null;
    }
}