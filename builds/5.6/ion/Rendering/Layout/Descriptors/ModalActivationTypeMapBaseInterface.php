<?php
namespace ion\Rendering\Layout\Descriptors;

interface ModalActivationTypeMapBaseInterface
{
    /**
     *
     * Return a value from the map by key.
     *
     * @param ModalActivationType $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */
    function get($key);
}