<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\Rendering\Layout\Descriptors\DescriptorVectorBaseInterface;
interface ListDescriptorVectorBaseInterface extends DescriptorVectorBaseInterface
{
    /**
     * Return a value from the list at a specified index.
     *
     * @param int $index The index of the value to return.
     * @return mixed The value to be returned.
     *
     */
    function get($index);
}