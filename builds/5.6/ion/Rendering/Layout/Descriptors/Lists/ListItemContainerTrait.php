<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Lists;

/**
 * Description of FormDescriptorTrait
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriActionInterface;
use ion\CopyableInterface;
trait ListItemContainerTrait
{
    private $items = null;
    //    private $showIcons = null;
    //    private $showCaptions = null;
    //
    //    public function setShowCaptions(bool $showCaptions): ListItemContainerInterface {
    //
    //        $this->showCaptions = $showCaptions;
    //        return $this;
    //    }
    //
    //    public function getShowCaptions(): bool {
    //
    //        return $this->showCaptions;
    //    }
    //
    //    public function setShowIcons(bool $showCaptions): ListItemContainerInterface {
    //
    //        $this->showIcons = $showIcons;
    //        return $this;
    //    }
    //
    //    public function getShowIcons(): bool {
    //
    //        return $this->showIcons;
    //    }
    /**
     * method
     * 
     * 
     * @return ListItemDescriptorVectorBaseInterface
     */
    public function getItems($startLevel = 0, $endLevel = null)
    {
        if ($this->items === null) {
            $this->items = ListItemDescriptorVector::create();
        }
        if ($endLevel === null) {
            if ($startLevel === 0) {
                return $this->items;
            }
        }
        $containers = [$this->copy()];
        $tmpContainers = [];
        for ($level = 0; $level < $startLevel; $level++) {
            foreach ($containers as $container) {
                foreach ($container->getItems() as $childContainer) {
                    $tmpContainers[] = $childContainer->copy();
                }
            }
            $containers = $tmpContainers;
            $tmpContainers = [];
        }
        $startContainers = ListItemDescriptorVector::create($containers);
        if ($endLevel < $startLevel) {
            return $startContainers;
        }
        for ($level = 0; $level > $endLevel - $startLevel; $level++) {
            if ($level >= $endLevel) {
                foreach ($containers as $container) {
                    $container->getItems()->clear();
                }
                break;
            }
        }
        return $startContainers;
    }
    /**
     * method
     * 
     * @return CopyableInterface
     */
    public function copy()
    {
        $instance = clone $this;
        $children = ListItemDescriptorVector::create();
        foreach ($this->getItems() as $item) {
            $children->add($item->copy());
        }
        $instance->getItems()->clear()->addVector($children);
        return $instance;
    }
    /**
     * method
     * 
     * 
     * @return ListItemContainerInterface
     */
    public function addItem($caption, UriInterface $target, UriActionInterface $uriAction = null, $uriActionModal = null, $icon = null, $showIcon = null, $showCaption = null, $order = null, $tag = null)
    {
        $this->getItems()->add(new ListItemDescriptor($this, $caption, $target, $uriAction, $uriActionModal, $icon, $showIcon, $showCaption, $order, $tag));
        return $this;
    }
}