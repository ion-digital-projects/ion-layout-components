<?php
namespace ion\Rendering\Layout\Descriptors\Lists;

use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriActionInterface;
use ion\CopyableInterface;
use ion\Rendering\Layout\Descriptors\Lists\ListItemDescriptorVectorBaseInterface;
interface ListItemContainerInterface
{
    /**
     * method
     * 
     * 
     * @return ListItemDescriptorVectorBaseInterface
     */
    function getItems($startLevel = 0, $endLevel = null);
    /**
     * method
     * 
     * @return CopyableInterface
     */
    function copy();
    /**
     * method
     * 
     * 
     * @return ListItemContainerInterface
     */
    function addItem($caption, UriInterface $target, UriActionInterface $uriAction = null, $uriActionModal = null, $icon = null, $showIcon = null, $showCaption = null, $order = null, $tag = null);
}