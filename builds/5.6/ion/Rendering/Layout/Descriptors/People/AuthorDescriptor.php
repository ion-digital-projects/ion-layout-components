<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\People;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\Types\Arrays\Specialized\StringVectorInterface;
class AuthorDescriptor extends PersonDescriptor implements AuthorDescriptorInterface
{
    use AuthorDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($displayName = null, StringVectorInterface $names = null, UriInterface $uri = null, $text = null)
    {
        parent::__construct($displayName, $names, $uri, $text);
    }
}