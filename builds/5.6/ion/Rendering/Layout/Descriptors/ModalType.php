<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalType
 *
 * @author Justus
 */
use ion\Types\EnumObject;
class ModalType extends EnumObject
{
    const DIALOG = 1;
    const NOTICE = 2;
}