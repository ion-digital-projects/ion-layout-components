<?php
namespace ion\Rendering\Layout\Descriptors\Media;

use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\Descriptors\Media\MediaDescriptorInterface;
interface ImageDescriptorInterface extends MediaDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    function setRound($round = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getRound();
    /**
     * method
     * 
     * @return bool
     */
    function isRound();
    /**
     * method
     * 
     * 
     * @return ImageDescriptorInterface
     */
    function setColour(ColourInterface $colour = null);
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getColour();
    /**
     * method
     * 
     * @return bool
     */
    function hasColour();
}