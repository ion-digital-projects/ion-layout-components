<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
interface ModalDescriptorMapInterface
{
    /**
     *
     * Set and associate a value in the map with a key.
     *
     * @param ModalDescriptorInterface $key The key to be associated with the value.
     * @param ModalDescriptorInterface $value The string value to be defined in the map.
     * @return MapInterface The modified map.
     *
     */
    function set($key, ModalDescriptorInterface $value);
    /**
     *
     * Checks whether a map contains either just a key, or a key and value combination.
     *
     * @param ModalDescriptorInterface $value The string value to look for in the map.
     * @param ?string $key Combine the value with a key and look for the combination - look only for the value if omitted.
     * @return bool Returns __TRUE_ if the key and/or value exists, __FALSE__ otherwise.
     *
     */
    function hasValue(ModalDescriptorInterface $value, $key = null);
    /**
     *
     * Remove all references of a value from the map
     *
     * @param ModalDescriptorInterface $value The value to look for in the map.
     * @return  The modified map
     *
     */
    function removeValue(ModalDescriptorInterface $value = null);
}