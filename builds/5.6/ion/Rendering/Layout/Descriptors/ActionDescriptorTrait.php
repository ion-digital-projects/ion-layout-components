<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of ModalDescriptorTrait
 *
 * @author Justus
 */
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Lists\ListDescriptorInterface;
use ion\Rendering\Layout\DescriptorException;
use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
trait ActionDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\UriPropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait;
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait;
    //use \ion\Rendering\Layout\Descriptors\Properties\ColorPropertyTrait;
    private $autoClickDelay = 0;
    private $autoClickCountdown = 0;
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    public function setAutoClickDelay($autoClickDelay = 0)
    {
        $this->autoClickDelay = $autoClickDelay;
        return $this;
    }
    /**
     * method
     * 
     * @return int
     */
    public function getAutoClickDelay()
    {
        return $this->autoClickDelay;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAutoClickDelay()
    {
        return $this->autoClickDelay > 0;
    }
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    public function setAutoClickCountdown($autoClickCountdown = 0)
    {
        $this->autoClickCountdown = $autoClickCountdown;
        return $this;
    }
    /**
     * method
     * 
     * @return int
     */
    public function getAutoClickCountdown()
    {
        return $this->autoClickCountdown;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAutoClickCountdown()
    {
        return $this->autoClickCountdown > 0;
    }
}