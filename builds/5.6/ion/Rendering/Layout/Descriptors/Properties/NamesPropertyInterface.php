<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
use ion\Types\Arrays\Specialized\StringVectorInterface;
interface NamesPropertyInterface
{
    /**
     * method
     * 
     * @return StringVectorInterface
     */
    function getNames();
    /**
     * method
     * 
     * @return bool
     */
    function hasNames();
    /**
     * method
     * 
     * 
     * @return ?StringInterface
     */
    function getFullName($seperator = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasFirstName();
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getFirstName();
    /**
     * method
     * 
     * @return bool
     */
    function hasLastName();
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getLastName();
}