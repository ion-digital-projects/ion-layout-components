<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface TagPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getTag();
    /**
     * method
     * 
     * 
     * @return TagPropertyInterface
     */
    function setTag($tag = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasTag();
}