<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\People\AuthorDescriptorInterface;
interface AuthorPropertyInterface
{
    /**
     * method
     * 
     * @return ?AuthorDescriptorInterface
     */
    function getAuthor();
    /**
     * method
     * 
     * 
     * @return AuthorPropertyInterface
     */
    function setAuthor(AuthorDescriptorInterface $author = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasAuthor();
}