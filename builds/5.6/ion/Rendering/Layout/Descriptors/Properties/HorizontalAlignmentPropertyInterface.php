<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
interface HorizontalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return HorizontalAlignmentPropertyInterface
     */
    function setHorizontalAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null);
    /**
     * method
     * 
     * @return HorizontalAlignmentTypeInterface
     */
    function getHorizontalAlignment();
    /**
     * method
     * 
     * @return bool
     */
    function hasHorizontalAlignment();
}