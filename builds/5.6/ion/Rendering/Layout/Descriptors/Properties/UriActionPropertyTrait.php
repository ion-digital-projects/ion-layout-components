<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
use ion\Rendering\Layout\UriActionInterface;
use ion\Rendering\Layout\UriAction;
trait UriActionPropertyTrait
{
    private $uriAction = null;
    private $uriModalName = null;
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    public function setUriAction(UriActionInterface $action = null)
    {
        $this->uriAction = $action;
        return $this;
    }
    /**
     * method
     * 
     * @return ?UriActionInterface
     */
    public function getUriAction()
    {
        return $this->uriAction;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasUriAction()
    {
        return $this->getUriAction() !== null && !$this->getUriAction()->equals(UriAction::NONE());
    }
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    public function setUriModalName($uriModalName = null)
    {
        $this->uriModalName = $uriModalName;
        return $this;
    }
    /**
     * method
     * 
     * @return ?string
     */
    public function getUriModalName()
    {
        return $this->uriModalName;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasUriModalName()
    {
        return $this->getUriModalName() !== null;
    }
}