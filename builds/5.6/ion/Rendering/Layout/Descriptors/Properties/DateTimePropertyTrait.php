<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Types\DateTimeInterface;
use ion\Types\DateTimeObject;
trait DateTimePropertyTrait
{
    private $dateTime = null;
    /**
     * method
     * 
     * @return ?DateTimeInterface
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }
    /**
     * method
     * 
     * 
     * @return DateTimePropertyInterface
     */
    public function setDateTime(DateTimeInterface $dateTime = null)
    {
        $this->dateTime = $dateTime;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasDateTime()
    {
        return $this->dateTime !== null;
    }
}