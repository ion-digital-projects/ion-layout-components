<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\UriActionInterface;
interface UriActionPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    function setUriAction(UriActionInterface $action = null);
    /**
     * method
     * 
     * @return ?UriActionInterface
     */
    function getUriAction();
    /**
     * method
     * 
     * @return bool
     */
    function hasUriAction();
    /**
     * method
     * 
     * 
     * @return UriActionPropertyInterface
     */
    function setUriModalName($uriModalName = null);
    /**
     * method
     * 
     * @return ?string
     */
    function getUriModalName();
    /**
     * method
     * 
     * @return bool
     */
    function hasUriModalName();
}