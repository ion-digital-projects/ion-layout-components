<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait OrderPropertyTrait
{
    private $order = null;
    /**
     * method
     * 
     * @return ?int
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * method
     * 
     * 
     * @return OrderPropertyInterface
     */
    public function setOrder($order = null)
    {
        if ($order === null) {
            $this->order = null;
            return $this;
        }
        $this->order = $order;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasOrder()
    {
        return $this->order !== null;
    }
}