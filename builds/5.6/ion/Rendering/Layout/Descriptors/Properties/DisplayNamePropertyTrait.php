<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait DisplayNamePropertyTrait
{
    private $displayName = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }
    /**
     * method
     * 
     * 
     * @return DisplayNamePropertyInterface
     */
    public function setDisplayName($displayName = null)
    {
        if ($displayName === null) {
            $this->displayName = null;
            return $this;
        }
        $this->displayName = StringObject::create($displayName);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasDisplayName()
    {
        return $this->displayName !== null;
    }
}