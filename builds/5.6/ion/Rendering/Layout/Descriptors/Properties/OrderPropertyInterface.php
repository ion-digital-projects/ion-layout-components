<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface OrderPropertyInterface
{
    /**
     * method
     * 
     * @return ?int
     */
    function getOrder();
    /**
     * method
     * 
     * 
     * @return OrderPropertyInterface
     */
    function setOrder($order = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasOrder();
}