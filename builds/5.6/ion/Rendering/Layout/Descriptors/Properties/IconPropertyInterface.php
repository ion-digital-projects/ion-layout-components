<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface IconPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getIcon();
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    function setIcon($icon = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasIcon();
    /**
     * method
     * 
     * 
     * @return IconPropertyInterface
     */
    function setIconVisible($default = null, $onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null);
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    function isIconVisible($onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null);
}