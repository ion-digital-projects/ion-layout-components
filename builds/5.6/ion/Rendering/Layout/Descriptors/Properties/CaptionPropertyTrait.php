<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait CaptionPropertyTrait
{
    private $caption = null;
    private $captionVisible = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getCaption()
    {
        return $this->caption;
    }
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    public function setCaption($caption = null)
    {
        if ($caption === null) {
            $this->caption = null;
            return $this;
        }
        $this->caption = StringObject::create($caption);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasCaption()
    {
        return $this->caption !== null;
    }
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    public function setCaptionVisible($default = null, $onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null)
    {
        $this->captionVisible = ['default' => $default, 'large' => $onLargeScreens, 'medium' => $onMediumScreens, 'small' => $onSmallScreens];
        return $this;
    }
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    public function isCaptionVisible($onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null)
    {
        if ($this->captionVisible === null) {
            return null;
        }
        $result = null;
        if ($onLargeScreens === true) {
            if ($this->captionVisible['large'] === false) {
                return false;
            }
            $result = true;
        }
        if ($onMediumScreens === true) {
            if ($this->captionVisible['medium'] === false) {
                return false;
            }
            $result = true;
        }
        if ($onSmallScreens === true) {
            if ($this->captionVisible['small'] === false) {
                return false;
            }
            $result = true;
        }
        if ($result !== null) {
            return $result;
        }
        return $this->captionVisible['default'];
    }
}