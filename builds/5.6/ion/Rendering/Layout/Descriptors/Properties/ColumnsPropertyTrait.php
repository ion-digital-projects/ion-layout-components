<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait ColumnsPropertyTrait
{
    private $columns = null;
    //    public function getColumns(): int {
    //
    //        return $this->columns;
    //    }
    //
    //    public function setColumns(int $columns): ColumnsPropertyInterface {
    //
    //        $this->columns = $columns;
    //        return $this;
    //    }
    /**
     * method
     * 
     * @return ?int
     */
    public function getColumns()
    {
        if ($this->columns !== null && $this->columns < 1) {
            $this->columns = 1;
            return 1;
        }
        return $this->columns;
    }
    /**
     * method
     * 
     * 
     * @return ColumnsPropertyInterface
     */
    public function setColumns($columns = null)
    {
        $this->columns = $columns;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasColumns()
    {
        return $this->columns !== null;
    }
}