<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\System\Remote\UriInterface;
interface TargetUriPropertyInterface
{
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getTargetUri();
    /**
     * method
     * 
     * 
     * @return TargetUriPropertyInterface
     */
    function setTargetUri(UriInterface $uri = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasTargetUri();
}