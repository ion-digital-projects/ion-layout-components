<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface TextPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getText();
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    function setText($text = null, $escapeText = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasText();
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    function setEscapeText($escapeText = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getEscapeText();
    /**
     * method
     * 
     * @return bool
     */
    function isTextEscaped();
}