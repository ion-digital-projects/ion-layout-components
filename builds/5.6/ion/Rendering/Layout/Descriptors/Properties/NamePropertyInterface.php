<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface NamePropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getName();
    /**
     * method
     * 
     * 
     * @return NamePropertyInterface
     */
    function setName($name = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasName();
}