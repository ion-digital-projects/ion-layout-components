<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
interface VerticalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return VerticalAlignmentPropertyInterface
     */
    function setVerticalAlignment(VerticalAlignmentTypeInterface $verticalAlignment = null);
    /**
     * method
     * 
     * @return VerticalAlignmentTypeInterface
     */
    function getVerticalAlignment();
    /**
     * method
     * 
     * @return bool
     */
    function hasVerticalAlignment();
}