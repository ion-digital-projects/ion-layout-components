<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface VisiblePropertyInterface
{
    /**
     * method
     * 
     * @return ?bool
     */
    function getVisible();
    /**
     * method
     * 
     * 
     * @return VisiblePropertyInterface
     */
    function setVisible($visible = null);
    /**
     * method
     * 
     * @return bool
     */
    function isVisible();
}