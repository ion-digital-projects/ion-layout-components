<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface DisplayNamePropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getDisplayName();
    /**
     * method
     * 
     * 
     * @return DisplayNamePropertyInterface
     */
    function setDisplayName($displayName = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasDisplayName();
}