<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Types\StringInterface;
interface CaptionPropertyInterface
{
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getCaption();
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    function setCaption($caption = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasCaption();
    /**
     * method
     * 
     * 
     * @return CaptionPropertyInterface
     */
    function setCaptionVisible($default = null, $onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null);
    /**
     * method
     * 
     * 
     * @return ?bool
     */
    function isCaptionVisible($onLargeScreens = null, $onMediumScreens = null, $onSmallScreens = null);
}