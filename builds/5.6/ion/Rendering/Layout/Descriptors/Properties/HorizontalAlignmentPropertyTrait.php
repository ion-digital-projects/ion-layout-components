<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use ion\Rendering\Layout\HorizontalAlignmentType;
trait HorizontalAlignmentPropertyTrait
{
    private $horizontalAlignment = null;
    /**
     * method
     * 
     * 
     * @return HorizontalAlignmentPropertyInterface
     */
    public function setHorizontalAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null)
    {
        if ($horizontalAlignment === null) {
            $this->horizontalAlignment = HorizontalAlignmentType::NONE();
            return $this;
        }
        $this->horizontalAlignment = $horizontalAlignment;
        return $this;
    }
    /**
     * method
     * 
     * @return HorizontalAlignmentTypeInterface
     */
    public function getHorizontalAlignment()
    {
        if ($this->horizontalAlignment === null) {
            $this->horizontalAlignment = HorizontalAlignmentType::NONE();
        }
        return $this->horizontalAlignment;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasHorizontalAlignment()
    {
        return !$this->getHorizontalAlignment()->equals(HorizontalAlignmentType::NONE());
    }
}