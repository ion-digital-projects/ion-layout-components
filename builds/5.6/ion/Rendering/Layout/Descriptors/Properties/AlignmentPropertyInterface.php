<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\HorizontalAlignmentTypeInterface;
use ion\Rendering\Layout\VerticalAlignmentTypeInterface;
interface AlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return AlignmentPropertyInterface
     */
    function setAlignment(HorizontalAlignmentTypeInterface $horizontalAlignment = null, VerticalAlignmentTypeInterface $verticalAlignment = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasAlignment();
}