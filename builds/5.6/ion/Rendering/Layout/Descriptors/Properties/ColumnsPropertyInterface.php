<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

interface ColumnsPropertyInterface
{
    /**
     * method
     * 
     * @return ?int
     */
    function getColumns();
    /**
     * method
     * 
     * 
     * @return ColumnsPropertyInterface
     */
    function setColumns($columns = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasColumns();
}