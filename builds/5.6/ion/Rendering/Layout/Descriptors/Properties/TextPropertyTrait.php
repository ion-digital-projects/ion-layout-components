<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
trait TextPropertyTrait
{
    private $text = null;
    private $escapeText = null;
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    public function setText($text = null, $escapeText = null)
    {
        $this->setEscapeText($escapeText);
        if ($text === null) {
            $this->text = null;
            return $this;
        }
        $this->text = StringObject::create($text);
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasText()
    {
        return $this->text !== null;
    }
    /**
     * method
     * 
     * 
     * @return TextPropertyInterface
     */
    public function setEscapeText($escapeText = null)
    {
        $this->escapeText = $escapeText;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getEscapeText()
    {
        return $this->escapeText;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function isTextEscaped()
    {
        return $this->escapeText === true;
    }
}