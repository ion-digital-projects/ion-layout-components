<?php
namespace ion\Rendering\Layout\Descriptors\Properties;

use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
interface ImagePropertyInterface
{
    /**
     * method
     * 
     * @return ?ImageDescriptorInterface
     */
    function getImage();
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    function setImage(ImageDescriptorInterface $image = null);
    /**
     * method
     * 
     * @return bool
     */
    function hasImage();
    /**
     * method
     * 
     * 
     * @return ImagePropertyInterface
     */
    function setImageVisible($visible = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function isImageVisible();
}