<?php
//
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Properties;

/**
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\People\AuthorDescriptorInterface;
trait AuthorPropertyTrait
{
    private $author = null;
    /**
     * method
     * 
     * @return ?AuthorDescriptorInterface
     */
    public function getAuthor()
    {
        return $this->author;
    }
    /**
     * method
     * 
     * 
     * @return AuthorPropertyInterface
     */
    public function setAuthor(AuthorDescriptorInterface $author = null)
    {
        $this->author = $author;
        return $this;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasAuthor()
    {
        return $this->author !== null;
    }
}