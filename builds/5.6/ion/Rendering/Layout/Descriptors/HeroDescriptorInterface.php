<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\MediaPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\VerticalAlignmentPropertyInterface;
interface HeroDescriptorInterface extends CaptionPropertyInterface, MediaPropertyInterface, UriPropertyInterface, VerticalAlignmentPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return HeroDescriptorInterface
     */
    function setUriVisible($uriVisible = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getUriVisible();
    /**
     * method
     * 
     * @return bool
     */
    function isUriVisible();
}