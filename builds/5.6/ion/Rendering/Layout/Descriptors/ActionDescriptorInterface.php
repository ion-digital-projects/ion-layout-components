<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Properties\CaptionPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
interface ActionDescriptorInterface extends CaptionPropertyInterface, UriPropertyInterface, ImagePropertyInterface, ColourPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    function setAutoClickDelay($autoClickDelay = 0);
    /**
     * method
     * 
     * @return int
     */
    function getAutoClickDelay();
    /**
     * method
     * 
     * @return bool
     */
    function hasAutoClickDelay();
    /**
     * method
     * 
     * 
     * @return ActionDescriptorInterface
     */
    function setAutoClickCountdown($autoClickCountdown = 0);
    /**
     * method
     * 
     * @return int
     */
    function getAutoClickCountdown();
    /**
     * method
     * 
     * @return bool
     */
    function hasAutoClickCountdown();
}