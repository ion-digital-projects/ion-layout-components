<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

interface ContentItemDescriptorMapBaseInterface
{
    /**
     *
     * Return a value from the map by key.
     *
     * @param ContentItemDescriptorInterface $key The key of the map value that will be returned.
     * @return mixed The map value to be returned.
     *
     */
    function get($key);
}