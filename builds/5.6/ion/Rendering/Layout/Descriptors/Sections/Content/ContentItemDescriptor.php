<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

/**
 * Description of ContentDescriptor
 *
 * @author Justus
 */
use ion\Rendering\Layout\Descriptors\Descriptor;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\Descriptors\Media\ImageDescriptorInterface;
use ion\Rendering\Layout\Descriptors\People\AuthorDescriptorInterface;
use ion\Types\DateTimeInterface;
class ContentItemDescriptor extends Descriptor implements ContentItemDescriptorInterface
{
    use ContentItemDescriptorTrait;
    /**
     * method
     * 
     * 
     * @return mixed
     */
    public function __construct($text = null, $caption = null, UriInterface $uri = null, ImageDescriptorInterface $image = null, DateTimeInterface $dateTime = null, AuthorDescriptorInterface $author = null, $tag = null, $prioritized = null, $singular = null, $index = null)
    {
        parent::__construct();
        $this->setText($text);
        $this->setCaption($caption);
        $this->setUri($uri);
        $this->setImage($image);
        $this->setDateTime($dateTime);
        $this->setAuthor($author);
        $this->setTag($tag);
        $this->setPrioritized($prioritized);
        $this->setSingular($singular);
        $this->setIndex($index);
    }
}