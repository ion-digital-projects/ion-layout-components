<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\Rendering\Layout\Descriptors\Lists\GridDescriptorInterface;
interface ContentDescriptorInterface extends GridDescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return ContentDescriptorInterface
     */
    function setImagesVisible($visible = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getImagesVisible();
    /**
     * method
     * 
     * @return bool
     */
    function areImagesVisible();
}