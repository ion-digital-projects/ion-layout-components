<?php
namespace ion\Rendering\Layout\Descriptors\Sections\Content;

use ion\System\Remote\UriInterface;
use ion\System\Remote\UriMapBaseInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
interface ContentPagingDescriptorInterface extends ColourPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setPageSize($pageSize = null);
    /**
     * method
     * 
     * @return ?int
     */
    function getPageSize();
    /**
     * method
     * 
     * @return bool
     */
    function hasPageSize();
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setEndSize($endSize = null);
    /**
     * method
     * 
     * @return ?int
     */
    function getEndSize();
    /**
     * method
     * 
     * @return bool
     */
    function hasEndSize();
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setNextPreviousEnabled($nextPrevious);
    /**
     * method
     * 
     * @return bool
     */
    function isNextPreviousEnabled();
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getNextLink();
    /**
     * method
     * 
     * @return ?UriInterface
     */
    function getPreviousLink();
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setItemCount($itemCount);
    /**
     * method
     * 
     * @return int
     */
    function getItemCount();
    /**
     * method
     * 
     * @return int
     */
    function getPageCount();
    /**
     * method
     * 
     * 
     * @return ContentPagingDescriptorInterface
     */
    function setCurrentPage($pageIndex);
    /**
     * method
     * 
     * @return int
     */
    function getCurrentPage();
    /**
     * method
     * 
     * @return UriMapBaseInterface
     */
    function getPageLinks();
}