<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Rendering\Layout\Descriptors\Lists\ListItemContainerInterface;
use ion\System\Remote\UriInterface;
use ion\Rendering\Layout\ColourInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\UriPropertyInterface;
use ion\Rendering\Layout\Descriptors\ActionDescriptorInterface;
interface NavigationDescriptorInterface extends ImagePropertyInterface, ColourPropertyInterface, UriPropertyInterface
{
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setMenu(ListItemContainerInterface $items = null);
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    function getMenu();
    /**
     * method
     * 
     * @return bool
     */
    function hasMenu();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAction(ActionDescriptorInterface $action = null);
    /**
     * method
     * 
     * @return ?ActionDescriptorInterface
     */
    function getAction();
    /**
     * method
     * 
     * @return bool
     */
    function hasAction();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSearchEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isSearchEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isAccountEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountMenu(ListItemContainerInterface $menu = null);
    /**
     * method
     * 
     * @return ListItemContainerInterface
     */
    function getAccountMenu();
    /**
     * method
     * 
     * @return bool
     */
    function hasAccountMenu();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setAccountUri(UriInterface $uri = null);
    /**
     * method
     * 
     * @return UriInterface
     */
    function getAccountUri();
    /**
     * method
     * 
     * @return bool
     */
    function hasAccountUri();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSearchParameterName($searchParameterName = null);
    /**
     * method
     * 
     * @return ?string
     */
    function getSearchParameterName();
    /**
     * method
     * 
     * @return bool
     */
    function hasSearchParameterName();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setActionEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isActionEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setInitialColourAlpha($initialColourAlpha = null);
    /**
     * method
     * 
     * @return ?int
     */
    function getInitialColourAlpha();
    /**
     * method
     * 
     * @return bool
     */
    function hasInitialColourAlpha();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setShadowEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isShadowEnabled();
    /**
     * method
     * 
     * @return ?bool
     */
    function getShadowEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setShadowEnabledInitially($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isShadowEnabledInitially();
    /**
     * method
     * 
     * @return ?bool
     */
    function getShadowEnabledInitially();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setSmallEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isSmallEnabled();
    /**
     * method
     * 
     * @return ?bool
     */
    function getSmallEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setMediumEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isMediumEnabled();
    /**
     * method
     * 
     * @return ?bool
     */
    function getMediumEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setLargeEnabled($enabled = null);
    /**
     * method
     * 
     * @return bool
     */
    function isLargeEnabled();
    /**
     * method
     * 
     * @return ?bool
     */
    function getLargeEnabled();
    /**
     * method
     * 
     * 
     * @return NavigationDescriptorInterface
     */
    function setInitialColour(ColourInterface $colour = null);
    /**
     * method
     * 
     * @return ?ColourInterface
     */
    function getInitialColour();
    /**
     * method
     * 
     * @return bool
     */
    function hasInitialColour();
}