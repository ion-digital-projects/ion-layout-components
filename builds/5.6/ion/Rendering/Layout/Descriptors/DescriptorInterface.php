<?php
namespace ion\Rendering\Layout\Descriptors;

interface DescriptorInterface
{
    /**
     * method
     * 
     * 
     * @return DescriptorInterface
     */
    function setStringPropertyValue($name, $value = null);
}