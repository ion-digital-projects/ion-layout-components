<?php
namespace ion\Rendering\Layout\Descriptors;

use ion\Types\StringInterface;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Properties\ColourPropertyInterface;
use ion\Rendering\Layout\Descriptors\Properties\ImagePropertyInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorVectorInterface;
interface LayoutDescriptorInterface extends ColourPropertyInterface, ImagePropertyInterface
{
    /**
     * method
     * 
     * @return ModalDescriptorVectorInterface
     */
    function getModals();
    /**
     * method
     * 
     * @return NavigationSectionDescriptorInterface
     */
    function getNavigation();
    /**
     * method
     * 
     * @return HeaderSectionDescriptorInterface
     */
    function getHeader();
    /**
     * method
     * 
     * @return MainSectionDescriptorInterface
     */
    function getMain();
    /**
     * method
     * 
     * @return FooterSectionDescriptorInterface
     */
    function getFooter();
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    function setTitle($title = null);
    /**
     * method
     * 
     * @return ?StringInterface
     */
    function getTitle();
    /**
     * method
     * 
     * @return bool
     */
    function hasTitle();
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    function setFullHeight($fullHeight = null);
    /**
     * method
     * 
     * @return ?bool
     */
    function getFullHeight();
    /**
     * method
     * 
     * @return bool
     */
    function hasFullHeight();
}