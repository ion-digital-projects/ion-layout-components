<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout\Descriptors;

/**
 * Description of Form
 *
 * @author Justus
 */
use ion\Types\StringInterface;
use ion\Types\StringObject;
use ion\Types\Arrays\Specialized\StringVectorInterface;
use ion\Types\Arrays\Specialized\StringVector;
use ion\Rendering\Layout\Descriptors\Sections\NavigationSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\HeaderSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\MainSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\Sections\FooterSectionDescriptorInterface;
use ion\Rendering\Layout\Descriptors\ModalDescriptorInterface;
use ion\Rendering\Layout\ComponentException;
use ion\Rendering\Layout\Descriptors\Properties;
use ion\Rendering\Layout\ColourInterface;
trait LayoutDescriptorTrait
{
    use \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::setColour as setBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::getColour as getBackgroundColour;
        \ion\Rendering\Layout\Descriptors\Properties\ColourPropertyTrait::hasColour as hasBackgroundColour;
    }
    use \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait {
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::setImage as setBackgroundImage;
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::getImage as getBackgroundImage;
        \ion\Rendering\Layout\Descriptors\Properties\ImagePropertyTrait::hasImage as hasBackgroundImage;
    }
    private $navigation;
    private $header;
    private $main;
    private $footer;
    private $title;
    private $fullHeight;
    private $features;
    private $modals;
    //    protected function setModal(ModalDescriptorInterface $descriptor = null): LayoutDescriptorInterface {
    //
    //        $this->modals = $descriptor;
    //        return $this;
    //    }
    /**
     * method
     * 
     * @return ModalDescriptorVectorInterface
     */
    public function getModals()
    {
        if ($this->modals === null) {
            $this->modals = ModalDescriptorVector::create();
        }
        return $this->modals;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    protected function setNavigation(NavigationSectionDescriptorInterface $descriptor)
    {
        $this->navigation = $descriptor;
        return $this;
    }
    /**
     * method
     * 
     * @return NavigationSectionDescriptorInterface
     */
    public function getNavigation()
    {
        if ($this->header === null) {
            throw new ComponentException("Navigation component not initialized.");
        }
        return $this->navigation;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    protected function setHeader(HeaderSectionDescriptorInterface $descriptor)
    {
        $this->header = $descriptor;
        return $this;
    }
    /**
     * method
     * 
     * @return HeaderSectionDescriptorInterface
     */
    public function getHeader()
    {
        if ($this->header === null) {
            throw new ComponentException("Header component not initialized.");
        }
        return $this->header;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    protected function setMain(MainSectionDescriptorInterface $descriptor)
    {
        $this->main = $descriptor;
        return $this;
    }
    /**
     * method
     * 
     * @return MainSectionDescriptorInterface
     */
    public function getMain()
    {
        if ($this->main === null) {
            throw new ComponentException("Content component not initialized.");
        }
        return $this->main;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    protected function setFooter(FooterSectionDescriptorInterface $descriptor)
    {
        $this->footer = $descriptor;
        return $this;
    }
    /**
     * method
     * 
     * @return FooterSectionDescriptorInterface
     */
    public function getFooter()
    {
        if ($this->footer === null) {
            throw new ComponentException("Footer component not initialized.");
        }
        return $this->footer;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    public function setTitle($title = null)
    {
        if ($title === null) {
            $this->title = null;
            return $this;
        }
        $this->title = StringObject::create($title);
        return $this;
    }
    /**
     * method
     * 
     * @return ?StringInterface
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasTitle()
    {
        return $this->getTile() !== null;
    }
    /**
     * method
     * 
     * 
     * @return LayoutDescriptorInterface
     */
    public function setFullHeight($fullHeight = null)
    {
        $this->fullHeight = $fullHeight;
        return $this;
    }
    /**
     * method
     * 
     * @return ?bool
     */
    public function getFullHeight()
    {
        return $this->fullHeight;
    }
    /**
     * method
     * 
     * @return bool
     */
    public function hasFullHeight()
    {
        return $this->getFullHeight() === true;
    }
}