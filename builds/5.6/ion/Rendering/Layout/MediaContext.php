<?php
/*
 * See license information at the package root in LICENSE.md
 */
namespace ion\Rendering\Layout;

/**
 * Description of UriAction
 *
 * @author Justus
 */
use ion\Types\EnumObject;
final class MediaContext extends EnumObject implements MediaContextInterface
{
    const NONE = 0;
    const FOREGROUND = 1;
    const BACKGROUND = 2;
}